
<?php
require_once('../data/conexion.php');

$id_dm = $_GET['id_dm'];

$sql="SELECT TOP 1 tr_fcreacion,tr_id_dm,dm_id_cli,cli_nombre,dir_latitude,dir_longitude,tr_latitud,tr_longitud,tr_fcreacion FROM prg.tr_track tr
JOIN prg.dm_domicilios dm ON dm.dm_id=tr_id_dm
JOIN prg.cli_clientes cli ON dm.dm_id_cli = cli_id
JOIN prg.dir_direcciones ON dm_id_dir = dir_id 
WHERE tr_id_dm = '$id_dm' ORDER BY tr_fcreacion DESC ";
$ds=odbc_exec($conn,$sql);
    $data = array();
    while($fila=odbc_fetch_array($ds))
 {
    $lat= $fila['tr_latitud'];
    $lon= $fila['tr_longitud'];
    $last_update = date('d/m/y H:i:s',strtotime($fila['tr_fcreacion']));
    $lat_cliente=$fila['dir_latitude'];
    $lon_cliente=$fila['dir_longitude'];
    $nombre_cliente=$fila['cli_nombre'];
 }


require_once('../layouts/header_webservice.php');
?>
<body onload="calcRoute()">
         <div class="container">
               <div class="row">
                  <div id="googleMap" style="width:600px; height: 430px; "></div>
               </div>                  
               <div class="row">            
                  <div id="output" ></div>            
               </div>
         </div>
</body>
            <!---->
     
<?php
require_once('../layouts/foother.php');
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJPfKa5UyTLlJ1EAI8bIYH0nBau8NECEQ&libraries=place"> </script>
 
<script>

  function calcRoute() {
   var lat = <?php echo $lat;?>;
   var long = <?php echo $lon;?>;
   var lat_cliente = <?php echo $lat_cliente?>;
   var lon_cliente = <?php echo $lon_cliente?>;

   var mapOption = {
     center: new google.maps.LatLng(lat,long),
     zoom: 20,    
    disableDefaultUI: true,//quita las opciones en el mapa
     mapTypeId:google.maps.MapTypeId.ROADMAP
     
    };
     
   var gMapa = new google.maps.Map(document.getElementById("googleMap"),mapOption);
    //create a DirectionsService object to use the route method and get a result for our request
    var directionsService = new google.maps.DirectionsService();

   //create a DirectionsRenderer object which we will use to display the route
   var directionsDisplay = new google.maps.DirectionsRenderer();
    //bind the DirectionsRenderer to the map
   
    directionsDisplay.setMap(gMapa);
         var haight     = new google.maps.LatLng(lat, long);
         var oceanBeach = new google.maps.LatLng(lat_cliente, lon_cliente);
        
         var request = {
            origin: haight,
            destination: oceanBeach,
            
            travelMode: google.maps.TravelMode.DRIVING, //WALKING, BYCYCLING, TRANSIT
            unitSystem: google.maps.UnitSystem.METRIC,
            
         }

         //pass the request to the route method
         directionsService.route(request, function (result, status) {
            if (status == google.maps.DirectionsStatus.OK) {

               //Get distance and time
                $("#output").html("<div class='alert-info'><h6>Ultima Actualizacion:  <?php echo $last_update;?><br>Cliente: <?php echo $nombre_cliente; ?><br>Distancia Manejando: " + result.routes[0].legs[0].distance.text + ".<br />Tiempo Aproximado: " + result.routes[0].legs[0].duration.text + ".</h6></div>");
               
               //display route
               directionsDisplay.setDirections(result);
            } else {
               //delete route from map
               directionsDisplay.setDirections({ routes: [] });
               //center map in London
               map.setCenter(myLatLng);

               //show error message
               $("#output").html("<div class='alert-danger'>Could not retrieve driving distance.</div>");
            }
         });

       

      }

      var actualizar = () =>location.reload(true);

      setInterval(actualizar, 55000)
</script>

</body>
</html>
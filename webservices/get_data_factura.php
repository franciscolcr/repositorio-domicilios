<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow- *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
require_once('../data/conexion.php');

$codigo = $_GET["codigo"];
var_dump($codigo);
if ($codigo) {
    $data["data"] = array();

    $sql1 = "SELECT dm_id,mo_nombre,mo_telefono,mo_url_img,dm_observacion,est_nombre,cantidad,nobasico,descripcion,FechaCreacion 
    FROM prg.dm_domicilios 
 	JOIN prg.docd dc on dm_id_doc=pludoccliente
    JOIN prg.productos pr ON dc.pluproducto=pr.pluproducto
    JOIN prg.mo_motoristas on dm_id_mt = mo_id    
    JOIN prg.estados_domicilios on dm_estado=est_id
    where dm_codigo = '$codigo' ";
    $ds = odbc_exec($conn, $sql1);

    while ($fila = odbc_fetch_array($ds)) {
        $item = array(
            'estado_domicilio' => $fila['est_nombre'],
            'motorista' => $fila['mo_nombre'],
            'foto_moto' => $fila['mo_url_img'],
            'telefono_moto' => $fila['mo_telefono'],
            'observacion' => $fila['dm_observacion'],
            'cantidad' => $fila['cantidad'],
            'nobasico' => $fila['nobasico'],
            'descripcion' => $fila['descripcion']
        );
        array_push($data['data'], $item);
    }
    if (empty($data['data'])) {
        //echo json_encode(array('mensaje' => 'No se han Encotrado tu Pedido'));
        error('No se han Encotrado tu Pedido');
    } else {
        echo json_encode($data);
    }
} else {
    //echo json_encode(array('mensaje' => 'Debes ingresar un codigo'));
    error('Debes ingresar un codigo');
}

function error($mensaje)
{
    echo  json_encode(array('mensaje' => $mensaje));
}

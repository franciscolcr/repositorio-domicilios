<?php

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
require_once('../data/conexion.php');

$codigo=$_GET["codigo"];

if ($codigo) {
    $data["data"] = array();

    $sql1="SELECT TOP 1 tr_id,dm_id,mo_nombre,mo_telefono,tr_latitud,tr_longitud,tr_id,dm_observacion,
    tr_fcreacion,est_nombre 
    FROM prg.dm_domicilios 
    JOIN prg.mo_motoristas on dm_id_mt = mo_id
    JOIN prg.tr_track on tr_id_dm = dm_id
    JOIN prg.estados_domicilios on dm_estado=est_id
    where dm_codigo = '$codigo' order by tr_id desc ";
    $ds=odbc_exec($conn,$sql1);
    
    while($fila=odbc_fetch_array($ds)){
        $item = array(
            'estado_domicilio' => $fila['est_nombre'],
            'motorista' => $fila['mo_nombre'],
            'telefono_moto' => $fila['mo_telefono'],
            'observacion' => $fila['dm_observacion'],
            'latitud' => $fila['tr_latitud'],
            'longitud' => $fila['tr_longitud'],
            'ultima_actualizacion_dia' => date('d/m/Y', strtotime($fila['tr_fcreacion'])),
            'ultima_actualizacion_hora' => date('H:m:i', strtotime($fila['tr_fcreacion']))

        );
        array_push($data['data'], $item);
    
    }
    if (empty($data['data'])) {
        //echo json_encode(array('mensaje' => 'No se han Encotrado tu Pedido'));
        error('No se han Encotrado tu Pedido');
    }else {
        echo json_encode($data);
    }
    
}else{
    //echo json_encode(array('mensaje' => 'Debes ingresar un codigo'));
    error('Debes ingresar un codigo');
}

function error($mensaje){
    echo  json_encode(array('mensaje' => $mensaje));
}


<?php
require_once('../data/conexion.php');
$fecha_actual = date('Y-m-d');
//echo $fecha_actual;
require_once('../layouts/header.php');
?>
<style>
    table#example.dataTable tbody tr.amarillo {
        background-color: #ffa;
    }

    .amarillo {
        background-color: #ffa;
        color: black;
        font-weight: bold;
    }

    table#example.dataTable tbody tr.red {
        background-color: #E74C3C;
    }

    .red {
        background-color: #E74C3C;
        color: white;
        font-weight: bold;
    }

    table#example.dataTable tbody tr.sky_blue {
        background-color: #5DADE2;
    }

    .sky_blue {

        background-color: #5DADE2;
        color: white;
        font-weight: bold;
    }

    table#example.dataTable tbody tr.verde {
        background-color: #82E0AA;
    }

    .verde {
        background-color: #82E0AA;
        color: black;
        font-weight: bold;
    }
</style>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include("../layouts/menu_sales.php") ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <?php include("../layouts/navbar.php") ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h4 class="m-0 font-weight-bold text-primary">Domicilios </h4>
                            <!--  <button class="btn btn-success">Actualizar</button>-->
                        </div>
                        <div class="card-body">
                            <div class="table-responsive" id="upload">
                                <table class="table table-border display" id="example" width="100%" cellspacing="0" style="text-align: center;">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="10px">#</th>
                                            <th class="text-center" width="25px">Código</th>
                                            <th class="text-center">Tipo</th>
                                            <th class="text-center" width="190px">Observacion</th>
                                            <th class="text-center">Cliente</th>
                                            <th class="text-center" width="75px">Fecha/Hora</th>
                                            <th class="text-center" width="90px">Inicio/Final</th>
                                            <th class="text-center" width="25px">Estado</th>
                                            <th class="text-center " width="25px">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="despacho">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->

            <!-- modal para ver los detalles del domicilio -->
            <div class="modal fade bd-example-modal-lg" id="ver_dm" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Detalles del Domicilio</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" autocomplet="OFF">
                                <div class="container-fluid">
                                    <!--parte de arriba del modal detalles domicilios-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="codigo">Codigo del Domicilio</label>
                                            <input type="text" class="form-control" disabled id="codigo_dm_ver" name="codigo_dm_ver">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="codigo">Tipo de Domicilio</label>
                                            <input type="text" class="form-control bg-success text-white" disabled id="tipo_dm_ver" name="tipo_dm_ver">
                                        </div>
                                    </div>
                                    <!-- venta -->
                                    <div id="divVenta" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">Código de Documento</label>
                                                <input type="text" class="form-control bg-primary text-white" id="codigodoc" disabled>
                                            </div>
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUDocumento</label>
                                                <input type="text" class="form-control bg-primary text-white" id="dm_doc" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos" class="table table-bordered" width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Traslado -->
                                    <div id="divTraslado" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">Código de Traslado</label>
                                                <input type="text" class="form-control bg-primary text-white" id="codigodoc1" disabled>
                                            </div>
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUTraslado</label>
                                                <input type="text" class="form-control bg-primary text-white" id="dm_doct" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_traslados" class="table table-bordered" width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Cobros -->
                                    <div id="divCobros" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUCobro</label>
                                                <input type="text" class="form-control bg-primary text-white" id="dm_docc" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_cobros" class="table table-bordered" width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- rutas -->
                                    <div id="divRutas" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLURuteo</label>
                                                <input type="text" class="form-control bg-primary text-white" id="dm_doc_rutas" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_rutas" class="table table-bordered" width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- recoger muestra -->
                                    <div id="divMuestra" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLURmuestra</label>
                                                <input type="text" class="form-control bg-primary text-white" id="dm_docm" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_muestra" class="table table-bordered" width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Admin -->
                                    <div id="divAdmin" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUAdmnistrativo</label>
                                                <input type="text" class="form-control bg-primary text-white" id="dm_doc_ad" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_admin" class="table table-bordered" width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- CP -->
                                    <div id="divCP" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUCP</label>
                                                <input type="text" class="form-control bg-primary text-white" id="dm_doc_cp" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_cp" class="table table-bordered" width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- taller -->
                                    <div id="divTaller" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUTaller</label>
                                                <input type="text" class="form-control bg-primary text-white" id="dm_doc_taller" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_taller" class="table table-bordered" width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--parte de abajo del modal ver dm-->
                                    <div class="row">
                                        <div class="col-md-6 mt-2">
                                            <label for="motoristas_select">Nombre del vendedor</label>
                                            <input type="text" class="form-control" disabled id="vendedor_dm_ver" name="vendedor_dm_ver">
                                        </div>
                                        <div class="col-md-3 mt-2">
                                            <label for="motoristas_select">Fecha Creacion</label>
                                            <input type="text" class="form-control" disabled id="fecha_dm_ver" name="fecha_dm_ver">
                                        </div>
                                        <div class="col-md-3 mt-2">
                                            <label for="motoristas_select">Hora Creacion</label>
                                            <input type="text" class="form-control" disabled id="hora_dm_ver" name="hora_dm_ver">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="observaciones_dm_ver">Observaciones</label>
                                            <textarea id="observaciones_dm_ver" class="form-control bg-info text-white" disabled></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="observaciones_dm_ver">Motortista</label>
                                            <input id="motorista_dm_ver" class="form-control" disabled />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="observaciones_dm_ver">Fecha Asignacion</label>
                                            <input id="fasignacion_dm_ver" class="form-control" disabled />
                                        </div>
                                        <div class="col-md-6">
                                            <label for="observaciones_dm_ver">Hora Asignacion</label>
                                            <input id="hasignacion_dm_ver" class="form-control" disabled />
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php
            require_once('../layouts/foother.php');
            ?>
            <script src="../js/ajax/vendedor.js?php echo time(); ?>"></script>
            <script>
                //para que funcione el tooltip en el table
                $(function() {
                    $('body').tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                });
           
                function ver_dm(id_domicilio) {
                    $('#ver_dm').modal('show');
                    //alert(id_domicilio);
                    trae_dm_ver(id_domicilio);
                }
                //para ver la hora de asignacion
                function get_hour() {
                    var d = new Date();
                    const CERO = n => n = n < 10 ? "0" + n : n;
                    $('#hora_asignacion').val(CERO(d.getHours()) + ":" + CERO(d.getMinutes()) + ":" + CERO(d.getSeconds()));
                }

                //actuliza cada 3 segundos la funcion de obtener la hora
                setInterval(get_hour, 3000);
                //trae los datos del domicilio
                function trae_dm(id) {
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: '../data/trae_datos_dm.php',
                        data: {
                            id: id
                        },
                        success: function(html) {
                            //var mo[] = html;
                            $('#codigo_dm').val(html.codigo);
                            $('#dm_id').val(html.id);
                            $('#dm_estado').val(html.estado);
                            $('#email_cliente').val(html.correo);
                            $('#nombre_cliente').val(html.cliente);
                            $('#id_dir').val(html.id_dir); 
                        }
                    });
                }
                //trae los datos del domicilio
                function trae_dm_ver(id) {
                    var divVenta = document.getElementById("divVenta");
                    var divTraslado = document.getElementById("divTraslado");
                    var divCobros = document.getElementById("divCobros");
                    var divAdmin = document.getElementById("divAdmin");
                    var divMuestra = document.getElementById("divMuestra");
                    var divCP = document.getElementById("divCP");
                    var divTaller = document.getElementById("divTaller");
                    var divRutas = document.getElementById("divRutas");
                    //alert(id);
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: '../data/trae_datos_dm.php',
                        data: {
                            id: id
                        },
                        success: function(html) {
                            console.log(html.doc, html.tipo);
                            //var mo[] = html;
                            $('#codigo_dm_ver').val(html.codigo);
                            $('#tipo_dm_ver').val(html.motivo);
                            $('#vendedor_dm_ver').val(html.vendedor);
                            $('#fecha_dm_ver').val(html.fecha);
                            $('#hora_dm_ver').val(html.hora);
                            $('#observaciones_dm_ver').val(html.observacion);
                            $('#motorista_dm_ver').val(html.motorista);
                            $('#fasignacion_dm_ver').val(html.fasignacion);
                            $('#hasignacion_dm_ver').val(html.hasignacion);
                            //$('#dm_tipo').val(html.tipo);         
                            $('#dm_doc').val(html.doc); // venta
                            $('#dm_doct').val(html.doc); // traslados
                            $('#dm_docc').val(html.doc); // cobros
                            $('#dm_doc_rutas').val(html.doc) // ruteo
                            $('#dm_doc_ad').val(html.doc); // admin
                            $('#dm_docm').val(html.doc); // muestra
                            $('#dm_doc_cp').val(html.doc); // compra en plaza
                            $('#dm_doc_taller').val(html.doc); // compra en plaza
                            $('#codigodoc').val(html.codigoFact);
                            $('#codigodoc1').val(html.codigoFact);
                            $('#codigodoc2').val(html.n_orden);
                            $('#dm_suc').val(html.sucursal);
                            // venta y devoluciones
                            if (html.tipo == 2 || html.tipo == 4) {
                                divVenta.style.display = "block";
                                divTraslado.style.display = "none";
                                divCobros.style.display = "none";
                                divAdmin.style.display = "none";
                                divMuestra.style.display = "none";
                                divCP.style.display = "none";
                                divTaller.style.display = "none";
                                divRutas.style.display = "none";
                            } else if (html.tipo == 11) { // traslado
                                divVenta.style.display = "none";
                                divTraslado.style.display = "block";
                                divCobros.style.display = "none";
                                divAdmin.style.display = "none";
                                divMuestra.style.display = "none";
                                divCP.style.display = "none";
                                divTaller.style.display = "none";
                                divRutas.style.display = "none";
                            } else if (html.tipo == 1) { // cobro
                                divVenta.style.display = "none";
                                divTraslado.style.display = "none";
                                divCobros.style.display = "block";
                                divAdmin.style.display = "none";
                                divMuestra.style.display = "none";
                                divCP.style.display = "none";
                                divTaller.style.display = "none";
                                divRutas.style.display = "none";
                            } else if (html.tipo == 6) { // muestra
                                divVenta.style.display = "none";
                                divTraslado.style.display = "none";
                                divCobros.style.display = "none";
                                divAdmin.style.display = "none";
                                divMuestra.style.display = "block";
                                divCP.style.display = "none";
                                divTaller.style.display = "none";
                                divRutas.style.display = "none";
                            } else if (html.tipo == 7) { // CP
                                divVenta.style.display = "none";
                                divTraslado.style.display = "none";
                                divCobros.style.display = "none";
                                divAdmin.style.display = "none";
                                divMuestra.style.display = "none";
                                divCP.style.display = "block";
                                divTaller.style.display = "none";
                                divRutas.style.display = "none";
                            } else if (html.tipo == 9) { // Admin
                                divVenta.style.display = "none";
                                divTraslado.style.display = "none";
                                divCobros.style.display = "none";
                                divAdmin.style.display = "block";
                                divMuestra.style.display = "none";
                                divCP.style.display = "none";
                                divTaller.style.display = "none";
                                divRutas.style.display = "none";
                            } else if (html.tipo == 14) { // taller
                                divVenta.style.display = "none";
                                divTraslado.style.display = "none";
                                divCobros.style.display = "none";
                                divAdmin.style.display = "none";
                                divMuestra.style.display = "none";
                                divCP.style.display = "none";
                                divTaller.style.display = "block";
                                divRutas.style.display = "none";
                            } else if (html.tipo == 12) { // cobro
                                divVenta.style.display = "none";
                                divTraslado.style.display = "none";
                                divCobros.style.display = "none";
                                divAdmin.style.display = "none";
                                divMuestra.style.display = "none";
                                divCP.style.display = "none";
                                divTaller.style.display = "none";
                                divRutas.style.display = "block";
                            } else {
                                divVenta.style.display = "none";
                                divTraslado.style.display = "none";
                                divCobros.style.display = "none";
                                divAdmin.style.display = "none";
                                divMuestra.style.display = "none";
                                divCP.style.display = "none";
                                divTaller.style.display = "none";
                                divRutas.style.display = "none";
                            }
                            trae_productos(html.doc, html.tipo);

                        }
                    });
                }
               
                //trae los productos del documento de una venta
                function trae_productos(id_doc, tipo) {
                    var tipo = tipo;
                    var ndoc = id_doc;
                    var tabla = '';
                    //alert(ndoc);
                    if (tipo == 11) {
                        tabla = '#table_productos_traslados';
                    }
                    if (tipo == 2 || tipo == 4) {
                        tabla = '#table_productos';
                    }
                    if (tipo == 6) {
                        tabla = '#table_productos_muestra';
                    }
                    if (tipo == 7) {
                        tabla = '#table_productos_cp';
                    }
                    if (tipo == 1) {
                        tabla = '#table_productos_cobros';
                    }

                    if (tipo == 9) {
                        tabla = '#table_productos_admin';
                    }
                    if (tipo == 14) {
                        tabla = '#table_productos_taller';
                    }
                    if (tipo == 12) {
                        tabla = '#table_productos_rutas';
                    }
                    // (tipo == 11) ? (tabla = '#table_productos_traslados', ruta = '../data/trae_detalle_traslado.php') : (tabla = tabla, ruta = '../data/trae_detalle_doc.php');
                    $.ajax({
                        data: {
                            ndoc: ndoc,
                            tipo: tipo
                        },
                        url: "../data/trae_detalle_doc.php",
                        dataType: 'html',
                        type: 'POST',
                        beforeSend: function() {
                            $(tabla).html(
                                '<center><img alt="Cargando espere..." src="../img/searching3.gif" /></center>'
                            );
                        },
                        success: function(html) {
                            setTimeout(function() {
                                $(tabla).html(html);
                            }, 2000)
                        }
                    });
                }
            </script>
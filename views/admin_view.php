<?php
require_once('../data/conexion.php');

require_once('../layouts/header.php');
?>
<body id="page-top"     >

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include("../layouts/menu_admin.php")?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
    <div id="content">

        <?php include("../layouts/navbar.php")?>
       
        <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <!-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Dashboard ad</h1>
                </div> -->
               <!-- <div class="d-sm-flex align-items-center  mb-4">            
                    <a href="create_domicilios.php" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Crear Domicilio</a>
                    <a href="create_motoristas.php" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Transportes</a>
                    <a href="#" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Buscar Domicilio</a>                          
                </div>-->
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Domicilios Del Dia</h6> 
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tipo</th>
                                            <th>Observacion</th>
                                            <th>Cliente</th>
                                            <th>Fecha</th>
                                            <th>Hora Creaciòn</th>
                                        </tr>
                                    </thead>                                   
                                    <tbody>    
                                    <?php                        
                                            $sql1="SELECT mt_motivo,dm_observacion,dm_fcreacion,
                                            (select cli_nombre from prg.cli_clientes where cli_id=dm_id_cli) cliente from prg.dm_domicilios 
                                            JOIN prg.mt_motivos ON dm_id_mt=mt_id WHERE dm_estado!=0 ORDER BY dm_fcreacion DESC";
                                            if (isset($sql1)) {
                                             } else {
                                                    $ds=odbc_exec($conn,$sql1);
                                                    while($fila=odbc_fetch_array($ds))
                                                        {$i++;
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $i;?></td>
                                                                <td><?php echo $fila['mt_motivo'] ?></td>                                                            
                                                                <td><?php echo $fila['dm_observacion'] ?></td>
                                                                <td><?php if(isset($fila['cliente'])){ echo $fila['cliente'];}else{echo "Sin Cliente";}?></td>
                                                                <td><?php echo date('d-m-Y',strtotime($fila['dm_fcreacion'])) ?></td>
                                                                <td><?php echo date('H:m:i',strtotime($fila['dm_fcreacion'])) ?></td>                                                                                                                             
                                                            </tr>
                                                        <?php
                                                        }
                                                odbc_close($conn);
                                            }
                                    ?>
                                    </tbody>
                            </table>
                            </div>
                        </div>
                    </div>             

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->


<?php
require_once('../layouts/foother.php');
?>

<?php
require_once('../data/conexion.php');
require_once('../layouts/header.php');
if (isset($_GET['msj'])) {
    $msj = $_GET['msj'];
}
?>
<style>
    #geomap {
        width: 100%;
        height: 400px;
    }
</style>


<body>
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include("../layouts/menu_sales.php") ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include("../layouts/navbar.php") ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->

                    <div class="row">
                        <div class="col-xl-10 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Registrar Cliente</h6>

                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <form action="../data/model_create_cliente.php" method="POST" autocomplete="OFF" accept-charset="UTF-8">
                                        <div class="row">

                                            <?php if (isset($msj) == 1) { ?>
                                                <script>
                                                    Swal.fire({
                                                        title: 'Cliente Creado con Exito',
                                                        //text: "You won't be able to revert this!",
                                                        icon: 'success',
                                                        showCancelButton: false,
                                                        confirmButtonColor: '#3085d6',
                                                        cancelButtonColor: '#d33',
                                                        confirmButtonText: 'ok!'
                                                    }).then((result) => {
                                                        if (result.isConfirmed) {
                                                            document.location.href = 'create_customers.php';
                                                        }
                                                    })
                                                </script>
                                            <?php } ?>


                                            <select name="select_tipo_cliente" id="select_tipo_cliente" class="form-control form-control-sm" onchange="ocultdiv();">
                                                <option value="0" disabled selected>Seleccion el tipo de cliente</option>
                                                <option value="1">Cliente Normal</option>
                                                <option value="2">Cliente Empresarial</option>
                                                <option value="3">Sucursal</option>
                                                <option value="4">Proveedor </option>
                                            </select>
                                            <!-- formulario para sucursal-->
                                            <div id="sucursalUbica" style="display: none">
                                                <div class="row">
                                                    <div class="form-group col-md-4 mb-2 mt-3">
                                                        <label>Seleccione la sucursal</label>
                                                        <select id="sucursal_id" class="form-control form-control-sm" name="sucursal_id">
                                                            <option value="" selected disabled> Seleccione una División</option>
                                                            <?php $sql1 = "select * from prg.divisiones";
                                                            $ds = odbc_exec($conn, $sql1);
                                                            while ($fila = odbc_fetch_array($ds)) { ?>
                                                                <option value="<?php echo utf8_encode($fila['Nombre']) ?>" <?php if ($fila['PLUDivision'] == $_COOKIE['sucursal']) {
                                                                                                                                echo "selected";
                                                                                                                            } else {
                                                                                                                            }
                                                                                                                            ?>><?php echo utf8_encode($fila['Nombre']) ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <div class="invalid-feedback">División requerida</div>
                                                    </div>
                                                    <div class="form-group col-md-4 mb-2 mt-3">
                                                        <label for="Tel-Oficina">Telefono <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="telefono_suc" name="telefono_suc">
                                                    </div>
                                                    <div class="form-group col-md-4 mb-2 mt-3">
                                                        <label for="capaciad">Correo <span style="color:red">*</span></label>
                                                        <input type="email" class="form-control" id="correo_suc" name="correo_suc">
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- formulario para proveedor-->
                                            <div id="proveedorUbica" style="display: none">
                                                <div class="row">

                                                    <div class="form-group col-md-6 ">
                                                        <label for="tipo_vehiculo">Nombre Proveedor <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="proveedor" name="proveedor">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="Tel-Oficina">Teléfono <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="telefono_pro" name="telefono_pro">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">NIT</label>
                                                        <input type="text" class="form-control" id="nit_pro" name="nit_pro">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">NRC</label>
                                                        <input type="text" class="form-control" id="nrc_pro" name="nrc_pro">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">Correo <span style="color:red">*</span></label>
                                                        <input type="email" class="form-control" id="correo_pro" name="correo_pro">
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- este input sirve para el cliente normal-->
                                            <div id="cliente_normal" style="display: none">
                                                <div class="row">

                                                    <div class="form-group col-md-6 ">
                                                        <label for="tipo_vehiculo">Nombre Cliente <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="nombrecliente" name="nombrecliente">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="Tel-Oficina">Telefono <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="telefono" name="telefono">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="modelo">Dui</label>
                                                        <input type="text" class="form-control" id="dui" name="dui">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">NIT</label>
                                                        <input type="text" class="form-control" id="nit" name="nit">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">NRC</label>
                                                        <input type="text" class="form-control" id="nrc" name="nrc">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">Correo <span style="color:red">*</span></label>
                                                        <input type="email" class="form-control" id="correo_n" name="correo_n">
                                                    </div>

                                                </div>
                                            </div>
                                            <!--cliente empresarial input-->
                                            <div id="cliente_empresarial" style="display: none">
                                                <div class="row">

                                                    <div class="form-group col-md-6 ">
                                                        <label for="tipo_vehiculo">Nombre Cliente <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="nombrecliente_emp" name="nombrecliente_emp">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="nom_emp">Nombre De Empresa <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="nom_emp" name="nom_emp">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="Tel-Oficina">Telefono De Empresa <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="telefono_emp" name="telefono_emp">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="per_cont">Persona De Contacto</label>
                                                        <input type="text" class="form-control" id="per_cont" name="per_cont">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="num_cont">Numero De Contacto</label>
                                                        <input type="text" class="form-control" id="num_cont" name="num_cont">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="modelo">Dui</label>
                                                        <input type="text" class="form-control" id="dui_emp" name="dui_emp">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">NIT <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="nit_emp" name="nit_emp">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">NRC <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="nrc_emp" name="nrc_emp">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">Correo <span style="color:red">*</span></label>
                                                        <input class="form-control" id="correo_e" name="correo_e" type="email" size="30">
                                                    </div>

                                                </div>
                                            </div>
                                            <!--fin cliente empresarial-->

                                        </div>

                                        <div class="form-row" id="uscursalU">
                                            <div class="form-group col-md-6">
                                                <label for="placa">Buscar Cliente Por Nombre</label>
                                                <select name="cliente_mt" id="cliente_mt" class="form-control form-control-sm">
                                                    <option value='0'>- Buscar Cliente por Nombre -</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="placa">Codigo Cliente MT</label>
                                                <select name="cod_mt" id="cod_mt" class="form-control form-control-sm">
                                                    <option value='0'>- Buscar Cliente por Codigo -</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row" id="uscursalU2">
                                            <div class="form-group col-md-6">
                                                <label for="nit_mt">Nit Cliente MT</label>
                                                <select name="nit_mt" id="nit_mt" class="form-control form-control-sm">
                                                    <option value='0'>- Buscar Cliente por NIT -</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="nit_mt">Tarjeta Cliente MT</label>
                                                <select name="tar_mt" id="tar_mt" class="form-control form-control-sm">
                                                    <option value='0'>- Buscar Cliente por Tarjeta -</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <input type="hidden" class="form-control bg-light border-0 small" id="cliente" name="cliente">
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="placa">Direcciòn</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control bg-light border-0 small" id="search_location" name="search_location" required>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-danger" onclick="limpiar();" type="button">
                                                            <i class="fa fa-times fa-sm"></i>
                                                        </button>
                                                        <button class="btn btn-primary get_map" type="submit">
                                                            <i class="fas fa-search fa-sm"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <a href="#" class="btn btn-primary" role="button" data-bs-toggle="button" onclick="ubi_cli();">Coordenadas</a>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <div id="geomap"></div>
                                                <input type="hidden" class="search_addr" size="65" id="direccion" name="direccion">
                                                <input type="hidden" class="search_latitude" size="20" id="latitude" name="latitude">
                                                <input type="hidden" class="search_longitude" size="20" id="longitude" name="longitude">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Guardar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- modal para obtener coordenadas de el cliente -->
            <div class="modal fade bd-example-modal-lg" id="ubi_cli" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Ubicacion Del Cliente</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form autocomplet="OFF">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="codigo">Direccion</label>
                                            <input type="text" class="form-control" id="direc" name="direc">
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 mt-2">
                                            <label for="motoristas_select">Latitud</label>
                                            <input type="text" class="form-control bg-info text-white" id="lati" name="lati">
                                        </div>
                                        <div class="col-md-6 mt-2">
                                            <label for="codigo">Longitud </label>
                                            <input type="text" class="form-control bg-info text-white" id="long" name="long">
                                            <!--26-07-21-->
                                        </div>

                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="comparar();" data-dismiss="modal">Guardar</button>
                        </div>
                        </form>

                    </div>
                </div>
            </div>
            <!-- modal para obtener coordenadas de el cliente -->

            <?php
            require_once('../layouts/foother.php');
            ?>
            <!--  AIzaSyDJPfKa5UyTLlJ1EAI8bIYH0nBau8NECEQ -->
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3cejbHkJOZfpHllVqF0bApcL2j0yKU8Y&libraries=places"></script>
            <script>
                $(document).ready(function() {
                    $('#telefono').mask('0000-0000');
                    $('#dui').mask('00000000-0');
                    $('#nit').mask('0000-000000-000-0');
                    $('#nrc').mask('000000-0');
                    $('#num_cont').mask('0000-0000');
                    $('#telefono_emp').mask('0000-0000');
                    $('#dui_emp').mask('00000000-0');
                    $('#nit_emp').mask('0000-000000-000-0');
                    $('#nrc_emp').mask('000000-0');
                    $('#telefono_suc').mask('0000-0000');
                    $('#telefono_pro').mask('0000-0000');
                    $('#nit_pro').mask('0000-000000-000-0');
                });

                $("#sucursal_id").select2({
                    width: '100%'
                });
                var geocoder;
                var map;
                var marker;

                /*
                 * Google Map with marker
                 */
                function initialize() {
                    var initialLat = $('.search_latitude').val();
                    var initialLong = $('.search_longitude').val();
                    initialLat = initialLat ? initialLat : 13.6871936;
                    initialLong = initialLong ? initialLong : -89.2469248;
                    var input1 = document.getElementById("search_location");
                    var autocomplete1 = new google.maps.places.Autocomplete(input1);
                    var latlng = new google.maps.LatLng(initialLat, initialLong);
                    var options = {
                        zoom: 16,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    map = new google.maps.Map(document.getElementById("geomap"), options);

                    geocoder = new google.maps.Geocoder();

                    marker = new google.maps.Marker({
                        map: map,
                        draggable: true,
                        position: latlng
                    });

                    google.maps.event.addListener(marker, "dragend", function() {
                        var point = marker.getPosition();
                        map.panTo(point);
                        geocoder.geocode({
                            'latLng': marker.getPosition()
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                map.setCenter(results[0].geometry.location);
                                marker.setPosition(results[0].geometry.location);
                                $('.search_addr').val(results[0].formatted_address);
                                $('.search_latitude').val(marker.getPosition().lat());
                                $('.search_longitude').val(marker.getPosition().lng());
                            }
                        });
                    });

                }

                $(document).ready(function() {
                    //load google map
                    initialize();

                    /*
                     * autocomplete location search
                     */
                    var PostCodeid = '#search_location';
                    $(function() {
                        $(PostCodeid).autocomplete({
                            source: function(request, response) {
                                geocoder.geocode({
                                    'address': request.term
                                }, function(results, status) {
                                    response($.map(results, function(item) {
                                        return {
                                            label: item.formatted_address,
                                            value: item.formatted_address,
                                            lat: item.geometry.location.lat(),
                                            lon: item.geometry.location.lng()
                                        };
                                    }));
                                });
                            },
                            select: function(event, ui) {
                                $('.search_addr').val(ui.item.value);
                                $('.search_latitude').val(ui.item.lat);
                                $('.search_longitude').val(ui.item.lon);
                                var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                                marker.setPosition(latlng);
                                initialize();
                            }
                        });
                    });

                    /*
                     * Point location on google map
                     */
                    $('.get_map').click(function(e) {
                        var address = $(PostCodeid).val();
                        geocoder.geocode({
                            'address': address
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                map.setCenter(results[0].geometry.location);
                                marker.setPosition(results[0].geometry.location);
                                $('.search_addr').val(results[0].formatted_address);
                                $('.search_latitude').val(marker.getPosition().lat());
                                $('.search_longitude').val(marker.getPosition().lng());
                            } else {
                                alert("Es necesario ingresar un Lugar para buscarlo.") //+ status);
                            }
                        });
                        e.preventDefault();
                    });

                    //Add listener to marker for reverse geocoding
                    google.maps.event.addListener(marker, 'drag', function() {
                        geocoder.geocode({
                            'latLng': marker.getPosition()
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    $('.search_addr').val(results[0].formatted_address);
                                    $('.search_latitude').val(marker.getPosition().lat());
                                    $('.search_longitude').val(marker.getPosition().lng());
                                }
                            }
                        });
                    });
                });

                $(document).ready(function() {
                    $('#cliente_mt').select2({
                        ajax: {
                            url: "../data/list_clientes.php",
                            type: "post",
                            dataType: 'json',
                            delay: 250,
                            data: function(params) {
                                return {
                                    searchTerm: params.term // search term
                                };
                            },
                            processResults: function(response) {
                                return {
                                    results: response
                                };
                            },
                            cache: true
                        },
                        width: '100%',
                        placeholder: "xxxxxx xxxx xxx"

                    });

                    $('#cod_mt').select2({
                        ajax: {
                            url: "../data/list_cliente_cod.php",
                            type: "post",
                            dataType: 'json',
                            delay: 250,
                            data: function(params) {
                                return {
                                    searchTerm: params.term // search term
                                };
                            },
                            processResults: function(response) {
                                return {
                                    results: response
                                };
                            },
                            cache: true
                        },
                        width: '100%',
                        placeholder: "00000",

                    });

                    $('#nit_mt').select2({
                        ajax: {
                            url: "../data/list_cliente_nit.php",
                            type: "post",
                            dataType: 'json',
                            delay: 250,
                            data: function(params) {
                                return {
                                    searchTerm: params.term // search term
                                };
                            },
                            processResults: function(response) {
                                return {
                                    results: response
                                };
                            },
                            cache: true
                        },
                        width: '100%',
                        placeholder: "0000-000000-000-0",
                        allowClear: true,
                        separator: '-'

                    });

                    $('#tar_mt').select2({
                        ajax: {
                            url: "../data/list_cliente_tar.php",
                            type: "post",
                            dataType: 'json',
                            delay: 250,
                            data: function(params) {
                                return {
                                    searchTerm: params.term // search term
                                };
                            },
                            processResults: function(response) {
                                return {
                                    results: response
                                    // $('#cliente').val(response)
                                };
                            },
                            cache: true
                        },
                        width: '100%',
                        placeholder: "0000000",
                        allowClear: true,
                        separator: '-'

                    });
                });

                $(function() {
                    $(document).on('change', '#cliente_mt', function() { //detectamos el evento change
                        var value = $(this).val(); //sacamos el valor del select
                        $('#cliente').val(value); //le agregamos el valor al input (notese que el input debe tener un ID para que le caiga el valor) 
                        $('option', '#cod_mt').remove();
                        $('option', '#nit_mt').remove();
                        $('option', '#tar_mt').remove();

                    });

                    $(document).on('change', '#cod_mt', function() {
                        var value = $(this).val();
                        $('#cliente').val(value);
                        $('option', '#cliente_mt').remove();
                        $('option', '#nit_mt').remove();
                        $('option', '#tar_mt').remove();
                    });

                    $(document).on('change', '#nit_mt', function() {
                        var value = $(this).val();
                        $('#cliente').val(value);
                        $('option', '#cod_mt').remove();
                        $('option', '#cliente_mt').remove();
                        $('option', '#tar_mt').remove();
                    });

                    $(document).on('change', '#tar_mt', function() {
                        var value = $(this).val();
                        $('#cliente').val(value);
                        $('option', '#cod_mt').remove();
                        $('option', '#nit_mt').remove();
                        $('option', '#cliente_mt').remove();
                    });

                });

                function ubi_cli() {
                    $('#ubi_cli').modal('show');

                } // modal ubicacion cliente

                function comparar() {

                    var direc = $('#direc').val();
                    var lati = $('#lati').val();
                    var long = $('#long').val();

                    $('.search_addr').val(direc);
                    $('.search_latitude').val(lati);
                    $('.search_longitude').val(long)
                    //alert (lati);   
                }


                // esta funcion realiza el ocultamiento de los input de el select  de cliente.
                function ocultdiv() {
                    var select_tipo_cliente = document.getElementById('select_tipo_cliente').value;

                    if (select_tipo_cliente == 1) {
                        document.getElementById("proveedorUbica").style.display = "none";
                        document.getElementById("cliente_normal").style.display = "block";
                        document.getElementById("cliente_empresarial").style.display = "none";
                        document.getElementById("sucursalUbica").style.display = "none";

                        $('#cliente_mt').prop('disabled', false);
                        $('#cod_mt').prop('disabled', false);
                        $('#nit_mt').prop('disabled', false);
                        $('#tar_mt').prop('disabled', false);
                    } else if (select_tipo_cliente == 2) {
                        document.getElementById("proveedorUbica").style.display = "none";
                        document.getElementById("cliente_empresarial").style.display = "block";
                        document.getElementById("cliente_normal").style.display = "none";
                        document.getElementById("sucursalUbica").style.display = "none";


                        $('#cliente_mt').prop('disabled', false);
                        $('#cod_mt').prop('disabled', false);
                        $('#nit_mt').prop('disabled', false);
                        $('#tar_mt').prop('disabled', false);
                    } else if (select_tipo_cliente == 3) {
                        document.getElementById("proveedorUbica").style.display = "none";
                        document.getElementById("sucursalUbica").style.display = "block";
                        document.getElementById("cliente_empresarial").style.display = "none";
                        document.getElementById("cliente_normal").style.display = "none";

                        $('#cliente_mt').prop('disabled', true);
                        $('#cod_mt').prop('disabled', true);
                        $('#nit_mt').prop('disabled', true);
                        $('#tar_mt').prop('disabled', true);
                    } else if (select_tipo_cliente == 4) {
                        document.getElementById("proveedorUbica").style.display = "block";
                        document.getElementById("sucursalUbica").style.display = "none";
                        document.getElementById("cliente_empresarial").style.display = "none";
                        document.getElementById("cliente_normal").style.display = "none";

                        $('#cliente_mt').prop('disabled', true);
                        $('#cod_mt').prop('disabled', true);
                        $('#nit_mt').prop('disabled', true);
                        $('#tar_mt').prop('disabled', true);
                    } else {
                        document.getElementById("proveedorUbica").style.display = "none";
                        document.getElementById("cliente_empresarial").style.display = "none";
                        document.getElementById("cliente_normal").style.display = "none";
                        document.getElementById("sucursalUbica").style.display = "none";

                        $('#cliente_mt').prop('disabled', false);
                        $('#cod_mt').prop('disabled', false);
                        $('#nit_mt').prop('disabled', false);
                        $('#tar_mt').prop('disabled', false);
                    }
                }


                var limpiar = () => document.getElementById("search_location").value = "";
            </script>
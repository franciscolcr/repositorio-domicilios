<?php
require_once('../data/conexion.php');
$fecha_actual = date('Y-m-d');
require_once('../layouts/header.php');
$iden = $_COOKIE['tp_id'];

$sv = file_get_contents("C:\PUBLIC\SERVER.ID", "r");
$servidora = trim($sv);

$servidora = "SELECT *FROM PRG.servidorasm where Nombre='$servidora'";
$serv = odbc_exec($conn, $servidora);
while ($filasv = odbc_fetch_array($serv)) {
    $servi = $filasv['ServidoraId'];
}
//echo $servi;
?>
<style>

</style>
<link href="../js/dist/css/select2.min.css" rel="stylesheet" />

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php if ($_COOKIE['tp_id'] == 2) {
            include("../layouts/menu_sales.php");
        } else if ($_COOKIE['tp_id'] == 3) {
            include("../layouts/menu_dispatch.php");
        } ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <?php include("../layouts/navbar.php") ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    </div>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h4>Crear Nuevo Viaje</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="motivo">Tipo de Domicilio</label>
                                    <select name="motivos" id="motivo" class="form-control form-control-sm" name="motivo" onchange="valor_motivo();">
                                        <option value="0">Seleccion el tipo de domicilio</option>
                                        <?php $sql1 = "select * from prg.mt_motivos where mt_estado = 0 and mt_iden = '$iden' ";
                                        $ds = odbc_exec($conn, $sql1);
                                        while ($fila = odbc_fetch_array($ds)) { ?>
                                            <option value="<?php echo $fila['mt_id'] ?>"><?php echo $fila['mt_motivo'] ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="divVenta" style="display: none;">
                                <form id="newDomicilio" name="newDomicilio" method="POST" autocomplete="OFF">
                                    <!--new domicilioooo-->
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero Documento</label> <br>
                                            <select class="form-control form-control-sm" name="ndoc" id="ndoc" onChange="ver(this.value);">
                                                <option value="0" selected>Seleccione un documento</option>
                                                <?php $sql1 = "select PLUDocCliente,Codigo,Tipo,Fecha from prg.docclientesm where fecha between '" . date("Y-m-d", strtotime($fecha_actual . "- 30 days")) . "' and '" . date("Y-m-d", strtotime($fecha_actual . "+ 1 days")) . "' and  plugestor = " . $_COOKIE['codigo_gestor'] . " and Cerrado = 'T' and tipo in ('FC','CF') order by fecha desc";;
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['PLUDocCliente'] ?>">
                                                        <?php echo $fila['Codigo'] . " - " . $fila['Tipo'] . " - " . date('d/m/Y', strtotime($fila['Fecha'])) ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <table id="footable-res2" class="table table-bordered" width="100%" cellspacing="0">
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">

                                            <label for="ndoc">Nombre de Cliente</label> <br>
                                            <select class="form-control form-control-sm" name="cli_id" id="cli_id" onchange="trae_datos();">
                                                <option value="0" selected>Seleccione el Cliente</option>
                                                <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0 AND cli_tipo<4";
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['cli_id'] ?>">
                                                        <?php echo $fila['cli_nombre'] ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Direccion de Envio</label> <br>
                                            <select class="form-control form-control-sm" name="direccioncliente" id="direccioncliente">
                                                <option value="0" selected>Seleccione una direccion </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero de Contacto</label> <br>
                                            <input type="text" class="form-control form-control-sm" id="telefonocliente" name="telefonocliente">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="observacion">Observaciones:</label>
                                            <textarea class="form-control" name="observacion" id="observacion" cols="30" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button id="btventa" type="button" class="btn btn-primary" onclick="send_venta();">Guardar Domicilio</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- Traslados -->
                            <div id="divTraslado" style="display: none;">
                                <form id="form_traslado" name="form_traslado" method="POST" autocomplete="OFF">
                                    <!--new traslado -->
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Sucursal Destino</label>
                                            <select class="form-control form-control-sm" name="tsucursal" id="tsucursal" onchange="trae_datos();">
                                                <option value="0" selected>Seleccione una sucursal</option>
                                                <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0 and cli_tipo=3";
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['cli_id'] ?>">
                                                        <?php echo utf8_encode($fila['cli_nombre']) ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Direccion de Envio</label> <br>
                                            <select class="form-control form-control-sm" name="direccionSuc" id="direccionSuc">
                                                <option value="0" selected>Seleccione una direccion </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero de Traslado</label>
                                            <select class="form-control form-control-sm" name="ntraslado" id="ntraslado" onChange="trae_detalle_traslado(this.value);">
                                                <option value="0" selected>Seleccione un Numero de Traslado</option>
                                                <?php
                                                $sql1 = "SELECT PLUTraslado,Fecha,ServidoraId FROM prg.trasladosm WHERE Fecha BETWEEN '" . date("Y-m-d", strtotime($fecha_actual . "- 5 days")) . "' and '" . $fecha_actual . "' AND LiberadoOrigen = 'T' AND ServidoraId='$servi' order by Fecha desc";

                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['PLUTraslado'] ?>">
                                                        <?php echo utf8_encode($fila['PLUTraslado']) . "-" . date('d/m/Y', strtotime($fila['Fecha'])) ?>
                                                    </option>
                                                <?php }

                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero de Contacto</label> <br>
                                            <input type="text" class="form-control form-control-sm" id="telefonoSuc" name="telefonoSuc">
                                        </div>
                                    </div>
                                    <!--fin bodegero-->
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <table id="table_traslado" class="table table-bordered" width="100%" cellspacing="0">
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="otraslado">Observaciones</label>
                                            <textarea class="form-control" name="otraslado" id="otraslado" cols="30" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-primary" onclick="send_traslado();">Guardar Domicilio</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!-- RECOGER MUESTRAS -->
                            <div id="divMuestras" style="display: none;">
                                <form id="newDomicilio_muestras" name="newDomicilio_muestras" method="POST" autocomplete="OFF">
                                    <!--recoger muestra -->

                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Nombre de Cliente</label> <br>
                                            <select class="form-control form-control-sm" name="clientes_muestras" id="clientes_muestras" onchange="trae_datos_muestra();">
                                                <option value="0" selected>Seleccione el Cliente</option>
                                                <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0 AND cli_tipo<4";
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['cli_id'] ?>">
                                                        <?php echo $fila['cli_nombre'] ?></option>
                                                <?php } ?>

                                            </select>

                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Direccion de Envio</label> <br>
                                            <select class="form-control form-control-sm" name="direccioncliente_muestra" id="direccioncliente_muestra">
                                                <option value="0" selected>Seleccione una direccion </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero de Contacto</label> <br>
                                            <input type="text" class="form-control form-control-sm" id="telefonocliente_muestra" name="telefonocliente_muestra">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="observacion">Observaciones:</label>
                                            <textarea class="form-control" name="observacion_muestra" id="observacion_muestra" cols="30" rows="3"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-primary" onclick="send_muestra();">Guardar Domicilio</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!-- COMPRAS EN PLAZA -->
                            <div id="divComprasPlaza" style="display: none;">
                                <!--new compra en plaza -->
                                <form method="POST" id="newDomicilio_compras" name="newDomicilio_compras" autocomplet="OFF">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="odc">Ingrese el Numero de Orden:</label>
                                            <input type="number" class="form-control" id="n_order" name="n_order">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">

                                            <label for="ndoc">Nombre de Cliente</label> <br>
                                            <select class="form-control form-control-sm" name="clientes_compras" id="clientes_compras" onchange="trae_datos_compras();">
                                                <option value="0" selected>Seleccione el Cliente</option>
                                                <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0 and  cli_tipo=4";
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['cli_id'] ?>">
                                                        <?php echo $fila['cli_nombre'] ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Direccion de Envio</label> <br>
                                            <select class="form-control form-control-sm" name="direccioncliente_compras" id="direccioncliente_compras">
                                                <option value="0" selected>Seleccione una direccion </option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="otraslado">Observaciones</label>
                                            <textarea class="form-control" name="ocompras" id="ocompras" cols="30" rows="3"></textarea>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero de Contacto</label> <br>
                                            <input type="text" class="form-control form-control-sm" id="telefonocliente_compras" name="telefonocliente_compras">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-primary" onclick="send_cplaza();">Guardar Domicilio</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!--fin compras en plaza-->
                            <!-- taller -->
                            <div id="divTaller" style="display: none;">
                                <!--new compra en plaza -->
                                <form method="POST" id="newDomicilio_taller" name="newDomicilio_taller" autocomplet="OFF">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="odc">Ingrese el Numero de Orden:</label>
                                            <input type="number" class="form-control" id="n_order_taller" name="n_order_taller">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">

                                            <label for="ndoc">Nombre de Cliente</label> <br>
                                            <select class="form-control form-control-sm" name="clientes_taller" id="clientes_taller" onchange="trae_datos_taller();">
                                                <option value="0" selected>Seleccione el Cliente</option>
                                                <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0 and  cli_tipo=4";
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['cli_id'] ?>">
                                                        <?php echo $fila['cli_nombre'] ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Direccion de Envio</label> <br>
                                            <select class="form-control form-control-sm" name="direccioncliente_taller" id="direccioncliente_taller">
                                                <option value="0" selected>Seleccione una direccion </option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="otraslado">Observaciones</label>
                                            <textarea class="form-control" name="otaller" id="otaller" cols="30" rows="3"></textarea>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero de Contacto</label> <br>
                                            <input type="text" class="form-control form-control-sm" id="telefonocliente_taller" name="telefonocliente_taller">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-primary" onclick="send_taller();">Guardar Domicilio</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!--fin taller-->
                            <!-- Domicilios de cobros -->
                            <div id="divCobro" style="display: none;">
                                <form method="POST" id="newDomicilio_cobro" name="newDomicilio_cobro" autocomplet="OFF">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Nombre de Cliente</label> <br>
                                            <select class="form-control form-control-sm" name="clientes_cobro" id="clientes_cobro" onchange="trae_datos_cobro();">
                                                <option value="0" selected>Seleccione el Cliente</option>
                                                <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0 AND cli_tipo<4";
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['cli_id'] ?>">
                                                        <?php echo $fila['cli_nombre'] ?></option>
                                                <?php } ?>

                                            </select>

                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Direccion de Envio</label> <br>
                                            <select class="form-control form-control-sm" name="direccioncliente_cobro" id="direccioncliente_cobro">
                                                <option value="0" selected>Seleccione una direccion </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero de Contacto</label> <br>
                                            <input type="text" class="form-control form-control-sm" id="telefonocliente_cobro" name="telefonocliente_cobro">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="otraslado">Observaciones Cobro</label>
                                            <textarea class="form-control" name="observacion_cobro" id="observacion_cobro" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-primary" onclick="send_cobro();">Guardar Domicilio</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- domicilio administrativo -->
                            <div id="divAdmin" style="display: none;">
                                <form id="form_Admin" method="POST" autocomplete="OFF">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Sucursal Destino</label>
                                            <select class="form-control form-control-sm" name="clientes_Admin" id="clientes_Admin" onchange="trae_datos_admin();">
                                                <option value="0" selected>Seleccione una sucursal</option>
                                                <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0 and cli_tipo=3";
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['cli_id'] ?>">
                                                        <?php echo utf8_encode($fila['cli_nombre']) ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Direccion de Envio</label> <br>
                                            <select class="form-control form-control-sm" name="direccioncliente_Admin" id="direccioncliente_Admin">
                                                <option value="0" selected>Seleccione una direccion </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero de Contacto</label> <br>
                                            <input type="text" class="form-control form-control-sm" id="telefonocliente_Admin" name="telefonocliente_Admin">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="otraslado">Observaciones</label>
                                            <textarea class="form-control" name="observaciones_Admin" id="observaciones_Admin" cols="30" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-primary" onclick="send_Admin();">Guardar Domicilio</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!--retorno-->
                            <!-- domicilio retorno -->
                            <div id="divRetorno" style="display: none;">
                                <form id="form_Retorno" method="POST" autocomplete="OFF">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Destino Retorno</label>
                                            <select class="form-control form-control-sm" name="clientes_Retorno" id="clientes_Retorno" onchange="trae_datos_Retorno();">
                                                <option value="0" selected>Seleccione una sucursal</option>
                                                <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0 and cli_tipo=3";
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['cli_id'] ?>">
                                                        <?php echo utf8_encode($fila['cli_nombre']) ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Direccion de Retorno</label> <br>
                                            <select class="form-control form-control-sm" name="direccioncliente_Retorno" id="direccioncliente_Retorno">
                                                <option value="0" selected>Seleccione una direccion </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero de Contacto</label> <br>
                                            <input type="text" class="form-control form-control-sm" id="telefonocliente_Retorno" name="telefonocliente_Retorno">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="otraslado">Observaciones</label>
                                            <textarea class="form-control" name="observaciones_Retorno" id="observaciones_Retorno" cols="30" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-primary" onclick="send_Retorno();">Guardar Domicilio</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!-- domicilio Ruta -->
                            <div id="divRuta" style="display: none;">
                                <form method="POST" id="newRuta" name="newRuta" autocomplet="OFF">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Nombre de Cliente</label> <br>
                                            <select class="form-control form-control-sm" name="clientes_ruta" id="clientes_ruta" onchange="trae_datos_ruta();">
                                                <option value="0" selected>Seleccione el Cliente</option>
                                                <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0 AND cli_tipo<4";
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['cli_id'] ?>">
                                                        <?php echo $fila['cli_nombre'] ?></option>
                                                <?php } ?>

                                            </select>

                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Direccion de Envio</label> <br>
                                            <select class="form-control form-control-sm" name="direccioncliente_ruta" id="direccioncliente_ruta">
                                                <option value="0" selected>Seleccione una direccion </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero de Contacto</label> <br>
                                            <input type="text" class="form-control form-control-sm" id="telefonocliente_ruta" name="telefonocliente_ruta">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="otraslado">Observaciones Cobro</label>
                                            <textarea class="form-control" name="observacion_ruta" id="observacion_ruta" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-primary" onclick="send_ruta();">Guardar
                                                Domicilio</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="divDevolucion" style="display: none;">
                                <form id="newDomicilio_devolucion" name="newDomicilio_devolucion" method="POST" autocomplete="OFF">
                                    <!--new domicilioooo-->
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero Documento</label> <br>
                                            <select class="form-control form-control-sm" name="ndoc3" id="ndoc3" onChange="ver2(this.value);">
                                                <option value="0" selected>Seleccione un documento</option>
                                                <?php $sql1 = "select PLUDocCliente,Codigo,Tipo,Fecha from prg.docclientesm where fecha between '" . date("Y-m-d", strtotime($fecha_actual . "- 7 days")) . "' and '" . date("Y-m-d", strtotime($fecha_actual . "+ 1 days")) . "' and  plugestor = " . $_COOKIE['codigo_gestor'] . " and impreso = 'T' and tipo in ('DV', 'NC') order by fecha desc";;
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['PLUDocCliente'] ?>">
                                                        <?php echo $fila['Codigo'] . " - " . $fila['Tipo'] . " - " . date('d/m/Y', strtotime($fila['Fecha'])) ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <table id="footable-res3" class="table table-bordered" width="100%" cellspacing="0">
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">

                                            <label for="ndoc">Nombre de Cliente</label> <br>
                                            <select class="form-control form-control-sm" name="cli_id3" id="cli_id3" onchange="trae_datos_devo();">
                                                <option value="0" selected>Seleccione el Cliente</option>
                                                <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0 AND cli_tipo<4";
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['cli_id'] ?>">
                                                        <?php echo $fila['cli_nombre'] ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Direccion de Envio</label> <br>
                                            <select class="form-control form-control-sm" name="direccioncliente3" id="direccioncliente3">
                                                <option value="0" selected>Seleccione una direccion </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero de Contacto</label> <br>
                                            <input type="text" class="form-control form-control-sm" id="telefonocliente3" name="telefonocliente3">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="observacion">Observaciones:</label>
                                            <textarea class="form-control" name="observacion3" id="observacion3" cols="30" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-primary" onclick="send_devolucion();">Guardar Domicilio</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div id="divPPendiente" style="display: none;">
                                <form method="POST" id="newDomicilio_ppendiente" name="newDomicilio_ppendiente" autocomplet="OFF">
                                    <!--new pedido pendiente -->
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="ndoc">Numero Documento</label> <br>
                                            <select class="form-control form-control-sm" name="ndoc2" id="ndoc2" onChange="trae_productos(this.value);">
                                                <option value="0" selected>Seleccione un documento</option>
                                                <?php $sql1 = "SELECT * FROM prg.docclientesm WHRE fecha BETWEEN '" . date("Y-m-d", strtotime($fecha_actual . "- 1 days")) . "' AND '" . $fecha_actual . "' AND  pludivision = " . $_COOKIE['sucursal'] . "";;
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['PLUDocCliente'] ?>">
                                                        <?php echo $fila['PLUDocCliente'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <table id="table_productos" class="table table-bordered" width="100%" cellspacing="0">
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label for="ndoc">Nombre de Cliente</label> <br>
                                            <select class="form-control form-control-sm" name="cli_id_p" id="cli_id_p" onchange="trae_datos2();">
                                                <option value="0" selected>Seleccione el Cliente</option>
                                                <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0";;
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) { ?>
                                                    <option value="<?php echo $fila['cli_id'] ?>">
                                                        <?php echo $fila['cli_nombre'] ?></option>
                                                <?php }
                                                odbc_close($conn); ?>

                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="ndoc">Direccion de Envio</label> <br>
                                            <select class="form-control form-control-sm" name="direccioncliente_ppendiente" id="direccioncliente_ppendiente">
                                                <option value="0" selected>Seleccione una Direccion </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="ndoc">Numero de Contacto</label> <br>
                                            <input type="text" class="form-control form-control-sm" id="telefonocliente_ppendiente" name="telefonocliente_ppendiente">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="otraslado">Observaciones Pedido Pendiente</label>
                                            <textarea class="form-control" name="oppendiente" id="oppendiente" cols="30" rows="10" placeholder="-Que producto lleva..."></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-primary" onclick="send_ppendiente();">Guardar Domicilio</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="divObanco" style="display: none;">
                                <form method="POST" id="newDomicilio_obanco" name="newDomicilio_obanco" autocomplet="OFF">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="otraslado">Observaciones Banco</label>
                                            <textarea class="form-control" name="obanco" id="obanco" cols="30" rows="10" placeholder="-Cambio de monedas.&#10-Pagos."></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-primary" onclick="send_banco();">Guardar Domicilio</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->


            <?php
            require_once('../layouts/foother.php');
            ?>

            <script>
                //select para select2
                $(document).ready(function() {
                    $('#ndoc').select2({
                        theme: 'classic',
                        width: '100%',
                        placeholder: "Selecccione un documento",
                        allowClear: true
                    });
                    $('#ndoc3').select2({
                        theme: 'classic',
                        width: '100%',
                        placeholder: "Selecccione un documento",
                        allowClear: true
                    });
                    $('#direccioncliente3').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#tsucursal').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#bodegero').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#cli_id').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#cli_id3').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#direccioncliente').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#ntraslado').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $("#direccionSuc").select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#clientes_muestras').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#direccioncliente_muestra').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#clientes_compras').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#direccioncliente_compras').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#clientes_cobro').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#clientes_ruta').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#direccioncliente_ruta').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#direccioncliente_cobro').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#clientes_Admin').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $("#direccioncliente_Admin").select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#clientes_Retorno').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $("#direccioncliente_Retorno").select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#ndoc2').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#cli_id_p').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#direccioncliente_ppendiente').select2({
                        theme: 'classic',
                        width: '100%'
                    });

                    $('#clientes_taller').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#direccioncliente_taller').select2({
                        theme: 'classic',
                        width: '100%'
                    });
                });

                //muestra y oculta los formularios correspondientes
                
                function valor_motivo() {
                    var motivo = document.getElementById('motivo').value;
                    var divVenta = document.getElementById("divVenta");
                    var divTraslado = document.getElementById("divTraslado");
                    var divMuestras = document.getElementById("divMuestras");
                    var divComprasPlaza = document.getElementById("divComprasPlaza");
                    var divCobro = document.getElementById("divCobro");
                    var divRuta = document.getElementById("divRuta");
                    var divDevolucion = document.getElementById("divDevolucion");
                    var divPPendiente = document.getElementById("divPPendiente");
                    var divObanco = document.getElementById("divObanco");
                    var divAdmin = document.getElementById("divAdmin");
                    var divTaller = document.getElementById("divTaller");
                    var divRetorno = document.getElementById("divRetorno");


                    if (motivo == 2) {
                        divVenta.style.display = 'block';
                        divTraslado.style.display = 'none';
                        divMuestras.style.display = 'none';
                        divComprasPlaza.style.display = 'none';
                        divDevolucion.style.display = 'none';
                        divCobro.style.display = 'none';
                        divAdmin.style.display = 'none';
                        divTaller.style.display = 'none';
                        divRetorno.style.display = 'none';
                    } else if (motivo == 11) {
                        divTraslado.style.display = 'block';
                        divVenta.style.display = 'none';
                        divMuestras.style.display = 'none';
                        divComprasPlaza.style.display = 'none';
                        divDevolucion.style.display = 'none';
                        divCobro.style.display = 'none';
                        divAdmin.style.display = 'none';
                        divTaller.style.display = 'none';
                        divRetorno.style.display = 'none';
                    } else if (motivo == 6) {
                        divMuestras.style.display = 'block';
                        divVenta.style.display = 'none';
                        divTraslado.style.display = 'none';
                        divComprasPlaza.style.display = 'none';
                        divDevolucion.style.display = 'none';
                        divCobro.style.display = 'none';
                        divAdmin.style.display = 'none';
                        divTaller.style.display = 'none';
                        divRetorno.style.display = 'none';

                    } else if (motivo == 7) {
                        divMuestras.style.display = 'none';
                        divVenta.style.display = 'none';
                        divTraslado.style.display = 'none';
                        divComprasPlaza.style.display = 'block';
                        divDevolucion.style.display = 'none';
                        divCobro.style.display = 'none';
                        divAdmin.style.display = 'none';
                        divTaller.style.display = 'none';
                        divRetorno.style.display = 'none';
                    } else if (motivo == 1) {
                        divCobro.style.display = 'block';
                        divMuestras.style.display = 'none';
                        divVenta.style.display = 'none';
                        divTraslado.style.display = 'none';
                        divComprasPlaza.style.display = 'none';
                        divDevolucion.style.display = 'none';
                        divPPendiente.style.display = 'none';
                        divObanco.style.display = 'none';
                        divAdmin.style.display = 'none';
                        divTaller.style.display = 'none';
                        divRetorno.style.display = 'none';
                    } else if (motivo == 4) {
                        divDevolucion.style.display = 'block';
                        divCobro.style.display = 'none';
                        divMuestras.style.display = 'none';
                        divVenta.style.display = 'none';
                        divTraslado.style.display = 'none';
                        divComprasPlaza.style.display = 'none';
                        divPPendiente.style.display = 'none';
                        divObanco.style.display = 'none';
                        divAdmin.style.display = 'none';
                        divTaller.style.display = 'none';
                        divRetorno.style.display = 'none';
                    } else if (motivo == 5) {
                        divDevolucion.style.display = 'none';
                        divCobro.style.display = 'none';
                        divMuestras.style.display = 'none';
                        divVenta.style.display = 'none';
                        divTraslado.style.display = 'none';
                        divComprasPlaza.style.display = 'none';
                        divPPendiente.style.display = 'block';
                        divAdmin.style.display = 'none';
                        divTaller.style.display = 'none';
                        divRetorno.style.display = 'none';
                    } else if (motivo == 9) {
                        divDevolucion.style.display = 'none';
                        divCobro.style.display = 'none';
                        divMuestras.style.display = 'none';
                        divVenta.style.display = 'none';
                        divTraslado.style.display = 'none';
                        divComprasPlaza.style.display = 'none';
                        divPPendiente.style.display = 'none';
                        divAdmin.style.display = 'block';
                        divTaller.style.display = 'none';
                        divRetorno.style.display = 'none';
                    } else if (motivo == 10) {
                        divDevolucion.style.display = 'none';
                        divCobro.style.display = 'none';
                        divMuestras.style.display = 'none';
                        divVenta.style.display = 'none';
                        divTraslado.style.display = 'none';
                        divComprasPlaza.style.display = 'none';
                        divPPendiente.style.display = 'none';
                        divObanco.style.display = 'block';
                        divAdmin.style.display = 'none';
                        divTaller.style.display = 'none';
                        divRetorno.style.display = 'none';
                    } else if (motivo == 12) {
                        divDevolucion.style.display = 'none';
                        divCobro.style.display = 'none';
                        divRuta.style.display = 'block';
                        divMuestras.style.display = 'none';
                        divVenta.style.display = 'none';
                        divTraslado.style.display = 'none';
                        divComprasPlaza.style.display = 'none';
                        divPPendiente.style.display = 'none';
                        divObanco.style.display = 'none';
                        divAdmin.style.display = 'none';
                        divTaller.style.display = 'none';
                        divRetorno.style.display = 'none';
                    } else if (motivo == 14) {
                        divDevolucion.style.display = 'none';
                        divCobro.style.display = 'none';
                        divMuestras.style.display = 'none';
                        divVenta.style.display = 'none';
                        divTraslado.style.display = 'none';
                        divComprasPlaza.style.display = 'none';
                        divPPendiente.style.display = 'none';
                        divObanco.style.display = 'none';
                        divAdmin.style.display = 'none';
                        divRetorno.style.display = 'none';
                        divTaller.style.display = 'block';

                    } else if (motivo == 15) {
                        divDevolucion.style.display = 'none';
                        divCobro.style.display = 'none';
                        divMuestras.style.display = 'none';
                        divVenta.style.display = 'none';
                        divTraslado.style.display = 'none';
                        divComprasPlaza.style.display = 'none';
                        divPPendiente.style.display = 'none';
                        divObanco.style.display = 'none';
                        divAdmin.style.display = 'none';
                        divTaller.style.display = 'none';
                        divRetorno.style.display = 'block';
                    } else {
                        divVenta.style.display = 'none';
                    }
                }

                //llena el campo del telefono del cliente
                function trae_datos() {
                    var cli_id = document.getElementById('cli_id').value;
                    var tsucursal = document.getElementById('tsucursal').value;
                    /*var posicion=document.getElementById('ndoc').options.selectedIndex; 
   	var id_doc=document.getElementById('ndoc').options[posicion].value;*/

                    if (cli_id) {
                        $.ajax({
                            dataType: 'json',
                            type: 'POST',
                            url: '../data/trae_info_cliente.php',
                            data: {
                                cli_id: cli_id
                            },
                            success: function(html) {
                                $('#telefonocliente').val(html.telefono);
                            }
                        });
                    }
                    // fixed jeu
                    if (tsucursal) {
                        $.ajax({
                            dataType: 'json',
                            type: 'POST',
                            url: '../data/trae_info_cliente.php',
                            data: {
                                cli_id: tsucursal
                            },
                            success: function(html) {
                                $('#telefonoSuc').val(html.telefono);
                            }
                        });
                        $.ajax({
                            url: "../data/trae_direccion.php",
                            type: "POST",
                            data: {
                                cli_id: tsucursal
                            },
                            success: function(opciones) {
                                $("#direccionSuc").html(opciones);
                            }
                        })
                    }
                }

                /**
                 * clientes_muestras
                 */
                function trae_datos_muestra() {
                    var cli_id = document.getElementById('clientes_muestras').value;
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: '../data/trae_info_cliente.php',
                        data: {
                            cli_id: cli_id
                        },
                        success: function(html) {
                            $('#telefonocliente_muestra').val(html.telefono);
                        }
                    });
                }
                //Llena las direcciones del cliente muestra
                $(document).ready(function() {
                    $("#clientes_muestras").change(function() {
                        $.ajax({
                            url: "../data/trae_direccion.php",
                            type: "POST",
                            data: "cli_id=" + $("#clientes_muestras").val(),
                            success: function(opciones) {
                                $("#direccioncliente_muestra").html(opciones);
                            }
                        })
                    });
                });
                /**@abstract */

                /**
                 * clientes_muestras
                 */
                function trae_datos_compras() {
                    var cli_id = document.getElementById('clientes_compras').value;
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: '../data/trae_info_cliente.php',
                        data: {
                            cli_id: cli_id
                        },
                        success: function(html) {
                            $('#telefonocliente_compras').val(html.telefono);
                        }
                    });
                }
                //Llena las direcciones del cliente muestra
                $(document).ready(function() {
                    $("#clientes_compras").change(function() {
                        $.ajax({
                            url: "../data/trae_direccion.php",
                            type: "POST",
                            data: "cli_id=" + $("#clientes_compras").val(),
                            success: function(opciones) {
                                $("#direccioncliente_compras").html(opciones);
                            }
                        })
                    });
                });
                /* taller */
                function trae_datos_taller() {
                    var cli_id = document.getElementById('clientes_taller').value;
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: '../data/trae_info_cliente.php',
                        data: {
                            cli_id: cli_id
                        },
                        success: function(html) {
                            $('#telefonocliente_taller').val(html.telefono);
                        }
                    });
                }
                //Llena las direcciones del cliente muestra
                $(document).ready(function() {
                    $("#clientes_taller").change(function() {
                        $.ajax({
                            url: "../data/trae_direccion.php",
                            type: "POST",
                            data: "cli_id=" + $("#clientes_taller").val(),
                            success: function(opciones) {
                                $("#direccioncliente_taller").html(opciones);
                            }
                        })
                    });
                });

                /**
                ::::::::::::::::::::::::::::::::::::::domicilios Cobros:::::::::::::::::::::::::::::::::::::::::::::::::::::
                 */
                function trae_datos_cobro() {
                    var cli_id = document.getElementById('clientes_cobro').value;
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: '../data/trae_info_cliente.php',
                        data: {
                            cli_id: cli_id
                        },
                        success: function(html) {
                            $('#telefonocliente_cobro').val(html.telefono);
                        }
                    });
                }
                //Llena las direcciones del cliente muestra
                $(document).ready(function() {
                    $("#clientes_cobro").change(function() {
                        $.ajax({
                            url: "../data/trae_direccion.php",
                            type: "POST",
                            data: "cli_id=" + $("#clientes_cobro").val(),
                            success: function(opciones) {
                                $("#direccioncliente_cobro").html(opciones);
                            }
                        })
                    });
                });
                /**
                 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: domicilios ruta:::::::::::::::::::::::::::::::::::::::
                 */
                function trae_datos_ruta() {
                    var cli_id = document.getElementById('clientes_ruta').value;
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: '../data/trae_info_cliente.php',
                        data: {
                            cli_id: cli_id
                        },
                        success: function(html) {
                            $('#telefonocliente_ruta').val(html.telefono);
                        }
                    });
                }
                $(document).ready(function() {
                    $("#clientes_ruta").change(function() {
                        $.ajax({
                            url: "../data/trae_direccion.php",
                            type: "POST",
                            data: "cli_id=" + $("#clientes_ruta").val(),
                            success: function(opciones) {
                                $("#direccioncliente_ruta").html(opciones);
                            }
                        })
                    });
                });


                /**
                ::::::::::::::::::::::::::::::::::::::termina domicilios Cobros:::::::::::::::::::::::::::::::::::::::::::::::::::::
                 */
                /**
                ::::::::::::::::::::::::::::::::::::::domicilios Administrativos:::::::::::::::::::::::::::::::::::::::::::::::::::::
                 */
                function trae_datos_admin() {
                    var cli_id = document.getElementById('clientes_Admin').value;
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: '../data/trae_info_cliente.php',
                        data: {
                            cli_id: cli_id
                        },
                        success: function(html) {
                            $('#telefonocliente_Admin').val(html.telefono);
                        }
                    });
                }
                //Llena las direcciones del cliente muestra
                $(document).ready(function() {
                    $("#clientes_Admin").change(function() {
                        $.ajax({
                            url: "../data/trae_direccion.php",
                            type: "POST",
                            data: "cli_id=" + $("#clientes_Admin").val(),
                            success: function(opciones) {
                                $("#direccioncliente_Admin").html(opciones);
                            }
                        })
                    });
                });


                /**
                ::::::::::::::::::::::::::::::::::::::termina domicilios Administrativos:::::::::::::::::::::::::::::::::::::::::::::::::::::
                 */

                /**
                ::::::::::::::::::::::::::::::::::::::domicilios Cobros:::::::::::::::::::::::::::::::::::::::::::::::::::::
                 */
                function trae_datos_devo() {
                    var cli_id = document.getElementById('cli_id3').value;
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: '../data/trae_info_cliente.php',
                        data: {
                            cli_id: cli_id
                        },
                        success: function(html) {
                            $('#telefonocliente3').val(html.telefono);
                        }
                    });
                }
                //Llena las direcciones del cliente muestra
                $(document).ready(function() {
                    $("#cli_id3").change(function() {
                        $.ajax({
                            url: "../data/trae_direccion.php",
                            type: "POST",
                            data: "cli_id=" + $("#cli_id3").val(),
                            success: function(opciones) {
                                $("#direccioncliente3").html(opciones);
                            }
                        })
                    });
                });


                /**
                ::::::::::::::::::::::::::::::::::::::termina domicilios Cobros:::::::::::::::::::::::::::::::::::::::::::::::::::::
                 */
                //llena el campo del telefono del cliente pedido pendiente
                function trae_datos2() {
                    var cli_id = document.getElementById('cli_id_p').value;

                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: '../data/trae_info_cliente.php',
                        data: {
                            cli_id: cli_id
                        },
                        success: function(html) {
                            $('#telefonocliente_ppendiente').val(html.telefono);
                        }
                    });
                }

                //Llena las direcciones del cliente
                $(document).ready(function() {
                    $("#cli_id").change(function() {
                        $.ajax({
                            url: "../data/trae_direccion.php",
                            type: "POST",
                            data: "cli_id=" + $("#cli_id").val(),
                            success: function(opciones) {
                                console.log(opciones);
                                $("#direccioncliente").html(opciones);
                            }
                        })
                    });
                });

                //cliente para retorno
                //Llena las direcciones del cliente
                $(document).ready(function() {
                    $("#clientes_Retorno").change(function() {
                        $.ajax({
                            url: "../data/trae_direccion.php",
                            type: "POST",
                            data: "cli_id=" + $("#clientes_Retorno").val(),
                            success: function(opciones) {
                                console.log(opciones);
                                $("#direccioncliente_Retorno").html(opciones);
                            }
                        })
                    });
                });
                //trae telefono retorno
                function trae_datos_Retorno() {
                    var cli_id = document.getElementById('clientes_Retorno').value;

                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: '../data/trae_info_cliente.php',
                        data: {
                            cli_id: cli_id
                        },
                        success: function(html) {
                            $('#telefonocliente_Retorno').val(html.telefono);
                        }
                    });
                }

                //Llena las direcciones del cliente pedido pendiente
                $(document).ready(function() {
                    $("#cli_id_p").change(function() {
                        $.ajax({
                            url: "../data/trae_direccion.php",
                            type: "POST",
                            data: "cli_id_p=" + $("#cli_id_p").val(),
                            success: function(opciones) {
                                $("#direccioncliente_ppendiente").html(opciones);
                            }
                        })
                    });
                });

                //trae los productos del documento de una venta
                function ver() {
                    var ndoc = $('#ndoc').val();
                    var tipo = $('#motivo').val();
                    $.ajax({
                        data: {
                            ndoc: ndoc,
                            tipo: tipo
                        },
                        url: '../data/trae_detalle_doc.php',
                        dataType: 'html',
                        type: 'POST',
                        beforeSend: function() {
                            $('#footable-res2').html(
                                '<center><img alt="Cargando..." src="../img/searching.gif" /></center>');
                        },
                        success: function(html) {
                            $('#footable-res2').html(html);

                        }
                    });
                }

                //trae los productos del documento de una devolucion
                function ver2() {
                    var ndoc = $('#ndoc3').val();
                    var tipo = $('#motivo').val();
                    $.ajax({
                        data: {
                            ndoc: ndoc,
                            tipo: tipo
                        },
                        url: '../data/trae_detalle_doc.php',
                        dataType: 'html',
                        type: 'POST',
                        beforeSend: function() {
                            $('#footable-res3').html(
                                '<center><img alt="Cargando..." src="../img/searching.gif" /></center>');
                        },
                        success: function(html) {
                            $('#footable-res3').html(html);

                        }
                    });
                }

                //trae los productos del documento de un pendido pendiente
                function trae_productos() {
                    var ndoc = $('#ndoc2').val();
                    var tipo = $('#motivo').val();
                    $.ajax({
                        data: {
                            ndoc: ndoc,
                            tipo: tipo
                        },
                        url: '../data/trae_detalle_doc.php',
                        dataType: 'html',
                        type: 'POST',
                        beforeSend: function() {
                            $('#table_productos').html(
                                '<center><img alt="Cargando..." src="../img/searching.gif" /></center>');
                        },
                        success: function(html) {
                            $('#table_productos').html(html);

                        }
                    });
                }

                //guarda domicilio venta
                function send_venta() {
                    $("#btventa").attr('disabled', true);
                    var tipo = $('#motivo').val();
                    var ndoc = $('#ndoc').val();
                    var cli_id = $('#cli_id').val();
                    var direccion = $('#direccioncliente').val();
                    var telefono = $('#telefonocliente').val();
                    var observacion = $('#observacion').val();

                    if (ndoc == 0) {
                        bootbox.alert("Es necesario Seleccionar un Documento!", function() {
                            $("#ndoc").select2('open');

                        });
                    } else if (cli_id == 0) {
                        bootbox.alert("Es necesario Seleccionar un Cliente!", function() {
                            $("#cli_id").select2('open');
                        });

                    } else if (direccion == 0 || direccion == null) {
                        bootbox.confirm({
                            message: " Ups Cliente no Tiene direccion, Desea Agregar una?",
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function(result) {
                                if (result == true) {
                                    document.location.href = 'modify_cliente.php?id=' + cli_id;
                                }
                                // console.log('This was logged in the callback: ' + result);                                   
                            }
                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'html',
                            data: {
                                tipo: tipo,
                                ndoc: ndoc,
                                cli_id: cli_id,
                                direccion: direccion,
                                telefono: telefono,
                                observacion: observacion
                            },
                            url: '../data/model_create_domicilio.php',
                            success: function(res) {
                                if (res > 1) {
                                    $("#btventa").attr('disabled', false);
                                    bootbox.alert("Registro creado Exitosamente!!", function() {
                                        document.location.href = 'create_domicilios.php'
                                    });

                                } else {
                                    bootbox.alert(
                                        "!Ups Algo salio mal comunicarse con un Administrado!",
                                        function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                }

                            }
                        });
                    }

                }

                //trae los detalles del traslado
                function trae_detalle_traslado() {
                    var ntraslado = $('#ntraslado').val();
                    var tipo = $('#motivo').val();
                    var i = 1;
                    $.ajax({
                        data: {
                            ndoc: ntraslado,
                            tipo: tipo
                        },
                        url: '../data/trae_detalle_traslado.php',
                        dataType: 'html',
                        type: 'POST',
                        beforeSend: function() {
                            $('#table_traslado').html(
                                '<center><img alt="Cargando..." src="../img/searching.gif" /></center>'
                            );
                        },
                        success: function(html) {
                            $('#table_traslado').html(html);

                        }
                    });
                }

                //guarda domicilio traslado
                function send_traslado() {
                    var tipo = $('#motivo').val();
                    var sucursal = $('#tsucursal').val();
                    var ntraslado = $('#ntraslado').val();
                    var otraslado = $('#otraslado').val();
                    var bodegero = $('#bodegero').val();
                    var direccionSuc = $('#direccionSuc').val();

                    // alert(direccionSuc);

                    if (sucursal == 0) {
                        bootbox.alert("Seleccionar una Sucursal!", function() {
                            $("#tsucursal").select2('open');

                        });
                    } else if (direccionSuc == 0 || direccionSuc == null) {
                        bootbox.confirm({
                            message: " Ups Cliente no Tiene direccion, Desea Agregar una?",
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function(result) {
                                if (result == true) {
                                    document.location.href = 'modify_cliente.php?id=' + sucursal;
                                }
                                // console.log('This was logged in the callback: ' + result);                                   
                            }
                        });
                    } else if (ntraslado == 0) {
                        bootbox.alert("Es necesario Seleccionar un Documento!", function() {
                            $("#ntraslado").select2('open');
                        });
                    } else {

                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                tipo: tipo,
                                sucursal: sucursal,
                                ntraslado: ntraslado,
                                otraslado: otraslado,
                                direccionSuc: direccionSuc
                            },
                            url: '../data/model_create_domicilio.php',
                            success: function(res) {
                                if (res > 1) {

                                    bootbox.alert("Registro creado Exitosamente!", function() {
                                        document.location.href = 'create_domicilios.php'
                                    });

                                } else {
                                    bootbox.alert("!Algo salio mal comunicarse con un Administrado!",
                                        function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                }

                            }
                        });
                    }

                }

                //guarda domicilio recoger muestas jeu
                function send_muestra() {
                    var form = $("#newDomicilio_muestras").serialize();
                    var tipo = $('#motivo').val();
                    var clientes_muestras = $('#clientes_muestras').val();
                    var observaciones_muestras = $('#observacion_muestras').val();
                    var direccioncliente_muestra = $('#direccioncliente_muestra').val();
                    //console.log(form+ '&tipo=' + tipo);
                    if (clientes_muestras == 0) {
                        bootbox.alert("Seleccionar un cliente!", function() {
                            $("#clientes_muestras").select2('open');

                        });
                    } else if (direccioncliente_muestra == 0 || direccioncliente_muestra == null) {
                        bootbox.confirm({
                            message: " Ups Cliente no Tiene direccion, Desea Agregar una?",
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function(result) {
                                if (result == true) {
                                    document.location.href = 'modify_cliente.php?id=' +
                                        clientes_muestras;
                                }
                                // console.log('This was logged in the callback: ' + result);                                   
                            }
                        });
                    } else if (observaciones_muestras == "") {
                        bootbox.alert("Es necesario ingresar una descripcion!", function() {

                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'JSON',
                            data: form + '&tipo=' + tipo,
                            url: '../data/model_create_domicilio.php',
                            success: function(res) {
                                if (res > 1) {

                                    bootbox.alert("Registro creado Exitosamente!", function() {
                                        document.location.href = 'create_domicilios.php'
                                    });

                                } else {
                                    bootbox.alert("!Algo salio mal comunicarse con un Administrado!",
                                        function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                }

                            }
                        });
                    }

                }
                //guarda domicilio Administrativo jeu
                function send_Admin() {
                    var form = $("#form_Admin").serialize();
                    var tipo = $('#motivo').val();
                    var clientes_Admin = $('#clientes_Admin').val();
                    var direccioncliente_Admin = $('#direccioncliente_Admin').val();
                    var observaciones_Admin = $('#observaciones_Admin').val();
                    //console.log(form+ '&tipo=' + tipo);
                    if (clientes_Admin == 0) {
                        bootbox.alert("Seleccionar un cliente!", function() {
                            $("#clientes_Admin").select2('open');

                        });
                    } else if (direccioncliente_Admin == 0 || direccioncliente_Admin == null) {
                        bootbox.confirm({
                            message: " Ups Cliente no Tiene direccion, Desea Agregar una?",
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function(result) {
                                if (result == true) {
                                    document.location.href = 'modify_cliente.php?id=' + clientes_Admin;
                                }
                                // console.log('This was logged in the callback: ' + result);                                   
                            }
                        });
                    } else if (observaciones_Admin == "") {
                        bootbox.alert("Es necesario ingresar una descripcion!", function() {

                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'JSON',
                            data: form + '&tipo=' + tipo,
                            url: '../data/model_create_domicilio.php',
                            success: function(res) {
                                if (res > 1) {

                                    bootbox.alert("Registro creado Exitosamente!", function() {
                                        document.location.href = 'create_domicilios.php'
                                    });

                                } else {
                                    bootbox.alert("!Algo salio mal comunicarse con un Administrado!",
                                        function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                }

                            }
                        });
                    }

                }
                //guarda retorno

                function send_Retorno() {
                    var form = $("#form_Retorno").serialize();
                    var tipo = $('#motivo').val();
                    var clientes_Retorno = $('#clientes_Retorno').val();
                    var direccioncliente_Retorno = $('#direccioncliente_Retorno').val();
                    var observaciones_Retorno = $('#observaciones_Retorno').val();
                    //console.log(form+ '&tipo=' + tipo);
                    if (clientes_Retorno == 0) {
                        bootbox.alert("Seleccionar un cliente!", function() {
                            $("#clientes_Retorno").select2('open');

                        });
                    } else if (direccioncliente_Retorno == 0 || direccioncliente_Retorno == null) {
                        bootbox.confirm({
                            message: " Ups Cliente no Tiene direccion, Desea Agregar una?",
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function(result) {
                                if (result == true) {
                                    document.location.href = 'modify_cliente.php?id=' +
                                        clientes_Retorno;
                                }
                                // console.log('This was logged in the callback: ' + result);                                   
                            }
                        });
                    } else if (observaciones_Retorno == "") {
                        bootbox.alert("Es necesario ingresar una descripcion!", function() {

                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'JSON',
                            data: form + '&tipo=' + tipo,
                            url: '../data/model_create_domicilio.php',
                            success: function(res) {
                                if (res > 1) {

                                    bootbox.alert("Registro creado Exitosamente!", function() {
                                        document.location.href = 'create_domicilios.php'
                                    });

                                } else {
                                    bootbox.alert("!Algo salio mal comunicarse con un Administrado!",
                                        function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                }

                            }
                        });
                    }

                }
                //guarda domicilio Cobro jeu
                function send_cobro() {
                    var form = $("#newDomicilio_cobro").serialize();
                    var tipo = $('#motivo').val();
                    var clientes_cobro = $('#clientes_cobro').val();
                    var direccioncliente_cobro = $('#direccioncliente_cobro').val();
                    var observacion_cobro = $('#observacion_cobro').val();
                    //console.log(form+ '&tipo=' + tipo);
                    if (clientes_cobro == 0) {
                        bootbox.alert("Seleccionar un cliente!", function() {
                            $("#clientes_cobro").select2('open');

                        });
                    } else if (direccioncliente_cobro == 0 || direccioncliente_cobro == null) {
                        bootbox.confirm({
                            message: " Ups Cliente no Tiene direccion, Desea Agregar una?",
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function(result) {
                                if (result == true) {
                                    document.location.href = 'modify_cliente.php?id=' + clientes_cobro;
                                }
                                // console.log('This was logged in the callback: ' + result);                                   
                            }
                        });
                    } else if (observacion_cobro == "") {
                        bootbox.alert("Es necesario ingresar una descripcion!", function() {

                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'JSON',
                            data: form + '&tipo=' + tipo,
                            url: '../data/model_create_domicilio.php',
                            success: function(res) {
                                if (res > 1) {

                                    bootbox.alert("Registro creado Exitosamente!", function() {
                                        document.location.href = 'create_domicilios.php'
                                    });

                                } else {
                                    bootbox.alert("!Algo salio mal comunicarse con un Administrado!",
                                        function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                }

                            }
                        });
                    }

                }
                //guarda domicilio Ruta jeu
                function send_ruta() {
                    var form = $("#newRuta").serialize();
                    var tipo = $('#motivo').val();
                    var clientes_ruta = $('#clientes_ruta').val();
                    var direccioncliente_ruta = $('#direccioncliente_ruta').val();
                    var observacion_ruta = $('#observacion_ruta').val();
                    console.log(form + '&tipo=' + tipo);
                    if (clientes_ruta == 0) {
                        bootbox.alert("Seleccionar un cliente!", function() {
                            $("#clientes_ruta").select2('open');

                        });
                    } else if (direccioncliente_ruta == 0 || direccioncliente_ruta == null) {
                        bootbox.confirm({
                            message: " Ups Cliente no Tiene direccion, Desea Agregar una?",
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function(result) {
                                if (result == true) {
                                    document.location.href = 'modify_cliente.php?id=' + clientes_ruta;
                                }
                                // console.log('This was logged in the callback: ' + result);                                   
                            }
                        });
                    } else if (observacion_ruta == "") {
                        bootbox.alert("Es necesario ingresar una descripcion!", function() {

                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'JSON',
                            data: form + '&tipo=' + tipo,
                            url: '../data/model_create_domicilio.php',
                            success: function(res) {
                                if (res > 1) {

                                    bootbox.alert("Registro creado Exitosamente!", function() {
                                        document.location.href = 'create_domicilios.php'
                                    });

                                } else {
                                    bootbox.alert("!Algo salio mal comunicarse con un Administrado!",
                                        function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                }

                            }
                        });
                    }

                }
                //guarda domicilio compras en plza
                function send_cplaza() {
                    var tipo = $('#motivo').val();
                    var n_order = $('#n_order').val();
                    var clientes_compras = $("#clientes_compras").val();
                    var direccioncliente_compras = $("#direccioncliente_compras").val();
                    var ocompras = $('#ocompras').val();
                    var telefonocliente_compras = $("#telefonocliente_compras").val();

                    if (n_order == "") {
                        bootbox.alert("ingrese una ODC!", function() {
                            $('#n_order').focus();
                        });
                    } else if (clientes_compras == 0) {
                        bootbox.alert("ingrese una ODC!", function() {
                            $('#n_order').focus();
                        });
                    } else if (direccioncliente_cobro == 0 || direccioncliente_compras == null) {
                        bootbox.confirm({
                            message: " Ups Cliente no Tiene direccion, Desea Agregar una?",
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function(result) {
                                if (result == true) {
                                    document.location.href = 'modify_cliente.php?id=' +
                                        clientes_compras;
                                }
                                // console.log('This was logged in the callback: ' + result);                                   
                            }
                        });
                    } else if (ocompras == "") {
                        bootbox.alert("Es necesario ingresar una descripcion!", function() {
                            $('#ocompras').focus();
                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'html',
                            data: {
                                tipo: tipo,
                                n_order: n_order,
                                clientes_compras: clientes_compras,
                                direccioncliente_compras: direccioncliente_compras,
                                telefonocliente_compras: telefonocliente_compras,
                                ocompras: ocompras
                            },
                            url: '../data/model_create_domicilio.php',
                            success: function(res) {
                                if (res > 1) {

                                    bootbox.alert("Registro creado Exitosamente!", function() {
                                        document.location.href = 'create_domicilios.php'
                                    });

                                } else {
                                    bootbox.alert("!Algo salio mal comunicarse con un Administrado!",
                                        function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                }

                            }
                        });
                    }
                }
                //guarda domicilio taller
                function send_taller() {
                    var tipo = $('#motivo').val();
                    var n_order = $('#n_order_taller').val();
                    var clientes_taller = $("#clientes_taller").val();
                    var direccioncliente_taller = $("#direccioncliente_taller").val();
                    var otaller = $('#otaller').val();
                    var telefonocliente_taller = $("#telefonocliente_taller").val();

                    if (n_order == "") {
                        bootbox.alert("ingrese una ODC!", function() {
                            $('#n_order').focus();
                        });
                    } else if (clientes_taller == 0) {
                        bootbox.alert("ingrese una ODC!", function() {
                            $('#clientes_taller').focus();
                        });
                    } else if (direccioncliente_taller == 0 || direccioncliente_taller == null) {
                        bootbox.confirm({
                            message: " Ups Cliente no Tiene direccion, Desea Agregar una?",
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function(result) {
                                if (result == true) {
                                    document.location.href = 'modify_cliente.php?id=' + clientes_taller;
                                }
                                // console.log('This was logged in the callback: ' + result);                                   
                            }
                        });
                    } else if (otaller == "") {
                        bootbox.alert("Es necesario ingresar una descripcion!", function() {
                            $('#ocompras').focus();
                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'html',
                            data: {
                                tipo: tipo,
                                n_order: n_order,
                                clientes_taller: clientes_taller,
                                direccioncliente_taller: direccioncliente_taller,
                                telefonocliente_taller: telefonocliente_taller,
                                otaller: otaller
                            },
                            url: '../data/model_create_domicilio.php',
                            success: function(res) {
                                if (res > 1) {

                                    bootbox.alert("Registro creado Exitosamente!", function() {
                                        document.location.href = 'create_domicilios.php'
                                    });

                                } else {
                                    bootbox.alert("!Algo salio mal comunicarse con un Administrado!",
                                        function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                }

                            }
                        });
                    }
                }
                //guarda domicilio devolucion
                function send_devolucion() {
                    var tipo = $('#motivo').val();
                    var ndoc3 = $('#ndoc3').val();
                    var cli_id3 = $('#cli_id3').val();
                    var direccion3 = $('#direccioncliente3').val();
                    var telefono3 = $('#telefonocliente3').val();
                    var observacion3 = $('#observacion3').val();

                    if (ndoc3 == 0) {
                        bootbox.alert("Es necesario Seleccionar un Documento!", function() {
                            $("#ndoc3").select2('open');

                        });
                    } else if (cli_id3 == 0) {
                        bootbox.alert("Es necesario Seleccionar un Cliente!", function() {
                            $("#cli_id3").select2('open');
                        });

                    } else if (direccion3 == 0) {
                        bootbox.confirm({
                            message: " Ups Cliente no Tiene direccion, Desea Agregar una?",
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function(result) {
                                if (result == true) {
                                    document.location.href = 'modify_cliente.php?id=' + cli_id;
                                }
                                // console.log('This was logged in the callback: ' + result);                                   
                            }
                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'html',
                            data: {
                                tipo: tipo,
                                ndoc3: ndoc3,
                                cli_id3: cli_id3,
                                direccion3: direccion3,
                                telefono3: telefono3,
                                observacion3: observacion3
                            },
                            url: '../data/model_create_domicilio.php',
                            success: function(res) {
                                if (res > 1) {

                                    bootbox.alert("Registro creado Exitosamente!!", function() {
                                        document.location.href = 'create_domicilios.php'
                                    });

                                } else {
                                    bootbox.alert(
                                        "!Ups Algo salio mal comunicarse con un Administrado!",
                                        function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                }

                            }
                        });
                    }
                }

                //guarda domicilio pedido pendiente
                function send_ppendiente() {
                    var tipo = $('#motivo').val();
                    var oppendiente = $('#oppendiente').val();
                    var ndoc2 = $('#ndoc2').val();
                    var cli_id = $('#cli_id_p').val();
                    var direccion = $('#direccioncliente_ppendiente').val();
                    var telefono = $('#telefonocliente_ppendiente').val();


                    if (ndoc2 == 0) {
                        bootbox.alert("Es necesario selecciontar un documento!", function() {

                            $("#ndoc2").select2('open');
                        });
                    } else if (cli_id == 0) {
                        bootbox.alert("Seleccionar un cliente!", function() {
                            $("#cli_id_p").select2('open');

                        });
                    } else if (direccion == 0) {
                        bootbox.alert("Es necesario ingresar una direccion!", function() {
                            $('#direccioncliente_ppendiente').select2('open');
                        });
                    } else if (oppendiente == "") {
                        bootbox.alert("Es necesario ingresar una descripcion!", function() {
                            $('#oppendiente').focus();
                        });
                    } else {
                        if (document.getElementById("oppendiente").value.length < 15) {
                            bootbox.alert("Es necesario ingresar mas informacion!", function() {
                                $('#oppendiente').focus();
                            });
                        } else {
                            $.ajax({
                                type: 'POST',
                                dataType: 'html',
                                data: {
                                    tipo: tipo,
                                    oppendiente: oppendiente,
                                    ndoc2: ndoc2,
                                    cli_id: cli_id,
                                    direccion: direccion,
                                    telefono: telefono
                                },
                                url: '../data/model_create_domicilio.php',
                                success: function(res) {
                                    if (res > 1) {

                                        bootbox.alert("Registro creado Exitosamente!", function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                    } else {
                                        bootbox.alert(
                                            "!Algo salio mal comunicarse con un Administrado!",
                                            function() {
                                                document.location.href = 'create_domicilios.php'
                                            });

                                    }

                                }
                            });
                        }
                    }
                }
                //guarda domicilio operaciones banco
                function send_banco() {
                    var tipo = $('#motivo').val();
                    var obanco = $('#obanco').val();
                    obanco = obanco.replace(/\r?\n/g, '<br />');

                    if (obanco == "") {
                        bootbox.alert("Es necesario ingresar una descripcion!", function() {
                            $('#ocobro').focus();
                        });
                    } else {
                        if (document.getElementById("obanco").value.length < 15) {
                            bootbox.alert("Es necesario ingresar mas informacion!", function() {
                                $('#obanco').focus();
                            });
                        } else {
                            $.ajax({
                                type: 'POST',
                                dataType: 'html',
                                data: {
                                    tipo: tipo,
                                    obanco: obanco
                                },
                                url: '../data/model_create_domicilio.php',
                                success: function(res) {
                                    if (res > 1) {

                                        bootbox.alert("Registro creado Exitosamente!", function() {
                                            document.location.href = 'create_domicilios.php'
                                        });

                                    } else {
                                        bootbox.alert(
                                            "!Algo salio mal comunicarse con un Administrado!",
                                            function() {
                                                document.location.href = 'create_domicilios.php'
                                            });

                                    }

                                }
                            });
                        }
                    }
                }
            </script>
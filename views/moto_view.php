<?php
require_once('../data/conexion.php');

require_once('../layouts/header.php');
?>
<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include("../layouts/menu_moto.php")?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
    <div id="content">

        <?php include("../layouts/navbar.php")?>
       
        <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <!-- <h1 class="h3 mb-0 text-gray-800">Dashboard</h1> -->
                </div>
               
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Mis Domicilios Asignados</h6> 
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="10px">#</th>
                                            <th width="100px">Tipo</th>
                                            <th width="110px">Orden Entrega</th>
                                            <th width="160px">Cliente</th>
                                            <th width="190px">Observacion</th>                   
                                            <th width="100px">Hora Asignacion</th>
                                            <th width="90px">Hora Inicio</th>
                                            <th width="110px">Tiempo</th>
                                            <th >Acciones</th>
                                        </tr>
                                    </thead>                                   
                                    <tbody>
                                        
                                    <?php   

                                            $sql1="SELECT mt_motivo,dm_observacion,dm_fcreacion,des_hora_asignacion,dm_id,rt_numero_ruta,rt_orden,rt_id,rt_tiempo,
                                            (SELECT cli_nombre FROM prg.cli_clientes WHERE cli_id=dm_id_cli) cliente,rt_inicio,rt_estado
                                              FROM prg.rt_rutas
                                            JOIN prg.des_destinos on rt_id_des=des_id
                                            JOIN prg.dm_domicilios ON des_id_dm = dm_id
                                            JOIN prg.mt_motivos ON dm_id_mt=mt_id 
                                            WHERE des_id_mo = ".$_COOKIE['mo_id']." ORDER BY rt_orden ASC ";
                                                $ds=odbc_exec($conn,$sql1);


                                                    while($fila=odbc_fetch_array($ds))
                                                        {$i++;
                                                          
                                                        $h = ($fila['rt_inicio'] == '00:00:00')? new DateTime(date("Y-m-d H:i:s")): new DateTime(date('Y-m-d H:i:s',strtotime($fila['rt_inicio'])));
                                                        $hactual2 = date("Y-m-d H:i:s");
                                                           /*
                                                          $interval = $h->diff($hactual); */
                                                          //echo $interval->format('%H horas %i minutos %s seconds'); 

                                                          $time = str_replace(':','',$fila['rt_inicio']);
                                                            ?>
 
                                                            <script>
                                                                var fun = () => {
                                                                var hoy = new Date();
                                                                var hora1 = (hoy.getHours()+':'+hoy.getMinutes()+':'+hoy.getSeconds()).split(":");
                                                                  //var fe = hoy.getFullYear()+'-'+hoy.getMonth()+'-'+hoy.getDay();
                                                                var hora2 = '<?php echo $fila['rt_inicio']?>'.split(":");
                                                                var stado = '<?php echo $fila['rt_estado']?>';
                                                                var ffin = '<?php echo $fila['rt_tiempo']?>';

                                                                t1 = new Date(),
                                                                t2 = new Date();
                                                                t1.setHours(hora1[0], hora1[1], hora1[2]);
                                                                t2.setHours(hora2[0], hora2[1], hora2[2]);
                                                                
                                                                //Aquí hago la resta
                                                                t1.setHours(t1.getHours() - t2.getHours(), t1.getMinutes() - t2.getMinutes(), t1.getSeconds() - t2.getSeconds());
                                                                  
                                                                  if (stado == '0') {
                                                                  $('#hms_<?php echo $fila['dm_id']?>').val('00:00:00');                                                                     
                                                                  }else if(stado == '4'){
                                                                    $('#hms_<?php echo $fila['dm_id']?>').val(ffin);
                                                                  }else{
                                                                  $('#hms_<?php echo $fila['dm_id']?>').val((t1.getHours() ? " 0"+t1.getHours()+":" : "00:") + (t1.getMinutes() ? ("0" + (t1.getMinutes() + 1)).slice(-2): "00:") + (t1.getSeconds() ? (t1.getHours() || t1.getMinutes() ? ":" : "") + ("0" + (t1.getSeconds() + 1)).slice(-2)  : ":00")); 
                                                                  }
                                                                 
                                                                }
                                                               
                                                                setInterval(fun, 1000);
                                                            </script>
                                                            
                                                            <tr>
                                                                <td><?php echo $i;?></td>
                                                                <td><?php echo $fila['mt_motivo'] ?></td>
                                                                <td align="center"><?php echo $fila['rt_orden'] ?></td>
                                                                <td><?php if(isset($fila['cliente'])){ echo $fila['cliente'];}else{echo "Sin Cliente";}?></td>                                                                                                                           
                                                                <td><?php echo $fila['dm_observacion'] ?></td>                                                                
                                                                <td><?php echo date('H:i:s',strtotime($fila['des_hora_asignacion'])) ?></td> 
                                                                <td><?php echo $fila['rt_inicio'] ?></td>                                                                                                                            
                                                                <td>                                                                
                                                                <input type="text" class="form-control bg-primary text-white" id="hms_<?php echo $fila['dm_id'] ?>" name="hms_<?php echo $fila['dm_id'] ?>"  disabled >
                                                                </td>
                                                                <td>
                                                                <?php echo $es = ($fila['rt_estado'] == '3')? '<button type="button" class="btn btn-outline-danger" onclick="finalizar('.$fila['rt_id'].',\''.$fila['rt_inicio'].'\')">Finalizar</button>': (($fila['rt_estado'] == '4')? '<button type="button" class="btn btn-outline-primary" disabled>Finalizado</button>':'<button type="button" class="btn btn-outline-success" onclick="iniciar('.$fila['rt_id'].','.$fila['dm_id'].');" >Iniciar</button>') ?>
                                                                <button type="button" class="btn btn-outline-secondary" onclick="ver_dir(<?php echo $fila['dm_id']?>);">Ver</button>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                odbc_close($conn);
                                    ?>
                                    </tbody>
                            </table>
                            </div>
                        </div>
                    </div>             

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->

<!-- modal para ver los detalles del domicilio -->
<div class="modal fade bd-example-modal-lg" id="ver_dm" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Detalles del Domicilio</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="POST" autocomplet="OFF">
              <div class="container-fluid">
                      <div class="row">
                          <div class="col-md-6">
                            <label for="codigo">Codigo del Domicilio</label>
                            <input type="text" class="form-control" disabled id="codigo_dm_ver" name="codigo_dm_ver">                          
                          </div>
                          <div class="col-md-6">
                            <label for="codigo">Tipo de Domicilio</label>
                            <input type="text" class="form-control bg-success text-white" disabled id="tipo_dm_ver" name="tipo_dm_ver">                          
                          </div>
                      </div>
                      <div id="divVenta" style="display: none;">                      
                        <div class="row">
                          <div class="col-md-4 mt-2">
                          <label for="dm_doc">Numero de Documento</label>
                          <input type="text" class="form-control bg-primary text-white" id="dm_doc" disabled>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <label class="mt-3">Detalles</label>
                              <table id="table_productos" class="table table-bordered"  width="100%" cellspacing="0"></table>
                            </div>
                          </div>
                      </div>
                      <div id="divTraslado" style="display: none;">
                          <div class="row">
                            <div class="col-md-4 mt-2">
                            <label for="dm_doc">Numero de Traslado</label>
                            <input type="text" class="form-control bg-primary text-white" id="dm_doct" disabled>
                            </div> 
                            <div class="col-md-5 mt-2">
                            <label for="dm_doc">Sucural</label>
                            <input type="text" class="form-control bg-primary text-white" id="dm_suc" disabled>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                            <label class="mt-3">Detalles</label>
                              <table id="table_productos_traslados" class="table table-bordered"  width="100%" cellspacing="0"></table>
                            </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="col-md-6 mt-2">                    
                          <label for="motoristas_select">Nombre del vendedor</label>
                          <input type="text" class="form-control" disabled id="vendedor_dm_ver" name="vendedor_dm_ver">
                          </div>
                        
                      </div>              
                      <div class="row">
                        <div class="col-md-6">
                        <label for="observaciones_dm_ver">Observaciones</label>
                        <textarea id="observaciones_dm_ver" class="form-control bg-info text-white" disabled></textarea>                        
                        </div>
                        
                      </div>
                      <div class="row">
                        
                        <div class="col-md-6">
                        <label for="observaciones_dm_ver">Hora Asignacion</label>
                        <input id="hasignacion_dm_ver" class="form-control" disabled/>                        
                        </div>
                      </div>    
              </div>
      </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>              
            </div>
      </form>  
      
    </div>
  </div>
</div>

<?php
require_once('../layouts/foother.php');
?>
<script>   
var ver_dir = (id_domicilio) => {
    $('#ver_dm').modal('show');
    trae_dm_ver(id_domicilio);
    
}



//trae los datos del domicilio
function trae_dm_ver(id){
  var divVenta = document.getElementById("divVenta");
  var divTraslado = document.getElementById("divTraslado");

  $.ajax({
    dataType:'json',
		type:'POST',
		url:'../data/trae_datos_dm.php',
    data:{id:id},		
		success: function(html){            
		      //var mo[] = html;
        $('#codigo_dm_ver').val(html.codigo);         
        $('#tipo_dm_ver').val(html.motivo);         
        $('#vendedor_dm_ver').val(html.vendedor);         
        $('#fecha_dm_ver').val(html.fecha);         
        $('#hora_dm_ver').val(html.hora);         
        $('#observaciones_dm_ver').val(html.observacion);         
        $('#motorista_dm_ver').val(html.motorista);        
        $('#fasignacion_dm_ver').val(html.fasignacion);        
        $('#hasignacion_dm_ver').val(html.hasignacion);  
        //$('#dm_tipo').val(html.tipo);         
        $('#dm_doc').val(html.doc);
        $('#dm_doct').val(html.doc);     
        $('#dm_suc').val(html.sucursal);     

        if (html.tipo == 2) {
          divVenta.style.display = "block";
          divTraslado.style.display = "none";
        }else if(html.tipo == 11){
          divTraslado.style.display = "block";
          divVenta.style.display = "none";
        }else{
          divTraslado.style.display = "none";
          divVenta.style.display = "none";
        }   
        trae_productos(html.doc,html.tipo);
				}
		});
}

//trae los productos del documento de una venta
function trae_productos(id_doc,tipo){
    var tipo = tipo;
    var  ndoc = id_doc;
    var tabla = '#table_productos';
    (tipo == 11)? (tabla = '#table_productos_traslados',ruta = '../data/trae_detalle_traslado.php'):(tabla=tabla, ruta = '../data/trae_detalle_doc.php');
        $.ajax({
            data:{ndoc:ndoc},
            url: ruta,
            dataType:'html',
            type:'POST',
            beforeSend: function(){
                $(tabla).html('<center><img alt="Cargando..." src="../img/searching3.gif" /></center>');
            },
        success: function(html){
          setTimeout(function(){
            $(tabla).html(html);
            }, 2000)
            

            }
        });
}

//funcion para iniciar la 
var iniciar = (id_rt,id_dm) => {
 
    var id_rt = id_rt;
 // console.log(id_dm); 
         $.ajax({
                type:'POST',
                dataType:'html',
                data:{id_rt:id_rt,id_dm:id_dm},
                url: '../data/model_update_rt.php',
            success:function(res){
                    if(res>1){
                        bootbox.alert("Destino iniciado!", function() {
                            document.location.href='moto_view.php'
                            });

                    }else {
                    bootbox.alert("!Algo salio mal comunicarse con un Administrado!", function() {
                        document.location.href='moto_view.php'
                        });

                    }

            }
        }); 
}

var finalizar = (rt_id,rt_inicio) => {
  var iden = 1;
  $.ajax({
                type:'POST',
                dataType:'html',
                data:{id_rt:rt_id, iden:iden, rt_inicio:rt_inicio},
                url: '../data/model_update_rt.php',
            success:function(res){
                    if(res>1){
                        bootbox.alert("Destino Finalizado!", function() {
                            document.location.href='moto_view.php'
                            });

                    }else {
                    bootbox.alert("!Algo salio mal comunicarse con un Administrado!", function() {
                        document.location.href='moto_view.php'
                        });

                    }

            }
        }); 
}



</script>



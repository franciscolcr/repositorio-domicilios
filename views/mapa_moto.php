<?php
$id_dm = $_GET['id_dm'];

?>

<!DOCTYPE html>
<html lang="en">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body >
<div class="container" id="crudApp">
<div id="googleMap" style="width:1000px; height: 800px; border: solid; border-color: #00FFFF  "></div>

    <p v-for="row in allData">{{ latitude = row.tr_latitud}} {{longitude = row.tr_longitud }}  </p>
<p>{{latitude}}</p>    
</div>
</body>
</html>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJPfKa5UyTLlJ1EAI8bIYH0nBau8NECEQ&libraries=place"> </script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

 
<script>
    var application = new Vue({
        el:'#crudApp',
        data:{
            allData:'',
            myModel:false,
            actionButton:'Insert',
            dynamicTitle:'Add Data',
            timer:'',
            message:"",
            latitude: '13.718339',
            longitude:'-89.223871',
            myLatLng: '',
        },

        methods:{
            fetchAllData:function(){
                setInterval( () =>{
                    axios.post('../data/trae_ubicaciones.php?id_dm='+<?php echo $id_dm; ?>, {
                        action:'fetchall'
                    }).then(function(response){
                        application.allData = response.data;
                        console.log(response.data);
                    });
                },10000 )

            },
            
            calcRoute:function(){
                var mapOption = {
                center: new google.maps.LatLng(13.718339,-89.223871),
                zoom: 17,
                mapTypeId:google.maps.MapTypeId.ROADMAP                
                };

                var gMapa = new google.maps.Map(document.getElementById("googleMap"),mapOption);
                
               var new_ubicacion = this.myLatLng;                

            },  
            actualiza:function(){
                var myLatLng = new google.maps.LatLng(this.latitude, this.longitude);;
                setInterval(() => {
                    var myLatLng = new google.maps.LatLng(this.latitude, this.longitude);;
                }, 1000);
                

                var mapOption={
                    zoom:17,
                    center: myLatLng
    
                };
    
                var gMapa = new google.maps.Map(document.getElementById("googleMap"),mapOption);

                
               

                setInterval(() => {
                    var objConfigMarker={
                    position: myLatLng,
                    animation:google.maps.Animation.DROP,
                    map: gMapa,
                    title:'Usted Esta Aqui'
    
                }
                    var gMarker = new google.maps.Marker(objConfigMarker);
                }, 5000);
                
                console.log(myLatLng);
            },

          
        },
        created:function(){
            this.fetchAllData();
        },
        mounted () {
            this.fetchAllData(),
            this.calcRoute(),
            this.actualiza()
        },
    });

</script>
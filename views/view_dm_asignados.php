<?php
require_once('../data/conexion.php');

require_once('../layouts/header.php');
?>
<style>
.tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 120px;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;
  position: absolute;
  z-index: 1;
  bottom: 125%;
  left: 50%;
  margin-left: -60px;
  opacity: 0;
  transition: opacity 0.3s;
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
  opacity: 1;
}

.editable {
  width: 300px;
  height: 200px;
  border: 1px solid #ccc;
  padding: 5px;
  resize: both;
  overflow: auto;
}

</style>
<body id="page-top" >

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include("../layouts/menu_dispatch.php")?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
    <div id="content">

        <?php include("../layouts/navbar.php")?>
       
        <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                </div>
              
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                        <h4 class="m-0 font-weight-bold text-primary">Domicilios Asignados</h4> 
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center" >Código</th>
                                            <th class="text-center" width="10px">Tipo</th>
                                            <th class="text-center" width="190px">Observacion</th>
                                            <th class="text-center">Cliente</th>
                                            <th class="text-center">Nº Doc</th>
                                            <th class="text-center" width="75px">Fecha</th>
                                            <th class="text-center" width="110px">Hora Creaciòn</th>
                                            <th class="text-center">Estado</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>                                        
                                    </thead>                                   
                                    <tbody>    
                                    <?php 
                                        $sql1="SELECT mt_motivo,dm_observacion,dm_fcreacion,est_nombre,est_icon,dm_id,dm_codigo,
                                        (select cli_nombre from prg.cli_clientes where cli_id=dm_id_cli) cliente,
                                        (CASE dm_id_mt WHEN '2' THEN (select codigo from prg.docclientesm where pludoccliente = dm_id_doc)
                                        WHEN '11' THEN (dm_id_doc) ELSE '' END)  codigo,dm_id 
                                        FROM prg.dm_domicilios 
                                        LEFT OUTER JOIN prg.docclientesm dc on dm_id_doc=pludoccliente
                                        JOIN prg.mt_motivos ON dm_id_mt=mt_id 
                                        JOIN prg.estados_domicilios ON dm_estado=est_id WHERE dm_estado != 1 ORDER BY dm_fcreacion DESC ";
                                            $ds=odbc_exec($conn,$sql1);
                                                while($fila=odbc_fetch_array($ds))
                                                    {$i++;
                                                      $cod = (int)$fila['codigo'];
                                                      $codigo = $cod == 0 ? 'Sin Doc': $cod; 
                                                        ?>
                                                        <tr>
                                                            <td align="center"><?php echo $i;?></td>
                                                            <td><?php echo $fila['dm_codigo'];?></td>
                                                            <td><?php echo $fila['mt_motivo'] ?></td>                                                            
                                                            <td><?php echo $fila['dm_observacion'] ?></td>
                                                            <td><?php echo (isset($fila['cliente']))? $fila['cliente'] : "Sin Cliente";?></td>
                                                            <td><?php echo $codigo; ?></td>
                                                            <td><?php echo date('d-m-Y',strtotime($fila['dm_fcreacion'])) ?></td>
                                                            <td align="center"><?php echo date('H:i:s',strtotime($fila['dm_fcreacion'])) ?></td>                                                                
                                                            <td align="center" data-toggle="tooltip" title="<?php echo $fila['est_nombre'] ?>" >
                                                            <img src="../img/icons/<?php echo $fila['est_icon']?>" class="img-fluid"   width="25" height="10">                                                                
                                                            </td> 
                                                            <td>

                                                            <div class="dropdown mb-4">
                                                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Opciones
                                                                    </button>
                                                                    <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton" >
                                                                    <a class="dropdown-item" onclick="ver_dm(<?php echo $fila['dm_id']?>);">Ver</a>
                                                                        <li><hr class="dropdown-divider"></li>
                                                                        <a class="dropdown-item" onclick="trae_motoristas(<?php echo $fila['dm_id']?>);">Re-Asignar</a>
                                                                        <li><hr class="dropdown-divider"></li>
                                                                        <a class="dropdown-item"  onclick="ver_ruta(<?php echo $fila['dm_id']?>);">Ubicar</a>
                                                                    </div>
                                                                </div>   
                                                                <!--<button type="button" class="btn btn-outline-primary"  onclick="ver_dm(<?php echo $fila['dm_id']?>);">Ver</button>
                                                                <button type="button" class="btn btn-outline-info" onclick="trae_motoristas(<?php echo $fila['dm_id']?>);">Re-Asignar</button>
                                                                <button type="button" class="btn btn-outline-success" onclick="trae_motoristas(<?php echo $fila['dm_id']?>);">Ubicar</button>-->
                                                            </td>                                                                                                                            
                                                        </tr>
                                                    <?php
                                                    }
                                            odbc_close($conn);
                                    ?>
                                    </tbody>
                            </table>
                            </div>
                            
                        </div>
                    </div>             

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->

<!-- modal asignacion de domicilio a motorista -->
<div class="modal fade" tabindex="-1" role="dialog" id="ModalMotoristas">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Asignacion de domicilio</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
    <div class="modal-body">
      <form method="POST" id="asignacion_dm" name="asignacion_dm" autocomplet="OFF">
              <div class="container-fluid">
                      <div class="row">
                          <div class="col-md-12">
                          <label for="codigo">Codigo del Domicilio</label>
                          <input type="text" class="form-control" disabled id="codigo_dm" name="codigo_dm">
                          <input type="hidden" id="dm_id" name="dm_id">
                          <input type="hidden" id="dm_estado" name="dm_estado">
                          
                          </div>
                      </div>
                      <div class="row mt-2">
                          <div class="col-md-12">                    
                          <label for="motoristas_select">Nuevo Motorista</label>
                              <select name="motoristas_select" id="motoristas_select" class="form-control">
                              <option value="0" selected>Seleccione una direccion </option>
                              </select>
                          </div>
                      </div>
                      <div class="row mt-2">
                          <div class="col-md-4 ">
                          <label for="fecha">Fecha</label>
                          <input type="text" class="form-control" disabled value="<?php echo date('d-m-Y'); ?>" id="fecha_asignacion" name="fecha_asignacion">
                          </div>
                          <div class="col-md-4">
                          <label for="fecha">Hora</label>
                          <input type="text" class="form-control" disabled  id="hora_asignacion" name="hora_asignacion">
                          </div>
                      </div>    
              </div>
          </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">cancelar</button>
              <button type="button" class="btn btn-success"  onclick="asignar_dm();">Re-Asignar</button>
            </div>
      </form>
    </div>
  </div>
</div>

<!-- modal para ver los detalles del domicilio -->
<div class="modal fade bd-example-modal-lg" id="ver_dm" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Detalles del Domicilio</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="POST" autocomplet="OFF">
              <div class="container-fluid">
                      <div class="row">
                          <div class="col-md-6">
                            <label for="codigo">Codigo del Domicilio</label>
                            <input type="text" class="form-control" disabled id="codigo_dm_ver" name="codigo_dm_ver">                          
                          </div>
                          <div class="col-md-6">
                            <label for="codigo">Tipo de Domicilio</label>
                            <input type="text" class="form-control bg-success text-white" disabled id="tipo_dm_ver" name="tipo_dm_ver">                          
                          </div>
                      </div>
                      <div id="divVenta" style="display: none;">                      
                        <div class="row">
                          <div class="col-md-4 mt-2">
                          <label for="dm_doc">Numero de Documento</label>
                          <input type="text" class="form-control bg-primary text-white" id="dm_doc" disabled>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <label class="mt-3">Detalles</label>
                              <table id="table_productos" class="table table-bordered"  width="100%" cellspacing="0"></table>
                            </div>
                          </div>
                      </div>
                      <div id="divTraslado" style="display: none;">
                          <div class="row">
                            <div class="col-md-4 mt-2">
                            <label for="dm_doc">Numero de Traslado</label>
                            <input type="text" class="form-control bg-primary text-white" id="dm_doct" disabled>
                            </div> 
                            <div class="col-md-5 mt-2">
                            <label for="dm_doc">Sucural</label>
                            <input type="text" class="form-control bg-primary text-white" id="dm_suc" disabled>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                            <label class="mt-3">Detalles</label>
                              <table id="table_productos_traslados" class="table table-bordered"  width="100%" cellspacing="0"></table>
                            </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="col-md-6 mt-2">                    
                          <label for="motoristas_select">Nombre del vendedor</label>
                          <input type="text" class="form-control" disabled id="vendedor_dm_ver" name="vendedor_dm_ver">
                          </div>
                          <div class="col-md-3 mt-2">                    
                          <label for="motoristas_select">Fecha Creacion</label>
                          <input type="text" class="form-control" disabled id="fecha_dm_ver" name="fecha_dm_ver">
                          </div>
                          <div class="col-md-3 mt-2">                    
                          <label for="motoristas_select">Hora Creacion</label>
                          <input type="text" class="form-control" disabled id="hora_dm_ver" name="hora_dm_ver">
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                        <label for="observaciones_dm_ver">Observaciones</label>
                        <textarea id="observaciones_dm_ver" class="form-control bg-info text-white" disabled></textarea>                        
                        </div>
                        <div class="col-md-6">
                        <label for="observaciones_dm_ver">Motortista</label>
                        <input id="motorista_dm_ver" class="form-control" disabled/>                        
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                        <label for="observaciones_dm_ver">Fecha Asignacion</label>
                        <input id="fasignacion_dm_ver" class="form-control" disabled/>                        
                        </div>
                        <div class="col-md-6">
                        <label for="observaciones_dm_ver">Hora Asignacion</label>
                        <input id="hasignacion_dm_ver" class="form-control" disabled/>                        
                        </div>
                      </div>    
              </div>
      </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>              
            </div>
      </form>  
      
    </div>
  </div>
</div>


<div class="modal fade bd-example-modal-lg" id="mapa_ruta" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Detalles del Domicilio</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">     
              <div class="container-fluid">                
                          <div class="row">
                            <div class="col-md-12">
                            <label class="mt-3">Detalles</label>
                              <div id="div_mapa">
                              
                              </div>
                            </div>
                          </div>                         
              </div>
      </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>              
            </div>
     
      
    </div>
  </div>
</div>

<?php
require_once('../layouts/foother.php');
?>
<script>
//para que funcione el tooltip en el table
$(function(){
    $('body').tooltip({selector: '[data-toggle="tooltip"]'});
});

//carga el select de los motoristas
function trae_motoristas(id_domicilio){    
    $.ajax({
		type:'POST',
		url:'../data/trae_motoristas.php',		
		success: function(opciones){            
		      //var mo[] = html;
           $('#motoristas_select').html(opciones);
           //console.log(opciones);
				}
		});
    $('#ModalMotoristas').modal('show');    
    trae_dm(id_domicilio);
//console.log(html);
}

function ver_dm(id_domicilio){
  $('#ver_dm').modal('show');
  trae_dm_ver(id_domicilio);
}

function ver_ruta(id_domicilio){
  window.open('rt_ruta_moto.php?id_dm='+id_domicilio, '_blanck');
  //trae_div_mapa(id_domicilio);
}

//para ver la hora de asignacion
function get_hour(){
  var d = new Date();  
  const CERO = n => n = n < 10 ? "0"+n: n;
  $('#hora_asignacion').val(CERO(d.getHours())+":"+CERO(d.getMinutes())+":"+CERO(d.getSeconds()));
}


//actuliza cada 3 segundos la funcion de obtener la hora
setInterval(get_hour, 3000);

//trae los datos del domicilio
function trae_dm(id){
  $.ajax({
    dataType:'json',
		type:'POST',
		url:'../data/trae_datos_dm.php',
    data:{id:id},		
		success: function(html){            
		      //var mo[] = html;
           $('#codigo_dm').val(html.codigo);
           $('#dm_id').val(html.id);
           $('#dm_estado').val(html.estado);

		}
		});
}

//trae los datos del domicilio
function trae_dm_ver(id){
  var divVenta = document.getElementById("divVenta");
  var divTraslado = document.getElementById("divTraslado");

  $.ajax({
    dataType:'json',
		type:'POST',
		url:'../data/trae_datos_dm.php',
    data:{id:id},		
		success: function(html){            
		      //var mo[] = html;
        $('#codigo_dm_ver').val(html.codigo);         
        $('#tipo_dm_ver').val(html.motivo);         
        $('#vendedor_dm_ver').val(html.vendedor);         
        $('#fecha_dm_ver').val(html.fecha);         
        $('#hora_dm_ver').val(html.hora);         
        $('#observaciones_dm_ver').val(html.observacion);         
        $('#motorista_dm_ver').val(html.motorista);        
        $('#fasignacion_dm_ver').val(html.fasignacion);        
        $('#hasignacion_dm_ver').val(html.hasignacion);  
        //$('#dm_tipo').val(html.tipo);         
        $('#dm_doc').val(html.doc);
        $('#dm_doct').val(html.doc);     
        $('#dm_suc').val(html.sucursal);     

        if (html.tipo == 2) {
          divVenta.style.display = "block";
          divTraslado.style.display = "none";
        }else if(html.tipo == 11){
          divTraslado.style.display = "block";
          divVenta.style.display = "none";
        }else{
          divTraslado.style.display = "none";
          divVenta.style.display = "none";
        }   
        trae_productos(html.doc,html.tipo);
				}
		});
}

//funcion que envia los datos para asignar el domicilio
function asignar_dm(){
  var codigo_dm = $('#codigo_dm').val();    
  var dm_id = $('#dm_id').val();
  var motorista = $('#motoristas_select').val();
  var fecha = $('#fecha_asignacion').val();
  var hora = $('#hora_asignacion').val();
  var estado = $('#dm_estado').val();  
  var re = 1;
    $.ajax({
            type:'POST',
            dataType:'html',
            data:{codigo_dm:codigo_dm,dm_id:dm_id,motorista:motorista,fecha:fecha,hora:hora,estado:estado,re:re},
                    url: '../data/model_asignar_dm.php',
        success:function(res){
        if(res>1){
          $('#ModalMotoristas').modal('hide');
            bootbox.alert("Registro creado Exitosamente!", function() {
                document.location.href='dispatch_view.php'
                });

        }else {
          $('#ModalMotoristas').modal('hide');
        bootbox.alert("!Algo salio mal comunicarse con un Administrado!", function() {
            document.location.href='dispatch_view.php'
            });

        }

        }
    });

}

//trae los productos del documento de una venta
function trae_productos(id_doc,tipo){
    var tipo = tipo;
    var  ndoc = id_doc;
    var tabla = '#table_productos';
    (tipo == 11)? (tabla = '#table_productos_traslados',ruta = '../data/trae_detalle_traslado.php'):(tabla=tabla, ruta = '../data/trae_detalle_doc.php');
        $.ajax({
            data:{ndoc:ndoc},
            url: ruta,
            dataType:'html',
            type:'POST',
            beforeSend: function(){
                $(tabla).html('<center><img alt="Cargando..." src="../img/searching3.gif" /></center>');
            },
        success: function(html){
          setTimeout(function(){
            $(tabla).html(html);
            }, 2000)
          }
        });
}


</script>

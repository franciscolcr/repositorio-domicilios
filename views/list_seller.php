<?php
require_once('../data/conexion.php');

require_once('../layouts/header.php');
?>
<body id="page-top"     >

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include("../layouts/menu_admin.php")?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
    <div id="content">

        <?php include("../layouts/navbar.php")?>
       
        <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                </div>

                <div class="d-sm-flex align-items-center  mb-4">
            
                          
                </div>
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Usuarios</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr style="text-align: center">
                                           
                                            <th>Nombre Usuario</th>
                                            <th>Nombre Vendedor</th>
                                            <th>Tipo</th>                                           
                                           
                                            <th>Acciones</th>                                        
                                        </tr>
                                    </thead>                                   
                                    <tbody>    
                                    <?php                        
                                            $sql1="SELECT usu_nombre,tp_nombre,Nombre,usu_id  FROM prg.usu_usuarios
                                            JOIN prg.tp_tipos_usuarios on usu_id_tp=tp_id
                                            JOIN prg.gestores on PLUgestor=id_codigo_gestor WHERE usu_estado=0 ORDER BY usu_id ASC";
                                                $ds=odbc_exec($conn,$sql1);
                                                    while($fila=odbc_fetch_array($ds))
                                                        {
                                                            ?>
                                                            <tr>                                                               
                                                                <td><?php echo $fila['usu_nombre'] ?></td>
                                                                <td><?php echo $fila['Nombre']?></td>
                                                                <td><?php echo $fila['tp_nombre'] ?></td>  
                                                              
                                                                <td>
                                                                   <a href="../views/modify_user.php?id=<?php echo $fila['usu_id']?>" class="btn btn-success" role="button" data-bs-toggle="button">Editar</a>
                                                                  
                                                                    <a href="#" class="btn btn-danger" role="button" onclick="eliminausuario(<?php echo $fila['usu_id']?>);";>Eliminar</a>
                                                                </td>   

                                                            </tr>
                                                            <?php
                                                        }
                                                odbc_close($conn);
                                    ?>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                    </div>    
                       

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

       
<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Se Cerra La Session?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">Asegurece de Haber guardado todo!</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-primary" href="../data/cerrar_session.php">Salir</a>
            </div>
        </div>
    </div>
</div>


<?php
require_once('../layouts/foother.php');
?>

<script>

function eliminausuario(mo_id){
    var mo_id = mo_id;
    bootbox.confirm({
            message: "Se Eliminara el Usuario, Decea Proceder?",
            buttons: {
                confirm: {
                    label: 'Si',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result == true){
                    document.location.href='../data/delete_user.php?id=' + mo_id;
                }   
            }
        });
    
}
</script>

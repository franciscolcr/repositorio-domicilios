<?php
require_once('../data/conexion.php');
require_once('../layouts/header.php');

$id_cliente = $_GET['id'];

$sql1 = "select * from prg.cli_clientes  where cli_id=$id_cliente";
$ds = odbc_exec($conn, $sql1);
while ($fila = odbc_fetch_array($ds)) {
    $nombre = $fila['cli_nombre'];

    $suc = $fila['cli_id_suc'];
    $celu = $fila['cli_celular'];
    $dui = $fila['cli_dui'];
    $nit = $fila['cli_nit'];
    $nrc = $fila['cli_nrc'];
    $id = $fila['cli_id'];
    $cli_tipo = $fila['cli_tipo'];
    $nom_emp = $fila['cli_nom_emp'];
    $per_cont_emp = $fila['cli_per_cont'];
    $cli_num_cont = $fila['cli_num_cont'];
    $cli_email = $fila['cli_email'];
}
//echo $sql1;
?>

<body>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include("../layouts/menu_sales.php") ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include("../layouts/navbar.php") ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <div class="row d-flex justify-content-center">


                        <!-- Area Chart -->


                        <!-- Pie Chart -->
                        <div class="col-xl-8 col-lg-5">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Informacion Del Cliente</h6>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <form action="../data/ed_cliente.php" method="POST" autocomplete="OFF">
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <label for="cli_id_suc">Tipo de usuarios <span style="color:red">*</span></label>
                                                <select name="select_tipo_cliente" id="select_tipo_cliente" class="form-control form-control-sm" onchange="ocultdiv();" disabled>
                                                    <option value="0" disabled>Seleccion el tipo de domicilio</option>
                                                    <option value="1" <?php if ($cli_tipo == 1) {
                                                                            echo "selected";
                                                                        } ?>>Cliente Normal</option>
                                                    <option value="2" <?php if ($cli_tipo == 2) {
                                                                            echo "selected";
                                                                        } ?>>Cliente Empresarial</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">

                                                <label for="cli_id_suc">Sucursal <span style="color:red">*</span></label>
                                                <select id="cli_id_suc" name="cli_id_suc" class="form-control" disabled>
                                                    <option value="" disabled>Seleccione Sucursal</option>
                                                    <?php $sql1 = "select * from prg.divisiones ";
                                                    $ds = odbc_exec($conn, $sql1);
                                                    while ($fila = odbc_fetch_array($ds)) { ?>
                                                        <option <?php if ($suc == $fila['PLUDivision']) {
                                                                    echo "selected";
                                                                } ?> value="<?php echo $fila['PLUDivision'] ?>"><?php echo $fila['Nombre'] ?></option>
                                                    <?php } ?>

                                                </select>

                                            </div>
                                            <input type="hidden" id="cli_id_i" name="cli_id_i" value="<?php echo $id ?>">
                                            <!-- este input sirve para el cliente normal-->
                                            <div id="cliente_normal" style="display: none">
                                                <div class="row">

                                                    <div class="form-group col-md-6 ">
                                                        <label for="tipo_vehiculo">Nombre Cliente <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="cli_nombre" name="cli_nombre" value="<?php echo $nombre ?>" required disabled>

                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="Tel-Oficina">Telefono <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="cli_celular" name="cli_celular" value="<?php echo $celu ?>" required disabled>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="modelo">Dui</label>
                                                        <input type="text" class="form-control" id="cli_dui" name="cli_dui" value="<?php echo $dui ?>" disabled>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">NIT</label>
                                                        <input type="text" class="form-control" id="cli_nit" name="cli_nit" value="<?php echo $nit ?>" disabled>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">NRC</label>
                                                        <input type="text" class="form-control" id="cli_nrc" name="cli_nrc" value="<?php echo $nrc ?>" disabled>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">Correo <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="cli_email" name="cli_email" value="<?php echo $email ?>" disabled>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--cliente empresarial input-->
                                            <div id="cliente_empresarial" style="display: none">
                                                <div class="row">

                                                    <div class="form-group col-md-6 ">
                                                        <label for="tipo_vehiculo">Nombre Cliente <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="cli_nombre_emp" name="cli_nombre_emp" value="<?php echo $nombre ?>" required disabled>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="nom_emp">Nombre De Empresa <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="nom_emp" name="nom_emp" value="<?php echo $nom_emp ?>" disabled>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="Tel-Oficina">Telefono De Empresa <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="telefono_emp" name="telefono_emp" value="<?php echo $celu ?>" disabled>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="per_cont">Persona De Contacto</label>
                                                        <input type="text" class="form-control" id="per_cont" name="per_cont" value="<?php echo $per_cont_emp ?>" disabled>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="num_cont">Numero De Contacto</label>
                                                        <input type="text" class="form-control" id="num_cont" name="num_cont" value="<?php echo $cli_num_cont ?>" disabled>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="modelo">Dui</label>
                                                        <input type="text" class="form-control" id="dui_emp" name="dui_emp" value="<?php echo $dui ?>" disabled>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">NIT <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="nit_emp" name="nit_emp" value="<?php echo $nit ?>" disabled>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">NRC <span style="color:red">*</span></label>
                                                        <input type="text" class="form-control" id="nrc_emp" name="nrc_emp" value="<?php echo $nrc ?>" disabled>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="capaciad">Correo <span style="color:red">*</span></label>
                                                        <input class="form-control" id="correo_e" name="correo_e" type="email" size="30" value="<?php echo $cli_email ?>" disabled>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                        <!--<button type="submit" class="btn btn-primary">Actualizar</button>-->
                                        <!--<a href="#" class="btn btn-success" role="button" data-bs-toggle="button">Nueva Direccion</a>-->
                                        <!--<a href="../views/new_direc.php?id=<?php echo $id_cliente ?>" class="btn btn-success" role="button" data-bs-toggle="button">Nueva Direccion</a>-->
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-8 col-lg-5">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Direcciones Cliente</h6>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr style="text-align: center">
                                                    <th>Direccion</th>
                                                    <th>F_Creacion</th>

                                                    <!--<th>Accciones</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sql1 = "select*from prg.dir_direcciones dr join prg.cli_clientes cl  on dr.dir_id_cli=cl.cli_id where dir_id_cli=$id_cliente";
                                                $ds = odbc_exec($conn, $sql1);
                                                while ($fila = odbc_fetch_array($ds)) {
                                                ?>
                                                    <tr>
                                                        <td><?php echo $fila['dir_direccion'] ?> </td>
                                                        <td><?php echo date('d-m-Y', strtotime($fila['dir_fcreacion'])) ?></td>
                                                        <!--<td>
                                                                        <div class="dropdown mb-4">
                                                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                Opciones
                                                                            </button>
                                                                            <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton" style="">
                                                                                <a class="dropdown-item" href="../views/modify_direccion.php?id=<?php echo $fila['dir_id'] ?>">Editar Direccion</a>
                                                                                <li><hr class="dropdown-divider"></li>
                                                                                <a class="dropdown-item" href="#" onclick="eliminaruta(<?php echo $fila['dir_id'] ?>);">Eliminar Direccion</a>
                                                                            </div>
                                                                        </div>
                                                                    <a href="../views/modify_direccion.php?id=<?php echo $fila['dir_id'] ?>"class="btn btn-success" role="button" data-bs-toggle="button">Editar</a>
                                                                    <a href="#" class="btn btn-danger" role="button" onclick="eliminaruta(<?php echo $fila['dir_id'] ?>);";>Eliminar</a>
                                                                </td>-->
                                                    </tr>

                                                <?php
                                                }
                                                odbc_close($conn);
                                                ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- /.container-fluid -->
                        </div>
                    </div>

                </div>
            </div>



            <?php
            require_once('../layouts/foother.php');
            ?>
            <script>
                function eliminaruta(dir_id) {
                    var dir_id = dir_id;
                    bootbox.confirm({
                        message: "Se Eliminara La Direccion, Decea Proceder?",
                        buttons: {
                            confirm: {
                                label: 'Si',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function(result) {
                            if (result == true) {
                                document.location.href = '../data/delete_direc.php?id=' + dir_id;
                            }

                            // console.log('This was logged in the callback: ' + result);
                        }
                    });

                }
                // esta funcion realiza el ocultamiento de los input de el select  de cliente.
                function ocultdiv() {
                    var select_tipo_cliente = document.getElementById('select_tipo_cliente').value;

                    if (select_tipo_cliente == 1) {
                        document.getElementById("cliente_normal").style.display = "block";
                        document.getElementById("cliente_empresarial").style.display = "none";
                    } else if (select_tipo_cliente == 2) {
                        document.getElementById("cliente_empresarial").style.display = "block";
                        document.getElementById("cliente_normal").style.display = "none";
                    } else {
                        document.getElementById("cliente_empresarial").style.display = "none";
                        document.getElementById("cliente_normal").style.display = "none";
                    }
                }
                document.addEventListener("DOMContentLoaded", ocultdiv);
            </script>
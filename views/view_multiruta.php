<?php
require_once('../data/conexion.php');

require_once('../layouts/header.php');
?>
<style>
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -60px;
    opacity: 0;
    transition: opacity 0.3s;
}

.tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
}

.editable {
    width: 300px;
    height: 200px;
    border: 1px solid #ccc;
    padding: 5px;
    resize: both;
    overflow: auto;
}
</style>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include("../layouts/menu_dispatch.php") ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include("../layouts/navbar.php") ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                    </div>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Creacion de Ruta</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr style="text-align: center">
                                            <th>Nombre Motorista</th>
                                            <th>Domicilios Asignados</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                    $sql1 = "SELECT count(des_id_mo) n,mo_nombre,mo_id FROM prg.mo_motoristas
                                        LEFT OUTER JOIN prg.des_destinos ON mo_id=des_id_mo WHERE mo_id_suc = " . $_COOKIE['sucursal'] . " AND des_en_ruta = 0
                                        GROUP BY mo_nombre,mo_id";
                    $ds = odbc_exec($conn, $sql1);
                    while ($fila = odbc_fetch_array($ds)) {
                    ?>
                                        <tr>
                                            <td><?php echo $fila['mo_nombre'] ?></td>
                                            <td align="center"><?php echo $fila['n'] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-outline-primary"
                                                    onclick="dd(<?php echo $fila['mo_id'] ?>);">Ordenar</button>
                                            </td>

                                        </tr>
                                        <?php
                    }
                    odbc_close($conn);
                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
            <!-- modal para ordenar la ruta -->
            <div class="modal fade bd-example-modal-lg" id="ver_dm" tabindex="-1" role="dialog"
                aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Orden de Ruta</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="ordenarR" method="POST" autocomplet="OFF">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="mt-3">Detalles</label>
                                            <table id="table_dms" class="table table-bordered" width="100%"
                                                cellspacing="0"></table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" id="mo_id" name="mo_id">
                                    <button type="button" class="btn btn-success" onclick="create_ruta()">Crear</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <?php
      require_once('../layouts/foother.php');
      ?>
            <script>
            //para que funcione el tooltip en el table
            $(function() {
                $('body').tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            });

            var dd = (mo_id) => {
                $('#ver_dm').modal('show');
                document.getElementById("mo_id").value = mo_id;
                trae_dms(mo_id);
            }

            var trae_dms = (mo_id) => {
                $.ajax({
                    data: {
                        mo_id: mo_id
                    },
                    url: '../data/trae_detalle_dms.php',
                    dataType: 'html',
                    type: 'POST',
                    beforeSend: function() {
                        $('#table_dms').html(
                            '<center><img alt="Cargando..." src="../img/searching3.gif" /></center>'
                            );
                    },

                    success: function(html) {
                        setTimeout(function() {
                            $('#table_dms').html(html);
                        }, 1000)


                    }
                });
            }

            function create_ruta() {
                var form = $("#ordenarR").serialize();

                $.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    data: form,
                    url: '../data/model_asignar_ruta.php',
                    success: function(res) {
                        if (res == true) {
                            $('#ver_dm').modal('hide');
                            bootbox.alert("Ruta creada Exitosamente!", function() {
                                document.location.href = 'view_multiruta.php'
                            });
                        } else {


                            bootbox.alert("!Algo salio mal comunicarse con un Administrado!", function() {
                                document.location.href = 'view_multiruta.php'
                            });

                        }
                    }
                });
                //console.log(form);
            }
            /* var create_ruta = () => {
            var id_des = document.getElementsByName('des_id[]'); 
            var orden =  document.getElementsByName('orden[]'); 
            var motorista_id =  $('#mo_id').val();
            //console.log(orden);
            //var jsonString = JSON.stringify(id_des);
            var indexesDefault = [];
            var indexesDefault2 = [];

            for (var i = 0; i < id_des.length; i++) { 
                var a = id_des[i];
                indexesDefault.push( a.value );
                   
              
            } //console.log('id: '+indexesDefault);

            for (var i = 0; i < orden.length; i++) { 
                var a = orden[i];
                indexesDefault2.push( a.value );

                
               
            } //console.log('orden: '+indexesDefault2);


            const numeros  = indexesDefault2;
            const numerosUnicos = [...new Set(numeros)]; // Array sin duplicados
            let duplicados = [...numeros]; // Creamos una copia del array original
            numerosUnicos.forEach((numero) => {
              const indice = duplicados.indexOf(numero);
              duplicados = duplicados.slice(0, indice)
              .concat(duplicados.slice(indice + 1, duplicados.length));
              
            });

            //con esta funcion elimino los 0 para que no de la alerta de duplicados
             var removeItemFromArr = ( arr, item ) => {
                var i = arr.indexOf( item );
                i !== -1 && arr.splice( i, 1 );
            };

            removeItemFromArr( duplicados, '0' ); 

            if (Object.keys(duplicados).length === 0) {
              
               $('#ver_dm').modal('hide');
                  $.ajax({
                          type:'POST',
                          dataType:'html',
                          data:{id_des:indexesDefault,orden:indexesDefault2,motorista_id:motorista_id},
                                  url: '../data/model_asignar_ruta.php',
                      success:function(res){
                        if(res>1){         
                        bootbox.alert("Ruta creada Exitosamente!", function() {
                            document.location.href='view_multiruta.php'
                            });
                    }else {

                     
                    bootbox.alert("!Algo salio mal comunicarse con un Administrado!", function() {
                        document.location.href='view_multiruta.php'
                        });

                    }
                      }
                      }); 
              
              
            }else{
              //$('#ver_dm').modal('hide');
              bootbox.alert("Ordena la ruta Correctamente, no pueden ir valores duplicados "+ duplicados, function() {              
                            });
            }
            console.log(duplicados); 
            } */
            </script>
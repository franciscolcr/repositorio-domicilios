<?php
require_once('../data/conexion.php');
require_once('../layouts/header.php');

$id_usu=$_GET['id'];

$sql1="SELECT usu_nombre,tp_nombre,Nombre,usu_id,tp_id,id_codigo_gestor  FROM prg.usu_usuarios
JOIN prg.tp_tipos_usuarios on usu_id_tp=tp_id
JOIN prg.gestores on PLUgestor=id_codigo_gestor  where usu_id=$id_usu";

$ds=odbc_exec($conn,$sql1);
while($fila=odbc_fetch_array($ds))
{
    $id=$fila['usu_id'];
    $tp_id=$fila['tp_id'];
    $usu_nombre=$fila['usu_nombre'];
    $nombre=$fila['Nombre'];
    $tp_nombre=$fila['tp_nombre'];
    $id_codigo_gestor = $fila['id_codigo_gestor'];
   

}
odbc_close($conn);
//echo $id."<br>"/;
//echo $tp_id."<br>"/;
//echo $usu_nombre."<br>"/;
//echo $nombre."<br>"/;
//echo $tp_nombre."<br>"/;
//require_once('../data/ed_usuario.php');
?>

<body>

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include("../layouts/menu_admin.php")?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
    <div id="content">

        <?php include("../layouts/navbar.php")?>
       
        <!-- Begin Page Content -->
            <div class="container-fluid">
      
                <div class="row">


                 <!-- Area Chart -->
            

                        <!-- Pie Chart -->
                        <div class="col-xl-8 col-lg-5">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Editar Usuario</h6> 
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                <form action="../data/ed_usuario.php" method="POST" autocomplete="OFF" >
                                <div class="form-row">
                                            <!-- aqui ira el select que evaluara que tipo de cliente quiere editar -->
                                            
                                            <!-- aqui finaliza tipo de cliente -->
                                        <div class="form-group col-md-6">
                                            <label for="inputEmail4">Nombre usuario</label>
                                            <input type="text" class="form-control" id="user_name" name="user_name" value="<?php echo $usu_nombre ?>" required>
                                            <input type="hidden" id="usu_id_i" name="usu_id_i" value="<?php echo $id ?>" >
                                           
                                        </div>
                                      
                                       
                                        <div class="form-group col-md-6">
                                            <label for="usu_nombre">Nombre Vendedor</label>
                                            <input type="text" class="form-control" id="ven_name" name="ven_name" value="<?php echo $nombre ?>" required>
                                            <input type="hidden" name="id_codigo_gestor" value="<?php echo $id_codigo_gestor; ?>">
                                        </div>
                                        <div class="form-group col-md-6">
                                                <label for="type_user">Tipo Usuario</label>
                                                <select id="type_user" class="form-control" name="type_user">                                                   
                                                    <?php $sql1="select * from prg.tp_tipos_usuarios ";
                                                        $ds=odbc_exec($conn,$sql1);
                                                        while($fila=odbc_fetch_array($ds))
                                                        {?>
                                                      
                                                       <option <?php if($tp_id == $fila['tp_id']) {echo "selected";}?> value="<?php echo $fila['tp_id']?>"><?php echo $fila['tp_nombre']?></option>
                                                        <?php } ?>
                                                </select>
                                        </div>
                                                    
                                    </div>                        
                                    <button type="submit" class="btn btn-primary">Actualizar</button>
                                </form>
                                    
                                </div>
                            </div>
                        </div>
                     
                    
                    </div>    
                    
            <!-- /.container-fluid -->
                </div>       
                </div>

        </div>
        </div>


     
<?php
require_once('../layouts/foother.php');
?>




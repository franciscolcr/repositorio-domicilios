<?php
require_once('../data/conexion.php');
$fecha_actual = date('Y-m-d');
require_once('../layouts/header.php');
$iden = $_COOKIE['tp_id'];
?>
<style>

</style>
<link href="../js/dist/css/select2.min.css" rel="stylesheet" />  

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php if ($_COOKIE['tp_id'] == 2) {
            include("../layouts/menu_sales.php");
        } else if ($_COOKIE['tp_id'] == 3) {
            include("../layouts/menu_dispatch.php");
        } ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <?php include("../layouts/navbar.php") ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3 d-flex justify-content-between align-items-center">
                            <h4>Reporte Domicilios/Viajes Entregados</h4>
                            <div id="mostrar">

                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="motivo">Seleccione el moto</label>
                                    <select name="motorista" id="motorista" class="form-control" name="motivo">
                                        <option value="0">Seleccione</option>
                                        <option value="0">TODOS</option>
                                        <?php $sql1 = "select * from prg.mo_motoristas where mo_estado=0";
                                        $ds = odbc_exec($conn, $sql1);
                                        while ($fila = odbc_fetch_array($ds)) { ?>
                                        <option value="<?php echo $fila['mo_id'] ?>"><?php echo $fila['mo_nombre'] ?>
                                        </option>
                                        
                                        <?php }
                                       
                                        odbc_close($conn); ?>
                                    </select>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="periodo">Fecha Inicio</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='date' class="form-control" id="finicio" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="periodo">Fecha Fin</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='date' class="form-control" id="ffin" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <br>
                                    <button class="btn btn-primary" onclick="buscar()">Buscar</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="domicilios_moto" class="table table-bordered" width="100%"
                                        cellspacing="0"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!--p modal ara ver los detalles del domicilio -->
            <div class="modal fade bd-example-modal-lg" id="ver_dm" tabindex="-1" role="dialog"
                aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Detalles del Domicilio</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" autocomplet="OFF">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="codigo">Codigo del Domicilio</label>
                                            <input type="text" class="form-control" disabled id="codigo_dm_ver"
                                                name="codigo_dm_ver">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="codigo">Tipo de Domicilio</label>
                                            <input type="text" class="form-control bg-success text-white" disabled
                                                id="tipo_dm_ver" name="tipo_dm_ver">
                                        </div>
                                    </div>
                                    <!-- venta -->
                                    <div id="divVenta" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">Código de Documento</label>
                                                <input type="text" class="form-control bg-primary text-white"
                                                    id="codigodoc" disabled>
                                            </div>
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUDocumento</label>
                                                <input type="text" class="form-control bg-primary text-white"
                                                    id="dm_doc" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos" class="table table-bordered" width="100%"
                                                    cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Traslado -->
                                    <div id="divTraslado" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">Código de Traslado</label>
                                                <input type="text" class="form-control bg-primary text-white"
                                                    id="codigodoc1" disabled>
                                            </div>
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUTraslado</label>
                                                <input type="text" class="form-control bg-primary text-white"
                                                    id="dm_doct" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_traslados" class="table table-bordered"
                                                    width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Cobros -->
                                    <div id="divCobros" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUCobro</label>
                                                <input type="text" class="form-control bg-primary text-white"
                                                    id="dm_docc" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_cobros" class="table table-bordered"
                                                    width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- recoger muestra -->
                                    <div id="divMuestra" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLURmuestra</label>
                                                <input type="text" class="form-control bg-primary text-white"
                                                    id="dm_docm" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_muestra" class="table table-bordered"
                                                    width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Admin -->
                                    <div id="divAdmin" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUAdmnistrativo</label>
                                                <input type="text" class="form-control bg-primary text-white"
                                                    id="dm_doc_ad" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_admin" class="table table-bordered"
                                                    width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- CP -->
                                    <div id="divCP" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUCP</label>
                                                <input type="text" class="form-control bg-primary text-white"
                                                    id="dm_doc_cp" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_cp" class="table table-bordered" width="100%"
                                                    cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- taller -->
                                    <div id="divTaller" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <label for="dm_doc">PLUTaller</label>
                                                <input type="text" class="form-control bg-primary text-white"
                                                    id="dm_doc_taller" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mt-3">Detalles</label>
                                                <table id="table_productos_taller" class="table table-bordered"
                                                    width="100%" cellspacing="0"></table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 mt-2">
                                            <label for="motoristas_select">Nombre del vendedor</label>
                                            <input type="text" class="form-control" disabled id="vendedor_dm_ver"
                                                name="vendedor_dm_ver">
                                        </div>
                                        <div class="col-md-3 mt-2">
                                            <label for="motoristas_select">Fecha Creacion</label>
                                            <input type="text" class="form-control" disabled id="fecha_dm_ver"
                                                name="fecha_dm_ver">
                                        </div>
                                        <div class="col-md-3 mt-2">
                                            <label for="motoristas_select">Hora Creacion</label>
                                            <input type="text" class="form-control" disabled id="hora_dm_ver"
                                                name="hora_dm_ver">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="observaciones_dm_ver">Observaciones</label>
                                            <textarea id="observaciones_dm_ver" class="form-control bg-info text-white"
                                                disabled></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="observaciones_dm_ver">Motortista</label>
                                            <input id="motorista_dm_ver" class="form-control" disabled />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="observaciones_dm_ver">Fecha Asignacion</label>
                                            <input id="fasignacion_dm_ver" class="form-control" disabled />
                                        </div>
                                        <div class="col-md-6">
                                            <label for="observaciones_dm_ver">Hora Asignacion</label>
                                            <input id="hasignacion_dm_ver" class="form-control" disabled />
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
                        </div>
                        </form>

                    </div>
                </div>
            </div>

            <?php
            require_once('../layouts/foother.php');
            ?>

            <script>
            function buscar() {

                var motorista = $('#motorista').val();
                var finicio = $('#finicio').val();
                var ffin = $('#ffin').val();

                var tabla = '#domicilios_moto';


                if (motorista == 0) {
                    alert('Debes Seleccionar un Motorista');
                } else if (finicio == '') {
                    alert('Debes ingresar una Fecha de Inicio');
                } else if (ffin == '') {
                    alert('Debes ingresar una Fecha de Fin');
                } else {

                    var dataPrint = '<a href="../data/reporte_excel.php?motorista=' + motorista + '&finicio=' +
                        finicio + '&ffin=' + ffin +
                        '" class="btn btn-success" > <i class="fa fa-file-excel"></i> Imprimir Excel</a><a href="../data/reporte_pdf.php?motorista=' +
                        motorista + '&finicio=' + finicio + '&ffin=' + ffin +
                        '" class="btn btn-danger" target="_blank" id="reportePDF"> <i class="fa fa-file-pdf"></i> Imprimir PDF</a>';

                    $("#mostrar").html(dataPrint);
                    $.ajax({
                        data: {
                            motorista: motorista,
                            finicio: finicio,
                            ffin: ffin
                        },
                        url: '../data/trae_domicilios_motos.php',
                        dataType: 'html',
                        type: 'POST',
                        beforeSend: function() {
                            $(tabla).html(
                                '<center><img alt="Cargando Reporte..." src="../img/searching3.gif" /></center>'
                            );
                        },
                        success: function(html) {
                            setTimeout(function() {
                                $(tabla).html(html);
                            }, 2000)


                        }
                    });

                }

            }

            </script>
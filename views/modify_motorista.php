<?php
require_once('../data/conexion.php');
require_once('../layouts/header.php');

$id_motorista=$_GET['id'];

$sql1="select * from prg.mo_motoristas  where mo_id=$id_motorista";
$ds=odbc_exec($conn,$sql1);
while($fila=odbc_fetch_array($ds))
{
    $nombre=$fila['mo_nombre'];
    $observaciones=$fila['mo_observaciones'];
    $suc=$fila['mo_id_suc'];
    $vh=$fila['mo_id_vh'];
    $id=$fila['mo_id'];
    $img=$fila['mo_url_img'];
    $telefono=$fila['mo_telefono'];

}
//echo $sql1;
?>

<body>

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include("../layouts/menu_admin.php")?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
    <div id="content">

        <?php include("../layouts/navbar.php")?>
       
        <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Dashboard Sales</h1>
                </div>
               
                <div class="row">
                 <!-- Area Chart -->            

                        <!-- Pie Chart -->
                        <div class="col-xl-8 col-lg-5">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Editar Motorista</h6> 
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                <form action="../data/ed_motorista.php" method="POST" autocomplete="OFF" enctype="multipart/form-data" >
                                <div class="form-row">
                                
                                        <div class="form-group col-md-6">
                                            <label for="inputEmail4">Nombre Completo</label>
                                            <input type="text" class="form-control" id="mo_nombre" name="mo_nombre" value="<?php echo $nombre ?>" required>
                                            <input type="hidden" id="id_mo" name="id_mo" value="<?php echo $id ?>" >
                                           
                                        </div>                                                                              
                                        <div class="form-group col-md-6">
                                            <label for="mo_id_suc">Sucursal</label>
                                                <select id="mo_id_suc" name="mo_id_suc" class="form-control">
                                                    <?php $sql1="select * from prg.almacen ";
                                                    $ds=odbc_exec($conn,$sql1);
                                                    while($fila=odbc_fetch_array($ds))
                                                    {?>
                                                        <option <?php if($suc == $fila['id']) {echo "selected";}?> value="<?php echo $fila['id']?>"><?php echo $fila['nombre']?></option>
                                                    <?php }   ?>
                                                </select>
                                        </div>

                                        <div class="form-group col-md-6">                                        
                                            <label for="mo_id_vh">vehiculo</label>
                                                <select id="mo_id_vh" name="mo_id_vh" class="form-control">
                                                    <?php $sql1="select*from prg.mr_marcas_vehiculos";
                                                    $ds=odbc_exec($conn,$sql1);
                                                    while($fila=odbc_fetch_array($ds))
                                                    {?>
                                                        <option <?php if($vh == $fila['vh_id']) {echo "selected";}?> value="<?php echo $fila['vh_id']?>"><?php echo $fila['mr_nombre']?></option>
                                                    <?php }   odbc_close($conn);?>
                                                </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="telefono">Telefono</label>
                                            <input type="text" class="form-control" id="mo_telefono" name="mo_telefono" value="<?php echo $telefono ?>" >

                                        </div>
                                       
                                        <div class="form-group col-md-6">
                                            <label for="mo_observaciones">Observaciones</label>
                                            <input type="text" class="form-control" id="mo_observaciones" name="mo_observaciones" value="<?php echo $observaciones ?>" >
                                           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div class="mb-4">
                                                <img src="../fotos_motoristas/<?php echo $img; ?>" alt="" width="150px">
                                            </div>
                                            <input type="file" class="form-control-file" id="foto" name="foto" accept=".jpg, .jpeg, .png">
                                           
                                        </div>  

                                    </div>                        
                                    <button type="submit" class="btn btn-primary">Actualizar</button>
                                </form>
                                    
                                </div>
                            </div>
                        </div>
                
                </div>       
                </div>

        </div>


     
<?php
require_once('../layouts/foother.php');
?>

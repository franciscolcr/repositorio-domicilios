<?php
require_once('../data/conexion.php');
require_once('../layouts/header.php');
$id_cli = $_GET['id'];
?>
<style>
    #geomap {
        width: 100%;
        height: 400px;
    }
</style>


<body>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include("../layouts/menu_sales.php") ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include("../layouts/navbar.php") ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->

                    <div class="row">
                        <div class="col-xl-6 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Registrar Cliente</h6>

                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <form action="../data/model_create_direc.php" method="POST" autocomplete="OFF">

                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="placa">Direcciòn</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control bg-light border-0 small" id="search_location" name="search_location" required>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary get_map" type="submit">
                                                            <i class="fas fa-search fa-sm"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <div id="geomap"></div>
                                                <input type="hidden" class="search_addr" size="65" id="direccion" name="direccion">
                                                <input type="hidden" class="search_latitude" size="20" id="latitude" name="latitude">
                                                <input type="hidden" class="search_longitude" size="20" id="longitude" name="longitude">
                                                <input type="hidden" id="cli_id" name="cli_id" value="<?php echo $id_cli ?>">
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Guardar</button>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <?php
            require_once('../layouts/foother.php');
            ?>

            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3cejbHkJOZfpHllVqF0bApcL2j0yKU8Y&libraries=places"></script>
            <script>
                $(document).ready(function() {
                    $('#telefono').mask('0000-0000');
                    $('#dui').mask('00000000-0');
                    $('#nit').mask('0000-000000-000-0');
                    $('#nrc').mask('000000');
                    //$('#nit_mt').mask('0000-000000-000-0');


                });


                var geocoder;
                var map;
                var marker;

                /*
                 * Google Map with marker
                 */
                function initialize() {
                    var initialLat = $('.search_latitude').val();
                    var initialLong = $('.search_longitude').val();
                    initialLat = initialLat ? initialLat : 13.6871936;
                    initialLong = initialLong ? initialLong : -89.2469248;
                    var input1 = document.getElementById("search_location");
                    var autocomplete1 = new google.maps.places.Autocomplete(input1);
                    var latlng = new google.maps.LatLng(initialLat, initialLong);
                    var options = {
                        zoom: 16,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    map = new google.maps.Map(document.getElementById("geomap"), options);

                    geocoder = new google.maps.Geocoder();

                    marker = new google.maps.Marker({
                        map: map,
                        draggable: true,
                        position: latlng
                    });

                    google.maps.event.addListener(marker, "dragend", function() {
                        var point = marker.getPosition();
                        map.panTo(point);
                        geocoder.geocode({
                            'latLng': marker.getPosition()
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                map.setCenter(results[0].geometry.location);
                                marker.setPosition(results[0].geometry.location);
                                $('.search_addr').val(results[0].formatted_address);
                                $('.search_latitude').val(marker.getPosition().lat());
                                $('.search_longitude').val(marker.getPosition().lng());
                            }
                        });
                    });

                }

                $(document).ready(function() {
                    //load google map
                    initialize();

                    /*
                     * autocomplete location search
                     */
                    var PostCodeid = '#search_location';
                    $(function() {
                        $(PostCodeid).autocomplete({
                            source: function(request, response) {
                                geocoder.geocode({
                                    'address': request.term
                                }, function(results, status) {
                                    response($.map(results, function(item) {
                                        return {
                                            label: item.formatted_address,
                                            value: item.formatted_address,
                                            lat: item.geometry.location.lat(),
                                            lon: item.geometry.location.lng()
                                        };
                                    }));
                                });
                            },
                            select: function(event, ui) {
                                $('.search_addr').val(ui.item.value);
                                $('.search_latitude').val(ui.item.lat);
                                $('.search_longitude').val(ui.item.lon);
                                var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                                marker.setPosition(latlng);
                                initialize();
                            }
                        });
                    });

                    /*
                     * Point location on google map
                     */
                    $('.get_map').click(function(e) {
                        var address = $(PostCodeid).val();
                        geocoder.geocode({
                            'address': address
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                map.setCenter(results[0].geometry.location);
                                marker.setPosition(results[0].geometry.location);
                                $('.search_addr').val(results[0].formatted_address);
                                $('.search_latitude').val(marker.getPosition().lat());
                                $('.search_longitude').val(marker.getPosition().lng());
                            } else {
                                alert("Geocode was not successful for the following reason: " + status);
                            }
                        });
                        e.preventDefault();
                    });

                    //Add listener to marker for reverse geocoding
                    google.maps.event.addListener(marker, 'drag', function() {
                        geocoder.geocode({
                            'latLng': marker.getPosition()
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    $('.search_addr').val(results[0].formatted_address);
                                    $('.search_latitude').val(marker.getPosition().lat());
                                    $('.search_longitude').val(marker.getPosition().lng());
                                }
                            }
                        });
                    });
                });

                $(document).ready(function() {
                    $('#cliente_mt').select2({
                        ajax: {
                            url: "../data/list_clientes.php",
                            type: "post",
                            dataType: 'json',
                            delay: 250,
                            data: function(params) {
                                return {
                                    searchTerm: params.term // search term
                                };
                            },
                            processResults: function(response) {
                                return {
                                    results: response
                                };
                            },
                            cache: true
                        },
                        width: '100%',

                    });

                    $('#cod_mt').select2({
                        ajax: {
                            url: "../data/list_cliente_cod.php",
                            type: "post",
                            dataType: 'json',
                            delay: 250,
                            data: function(params) {
                                return {
                                    searchTerm: params.term // search term
                                };
                            },
                            processResults: function(response) {
                                return {
                                    results: response
                                };
                            },
                            cache: true
                        },
                        width: '100%'

                    });

                    $('#nit_mt').select2({
                        ajax: {
                            url: "../data/list_cliente_nit.php",
                            type: "post",
                            dataType: 'json',
                            delay: 250,
                            data: function(params) {
                                return {
                                    searchTerm: params.term // search term
                                };
                            },
                            processResults: function(response) {
                                return {
                                    results: response
                                };
                            },
                            cache: true
                        },
                        width: '100%',
                        placeholder: "0000-000000-000-0",
                        allowClear: true,
                        separator: '-'

                    });
                });
            </script>
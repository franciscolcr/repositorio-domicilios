<?php
require_once('../data/conexion.php');

require_once('../layouts/header.php');

?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include("../layouts/menu_sales.php") ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include("../layouts/navbar.php") ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">



                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Lista De Clientes</h6>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="300px">Nombre</th>
                                            <th>Celular</th>
                                            <th width="200px">Sucursal Creacion</th>
                                            <th>TIPO CLIENTE</th>
                                            <th>Nit</th>
                                            <th>Dui</th>
                                            <th>Nrc</th>
                                            <th>Correo</th>
                                            <th>Accciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql1 = "SELECT * FROM  prg.cli_clientes 
                                            JOIN prg.divisiones ON cli_id_suc=PLUDivision 
                                            WHERE cli_estado=0 ORDER BY cli_fcreacion DESC ";
                                        $ds = odbc_exec($conn, $sql1);
                                        while ($fila = odbc_fetch_array($ds)) {

                                            $tipo = "";
                                            if ($fila['cli_tipo'] == 1) {
                                                $tipo = "CLIENTE NORMAL";
                                            }
                                            if ($fila['cli_tipo'] == 2) {
                                                $tipo = "CLIENTE EMPRESARIAL";
                                            }
                                            if ($fila['cli_tipo'] == 3) {
                                                $tipo = "TRASLADO SUCURSAL";
                                            }
                                            if ($fila['cli_tipo'] == 4) {
                                                $tipo = "PROVEEDOR";
                                            }
                                        ?>
                                        <tr>
                                            <td><?php echo $fila['cli_nombre'] ?> </td>

                                            <td><?php echo $fila['cli_celular'] ?></td>
                                            <td><?php echo $fila['Nombre'] ?></td>
                                            <td><?php echo $tipo ?></td>
                                            <td><?php echo $fila['cli_nit'] ?></td>
                                            <td><?php echo $fila['cli_dui'] ?></td>
                                            <td><?php echo $fila['cli_nrc'] ?></td>
                                            <td><?php echo $fila['cli_email'] ?></td>
                                            <td>
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button"
                                                        id="dropdownMenuButton" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                        Opciones
                                                    </button>
                                                    <div class="dropdown-menu animated--fade-in"
                                                        aria-labelledby="dropdownMenuButton">
                                                        <a class="dropdown-item"
                                                            href="../views/info_client.php?id=<?php echo $fila['cli_id'] ?>">Info
                                                            Cliente</a>
                                                        <li>
                                                            <hr class="dropdown-divider">
                                                        </li>
                                                        <a class="dropdown-item"
                                                            href="../views/modify_cliente.php?id=<?php echo $fila['cli_id'] ?>">Editar
                                                            Cliente</a>
                                                        <li>
                                                            <hr class="dropdown-divider">
                                                        </li>
                                                        <a class="dropdown-item" href="#"
                                                            onclick="eliminar(<?php echo $fila['cli_id'] ?>);">Eliminar
                                                            Cliente</a>
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>

                                        <?php
                                        }
                                        odbc_close($conn);
                                        ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
            <!--modal cliente normal o empresarial-->

            <div class="modal fade bd-example-modal-lg" id="info_cli" tabindex="-1" role="dialog"
                aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Informacion Del Cliente</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form autocomplet="OFF">
                                <div class="container-fluid">
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Nombre Cliente</label>
                                        <input type="text" class="form-control" id="cli_nombre" name="cli_nombre"
                                            value="<?php echo $nombre ?>" required>
                                        <input type="hidden" id="cli_id_i" name="cli_id_i" value="<?php echo $id ?>">

                                    </div>
                                    <div class="form-group col-md-6">

                                        <label for="cli_id_suc">Sucursal</label>
                                        <select id="cli_id_suc" name="cli_id_suc" class="form-control">
                                            <?php $sql1 = "select * from prg.divisiones ";
                                            $ds = odbc_exec($conn, $sql1);
                                            while ($fila = odbc_fetch_array($ds)) { ?>
                                            <option <?php if ($suc == $fila['PLUDivision']) {
                                                            echo "selected";
                                                        } ?> value="<?php echo $fila['PLUDivision'] ?>">
                                                <?php echo $fila['Nombre'] ?></option>
                                            <?php }
                                            odbc_close($conn); ?>

                                        </select>

                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="cli_celular">celular</label>
                                        <input type="text" class="form-control" id="cli_celular" name="cli_celular"
                                            value="<?php echo $celu ?>" required>

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="cli_dui">Dui</label>
                                        <input type="text" class="form-control" id="cli_dui" name="cli_dui"
                                            value="<?php echo $dui ?>">

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="cli_nit">Nit</label>
                                        <input type="text" class="form-control" id="cli_nit" name="cli_nit"
                                            value="<?php echo $nit ?>" required>

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="cli_nrc">Nrc</label>
                                        <input type="text" class="form-control" id="cli_nrc" name="cli_nrc"
                                            value="<?php echo $nrc ?>">

                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="comparar();"
                                data-dismiss="modal">Guardar</button>
                        </div>
                        </form>

                    </div>
                </div>
            </div>

            <!--fin modal cliente normal o empresarial-->
            <?php
            require_once('../layouts/foother.php');
            ?>
            <script>
            $("#dataTable").DataTable();

            function eliminar(cli_id) {
                var cli_id = cli_id;
                bootbox.confirm({
                    message: "Se Eliminara el cliente, Decea Proceder?",
                    buttons: {
                        confirm: {
                            label: 'Si',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-danger'
                        }
                    },
                    callback: function(result) {
                        if (result == true) {
                            document.location.href = '../data/delete_cliente.php?id=' + cli_id;
                        }

                        // console.log('This was logged in the callback: ' + result);
                    }
                });
            }

            function info_cli() {
                $('#info_cli').modal('show');

            }
            </script>
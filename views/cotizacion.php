<?php
require_once('../data/conexion.php');
$fecha_actual = date('Y-m-d');
require_once('../layouts/header.php');
$iden = $_COOKIE['tp_id'];

$sv = file_get_contents("C:\PUBLIC\SERVER.ID", "r");
$servidora = trim($sv);

$servidora = "SELECT *FROM PRG.servidorasm where Nombre='$servidora'";
$serv = odbc_exec($conn, $servidora);
while ($filasv = odbc_fetch_array($serv)) {
    $servi = $filasv['ServidoraId'];
}
//echo $servi;
?>
<style>

</style>
<link href="../js/dist/css/select2.min.css" rel="stylesheet" />
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php if ($_COOKIE['tp_id'] == 2) {
            include("../layouts/menu_sales.php");
        } else if ($_COOKIE['tp_id'] == 3) {
            include("../layouts/menu_dispatch.php");
        } ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <?php include("../layouts/navbar.php") ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    </div>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h4>Enviar Cotización</h4>
                        </div>
                        <div class="card-body">
                            <form id="cotizacion" name="cotizacion" method="POST" autocomplete="OFF">
                                <!--new domicilioooo-->
                                <div class="row">
                                    <div class=" col-md-3">
                                        <label for="ndoc">Numero Documento</label> <br>
                                        <select class="form-control" name="ndoc" id="ndoc">
                                            <option value="0" selected>Seleccione un documento</option>
                                            <?php $sql1 = "select PLUDocCliente,Codigo,Tipo,Fecha from prg.docclientesm where fecha between '" . date("Y-m-d", strtotime($fecha_actual . "- 100 days")) . "' and '" . date("Y-m-d", strtotime($fecha_actual . "+ 1 days")) . "' and  plugestor = " . $_COOKIE['codigo_gestor'] . " and Cerrado = 'T' and tipo in ('CTFC','CTCF') order by fecha desc";;
                                            $ds = odbc_exec($conn, $sql1);
                                            while ($fila = odbc_fetch_array($ds)) { ?>
                                                <option value="<?php echo $fila['PLUDocCliente'] ?>"><?php echo $fila['Codigo'] . " - " . $fila['Tipo'] . " - " . date('d/m/Y', strtotime($fila['Fecha'])) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class=" col-md-4">
                                        <label for="ndoc">Nombre de Cliente</label> <br>
                                        <select class="form-control" name="cli_id" id="cli_id">
                                            <option value="0" selected>Seleccione el Cliente</option>
                                            <?php $sql1 = "select * from prg.cli_clientes where cli_estado = 0 AND cli_tipo<4";
                                            $ds = odbc_exec($conn, $sql1);
                                            while ($fila = odbc_fetch_array($ds)) { ?>
                                                <option value="<?php echo $fila['cli_id'] ?>"><?php echo $fila['cli_nombre'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-2 ">
                                        <label for="">Con Descuento</label><br>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="descuento" id="descuento1" value="Si">
                                            <label class="form-check-label" for="descuento1">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="descuento" id="descuento2" value="No" checked>
                                            <label class="form-check-label" for="descuento2">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mt-3" id="radio">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 mt-3">
                                        <button type="button" class="btn btn-primary" onclick="buscar()">Buscar Datos</button>
                                    </div>
                                    <div class="col-md-2 mt-3" id="mostrar">
                                    </div>
                                    <div class="col-md-2 mt-3" id="envioEmail">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" id="datoscotizacion" style="display:none;">
                                    </div>
                                    <div class="col-md-12" id="datoscotizacionPDF" style="display:none;">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="loading" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <img src="../img/procesando.gif" width="250px;" style="display:block; margin:auto;" />

            </div>
        </div>
    </div>
</body>
<?php
require_once('../layouts/foother.php');

?>
<script src="../js/ajax/cotizacion.js?v=<?php echo time(); ?>"></script>
<script>
    //select para select2
    $(document).ready(function() {
        $('#ndoc').select2({
            theme: 'classic',
            width: '100%',
            placeholder: "Selecccione un documento",
            allowClear: true
        });
        $('#cli_id').select2({
            theme: 'classic',
            width: '100%',
            placeholder: "Selecccione un documento",
            allowClear: true
        });

    });
</script>
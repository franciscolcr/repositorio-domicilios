<?php
require_once('../data/conexion.php');
require_once('../layouts/header.php');
?>

<body>

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include("../layouts/menu_admin.php")?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
    <div id="content">

        <?php include("../layouts/navbar.php")?>
       
        <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Dashboard Sales</h1>
                </div>

               
                <div class="row">


                 <!-- Area Chart -->
                 <div class="col-xl-6 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Nuevo Vehiculo</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                <form action="../data/model_create_vehiculo.php" method="POST" autocomplete="OFF">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="tipo_vehiculo">Tipo</label>
                                                <select id="tipo_vehiculo" class="form-control form-control-sm" name="tipo_vehiculo">                                                   
                                                    <option value="1" selected>Moto</option>
                                                    <option value="2">Camion</option>
                                                    <option value="3">Otro</option>
                                                </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                                <label for="marca_vehiculo">Marca</label>
                                                <select id="marca_vehiculo" class="form-control form-control-sm" name="marca_vehiculo">                                                   
                                                    <?php $sql1="select * from prg.mr_marcas_vehiculos where mr_estado = 0";
                                                        $ds=odbc_exec($conn,$sql1);
                                                        while($fila=odbc_fetch_array($ds))
                                                        {?>
                                                        <option value="<?php echo $fila['mr_id']?>"><?php echo $fila['mr_nombre']?></option>
                                                        <?php } ?>
                                                </select>
                                        </div>
                                        
                                            <div class="form-group col-md-6">
                                                <label for="modelo">Modelo</label>
                                                <input type="text" class="form-control" id="modelo" name="modelo" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="capaciad">Capacidad</label>
                                                <input type="text" class="form-control" id="capacidad" name="capacidad" required>
                                                <input type="hidden" value="<?php echo $_COOKIE['sucursal'] ?>" id="sucursal_vh" name="sucursal_vh">
                                            </div>
                                       
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="placa">Placa</label>
                                            <input type="text" class="form-control" id="placa" name="placa" >
                                            
                                        </div> 
                                                                       
                                        
                                        <div class="form-group col-md-6">
                                            
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </form>
                                    

                                </div>
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        <div class="col-xl-6 col-lg-5">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Nuevo Motorista</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                <form action="../data/model_create_motorista.php" method="POST" autocomplete="OFF" enctype="multipart/form-data">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputEmail4">Nombre Completo</label>
                                            <input type="text" class="form-control" id="nombre" name="nombre" required>
                                           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="sucursal">Sucursal</label>
                                                <select id="id_suc" name="id_suc" class="form-control">
                                                    <?php $sql1="select * from prg.almacen where id = ".$_COOKIE['sucursal']."";
                                                    $ds=odbc_exec($conn,$sql1);
                                                    while($fila=odbc_fetch_array($ds))
                                                    {?>
                                                    <option selected value="<?php echo $fila['id']?>"><?php echo $fila['nombre']?></option>
                                                    <?php }  ?>

                                                </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="vehiculo" >Vehiculo(Placa)</label>
                                            <select id="id_vh" class="form-control" name="id_vh" required>
                                                    <?php $sql1="select * from prg.vh_vehiculos where vh_estado = 0 and vh_id_suc = ".$_COOKIE['sucursal']."" ;
                                                    $ds=odbc_exec($conn,$sql1);
                                                    while($fila=odbc_fetch_array($ds))
                                                    {?>
                                                    <option selected value="<?php echo $fila['vh_id']?>"><?php echo $fila['vh_placa']?></option>
                                                    <?php } odbc_close($conn);?>

                                                </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="telefono">Telefono</label>
                                            <!-- <input type="text" class="form-control" id="phone" name="telefono" required> -->
                                            <input type="text" class="form-control" id="phone" name="phone" data-inputmask="'mask': '+33 9 99 99 99 99'" style="width: 17ch;">
                                        </div>
                                    </div>                   

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputAddress">Foto</label>
                                            <!--<input type="file" class="form-control-file" id="foto" name="foto" accept="image/*">--> 
                                            <input type="file" class="form-control-file" id="foto" name="foto" accept=".jpg, .jpeg, .png">
                                        </div>                             
                                    </div>
                                    <div class="form-row">                                        
                                        <div class="form-group col-md-12">                                       
                                            <label for="observacion">Observaciones</label>
                                            <input type="text" class="form-control" id="observaciones" name="observaciones" >                             
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </form>
                                    
                                </div>
                            </div>
                        </div>
                
                </div>       
                </div>

        </div>


     
        <?php
require_once('../layouts/foother.php');
?>

<script>
$(document).ready(function(){
    $('#phone').mask('0000-0000');
    $('#placa').mask('000000');
  });
</script>
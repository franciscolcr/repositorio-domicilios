<?php
require_once('../data/conexion.php');
require_once('../layouts/header.php');
?>

<body>

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include("../layouts/menu_admin.php")?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
    <div id="content">

        <?php include("../layouts/navbar.php")?>
       
        <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                </div>

               
                <div class="row">


                 <!-- Area Chart -->
                 <div class="col-xl-6 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Nuevo Usuario</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                <form action="../data/model_create_vendedor.php" method="POST" autocomplete="OFF">
                                    <div class="form-row">
                                       <input type="hidden" id="id_suc" name="id_suc" value="<?php echo $_COOKIE['sucursal']?>">
                                        <div class="form-group col-md-6">
                                                <label for="marca_vehiculo">Tipo Usuario</label>
                                                <select id="tipo_usuario" class="form-control" name="tipo_usuario">                                                   
                                                    <?php $sql1="select * from prg.tp_tipos_usuarios";
                                                        $ds=odbc_exec($conn,$sql1);
                                                        while($fila=odbc_fetch_array($ds))
                                                        {?>
                                                        <option value="<?php echo $fila['tp_id']?>"><?php echo $fila['tp_nombre']?></option>
                                                        <?php } ?>
                                                </select>
                                        </div>
                                        
                                            <div class="form-group col-md-6">
                                                <label for="modelo">Nombre Usuario</label>
                                                <input type="text" class="form-control" id="nombre_usuario" name="nombre_usuario" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="id_gestor">Vendedor</label>
                                                  
                                                <select id="id_gestor" class="form-control" name="id_gestor">                                                   
                                                    <?php $sql1="select * from prg.gestores where habilitado = 'T'";
                                                        $ds=odbc_exec($conn,$sql1);
                                                        while($fila=odbc_fetch_array($ds))
                                                        {?>
                                                        <option value="<?php echo $fila['PLUGestor']?>"><?php echo utf8_encode($fila['Nombre'])."-".$fila['Codigo']?></option>
                                                        <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="id_moto">Motociclista</label>
                                                <select id="id_moto" class="form-control" name="id_moto"> 
                                                    <option value="">Sin Moto</option>                                                  
                                                    <?php $sql1="SELECT * FROM prg.mo_motoristas WHERE mo_estado=0";
                                                        $ds=odbc_exec($conn,$sql1);
                                                        while($fila=odbc_fetch_array($ds))
                                                        {?>
                                                        <option value="<?php echo $fila['mo_id']?>"><?php echo $fila['mo_nombre']?></option>
                                                        <?php } ?>
                                                </select>
                                            </div>
                                       
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="placa">Contraseña</label>
                                            <input type="text" class="form-control" id="password" name="password" >
                                            
                                        </div> 
                                                                       
                                        
                                        <div class="form-group col-md-6">
                                            
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </form>
                                    

                                </div>
                            </div>
                        </div>

                        
                
                </div>       
                </div>

        </div>


     
<?php
require_once('../layouts/foother.php');
?>
<script>
    $(document).ready(function() {
    $('#id_gestor').select2({
        theme: 'classic',
        width: '100%',
        placeholder: "Selecccione un Vendedor",
        allowClear: true
    });
    
});
</script>

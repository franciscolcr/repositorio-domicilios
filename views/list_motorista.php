<?php
require_once('../data/conexion.php');

require_once('../layouts/header.php');
?>
<body id="page-top"     >

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include("../layouts/menu_admin.php")?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
    <div id="content">

        <?php include("../layouts/navbar.php")?>
       
        <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                </div>

                <div class="d-sm-flex align-items-center  mb-4">
            
                          
                </div>
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Motoristas Creados</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr style="text-align: center">
                                            <th>Foto</th>
                                            <th>Nombre Motorista</th>
                                            <th>vehiculo</th>
                                            <th>Sucursal</th>
                                            <th>Observaciones</th>
                                            <th>Acciones</th>                                        
                                        </tr>
                                    </thead>                                   
                                    <tbody>    
                                    <?php                        
                                            $sql1="select*from PRG.mo_motoristas join prg.vh_vehiculos vh on mo_id_vh=vh_id join prg.divisiones on mo_id_suc=PLUDivision 
                                            join prg.mr_marcas_vehiculos mr on vh.mr_id=mr.mr_id where mo_estado=0";
                                                $ds=odbc_exec($conn,$sql1);
                                                    while($fila=odbc_fetch_array($ds))
                                                        {
                                                            ?>
                                                            <tr>

                                                                <td><img src="<?php echo '../fotos_motoristas/'.$fila['mo_url_img'] ?>" width="80" height="70"> </td> 
                                                                <td><?php echo $fila['mo_nombre'] ?></td>
                                                                <td><?php echo $fila['vh_placa']."(".$fila['mr_nombre'].")" ?></td>
                                                                <td><?php echo $fila['Nombre'] ?></td>  
                                                                <td><?php echo $fila['mo_observaciones'] ?></td>  
                                                                <td>
                                                                    <a href="../views/modify_motorista.php?id=<?php echo $fila['mo_id']?>" class="btn btn-success" role="button" data-bs-toggle="button">Editar</a>
                                                                    <a href="#" class="btn btn-danger" role="button" onclick="eliminarmot(<?php echo $fila['mo_id']?>);";>Eliminar</a>
                                                                </td>   

                                                            </tr>
                                                            <?php
                                                        }
                                                odbc_close($conn);
                                    ?>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                    </div>    
                       

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

       
<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Se Cerra La Session?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">Asegurece de Haber guardado todo!</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-primary" href="../data/cerrar_session.php">Salir</a>
            </div>
        </div>
    </div>
</div>


<?php
require_once('../layouts/foother.php');
?>

<script>

function eliminarmot(mo_id){
    var mo_id = mo_id;
    bootbox.confirm({
            message: "Se Eliminara El Motorista, Decea Proceder?",
            buttons: {
                confirm: {
                    label: 'Si',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result == true){
                    document.location.href='../data/delete_motoristas.php?id=' + mo_id;
                }   
            }
        });
    
}
</script>

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
//import { query, where, orderBy, limit } from '@angular/fire/firestore/';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent  {

  palabra: any;


  title = 'maps';
  lat: number;
  lng: number;
  zoom: number;
  items: Motos[];
  init = false;
  siguiendoA: string = null;
  siguiendoNombre: string = null;
  placa_moto:string;
  telefono_moto:string;
  ultima_actualizacion:string;
  codigo:string = null;
  productos_factura: any[] = [];
  foto_moto:string = null;
  constructor(private route: ActivatedRoute,
    private db: AngularFirestore,
     private router: Router,
     private http: HttpClient){
    this.lat = 13.701668;
    this.lng = -89.2257807;
    this.zoom = 16;
    this.route.params
    .subscribe(params => {
      //console.log(params.termino);
      this.palabra = params.termino;
      this.codigo = this.palabra;
    });

    console.log(this.codigo);
    //const mapa = this.db.collection('motos').doc(String(this.codigo)).get();
    db.collection('motos').doc(String(this.codigo)).valueChanges().subscribe(
      (data: Motos[]) => {
        //console.log(data.latitude);
          this.items = data;
          if (!this.init){
            this.lat = data['latitude'];
            this.lng = data['longitude'];
            this.ultima_actualizacion = data['ultima_actualizacion'];
        }
        if ( this.codigo){
            //data.forEach( item => {
              //if (item.dm_codigo === this.codigo){
                  this.lat = data['latitude'];
                  this.lng = data['longitude'];
                  this.siguiendoNombre = data['nombre_moto'];
                  this.siguiendoA = data['dm_codigo'];
                  this.telefono_moto = data['telefono_moto'];
                  this.placa_moto = data['placa_moto'];
                  this.ultima_actualizacion = data['ultima_actualizacion'];
                  this.foto_moto = data['foto_moto'];

              //}
              this.palabra = '';
            //});
        }
    });


        console.log('latitude |'+this.lat);
        console.log('longitude |'+this.lng);
        console.log('ultima_actualizacion |'+this.ultima_actualizacion);

  //this.db.collection('motos').valueChanges().subscribe(val=>{ console.log(val)});
  //console.log(docRef.subscribe(response => {}));
    /* firestore.collection('motos').valueChanges().subscribe(
      (data: Motos[]) => {
          this.items = data;
          if (!this.init){
              this.lat = data[0].latitude;
              this.lng = data[0].longitude;
          }
          if ( this.codigo){
              data.forEach( item => {
                if (item.dm_codigo === this.codigo){
                    this.lat = item.latitude;
                    this.lng = item.longitude;
                    this.siguiendoNombre = item.nombre_moto;
                    this.siguiendoA = item.dm_codigo;
                    this.telefono_moto = item.telefono_moto;
                    this.placa_moto = item.placa_moto;
                    this.ultima_actualizacion = item.ultima_actualizacion;
                    this.foto_moto = item.foto_moto;

                }
                this.palabra = '';
              });
          }
    }); */
    //console.log(firestore.collection('motos').doc(this.codigo).valueChanges());

  }

// tslint:disable-next-line: typedef
seguir(item: Motos){
    //console.log(item);
    this.siguiendoA = item.dm_codigo;
    this.siguiendoNombre = item.nombre_moto;
    this.placa_moto = item.placa_moto;
    this.telefono_moto = item.telefono_moto;
    this.lat = item.latitude;
    this.lng = item.longitude;
    this.ultima_actualizacion = item.ultima_actualizacion;
    this.foto_moto = item.foto_moto;
    }

    seguir_codigo(codigo:string){

      this.codigo = codigo;

      this.http.get('https://domicilioslcr.com/domicilios_ver2/webservices/get_data_factura.php?codigo='+this.codigo)
      .subscribe( (datos: any[]) => {

        for (let index of Object.keys(datos)) {
          var est = datos[index];
          for(let x of est){
            this.productos_factura.push(x);
          }

        }
        /* console.log(this.lat);
        console.log(this.lng); */
      })

    }

// tslint:disable-next-line: typedef
dejar_seguir(){
  this.siguiendoNombre = null;
  this.siguiendoA = null;
  this.router.navigate(['index'])

}

  ngOnInit() {

    this.route.params
    .subscribe(params => {
      //console.log(params.termino);
      this.palabra = params.termino;
        this.seguir_codigo(this.palabra);


    });


    //this.db.collection('motos').valueChanges().subscribe(val=>console.log(val));
  }


}


interface Motos{
  dm_codigo: string;
  nombre_moto: string;
  latitude: number;
  longitude: number;
  clave: string;
  placa_moto: string;
  telefono_moto: string;
  ultima_actualizacion: string;
  foto_moto: string;
}

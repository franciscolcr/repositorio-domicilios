import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  
constructor(private router: Router){}



buscarProducto(termino: string){
  if( termino.length < 1){
    return;
  }
  this.router.navigate(['/search', termino]);
  //console.log(termino);
}
}



/*
13.734255
-89.218815
*/
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// https://myaccount.google.com/u/0/lesssecureapps?pli=1&rapt=AEjHL4Oe8LtqbkheiW5AWUL3zWn9SrzJN3kz_Keb_LHzkt4CR35AX8oKvYu7QV9Zh3n3oCqVGuWetLivL_TJqAQtWlov0jjlYQ
export const environment = {
  production: false,
  firebase: {
   /* apiKey: "AIzaSyC64cx0RImLWzZkCCsCpRfgXTPJrHN_4rE",
    authDomain: "tracking-1e98b.firebaseapp.com",
    projectId: "tracking-1e98b",
    storageBucket: "tracking-1e98b.appspot.com",
    messagingSenderId: "195945534295",
    appId: "1:195945534295:web:bc86fc85c2c2a20625a412",
    measurementId: "G-C6GNPY11SL"*/
  apiKey: "AIzaSyCpNFJLCbxP9guhcbTnXYIInteCSIK_s38",
  authDomain: "tracking-3738e.firebaseapp.com",
  databaseURL: "https://tracking-3738e-default-rtdb.firebaseio.com",
  projectId: "tracking-3738e",
  storageBucket: "tracking-3738e.appspot.com",
  messagingSenderId: "1066642989026",
  appId: "1:1066642989026:web:3f9b493f480a16bf91e3bb",
  measurementId: "G-M306ND83EQ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

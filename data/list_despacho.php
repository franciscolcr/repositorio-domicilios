<?php
header('Content-type: application/json');
require_once('conexion.php');
$fecha_actual = date('Y-m-d');
$sql1 = "SELECT mt_motivo,dm_observacion,dm_fcreacion,dm_date_start, dm_date_end, est_nombre,est_icon,dm_id,dm_codigo,dm_estado,cli_nombre, cli_id, dm_id_dir, dm_id_doc, dm_id_mt, d.Nombre as Nombre
    FROM prg.cli_clientes 
    JOIN prg.dm_domicilios on dm_id_cli = cli_id
    JOIN prg.divisiones as d on dm_id_suc = d.PLUDivision
    JOIN prg.mt_motivos ON dm_id_mt=mt_id 
    JOIN prg.estados_domicilios ON dm_estado=est_id WHERE dm_fcreacion BETWEEN '" . date("Y-m-d", strtotime($fecha_actual . "- 60 days")) . "' and '" . date("Y-m-d", strtotime($fecha_actual . "+ 1 days")) . "' AND dm_estado !=0   ORDER BY dm_estado ASC, dm_fcreacion DESC";
$ds = odbc_exec($conn, $sql1);
$data = array();
$impreso = '';
while ($fila = odbc_fetch_array($ds)) {
    $dm_id_doc = $fila['dm_id_doc'];
    if ($fila['dm_id_mt'] == 2 && $fila['dm_id_mt'] == 4) {
        $select2 = "SELECT Impreso FROM prg.docclientesm WHERE PLUDoccliente=$dm_id_doc and tipo in ('FC','CF', 'NC')";
        $sql2 = odbc_exec($conn, $select2);
        $impresoT = odbc_fetch_object($sql2);
        $impreso = $impresoT->Impreso;
    } else {
        $impreso = null;
    }
    //var_dump($impreso);
    $data[] = [
        "mt_motivo" => $fila['mt_motivo'], "dm_observacion" => $fila['dm_observacion'], 'dm_fcreacion' => $fila['dm_fcreacion'], 'est_nombre' => $fila['est_nombre'], 'est_icon' => $fila['est_icon'], 'dm_id' => $fila['dm_id'], 'dm_codigo' => utf8_encode($fila['dm_codigo']), 'dm_estado' => $fila['dm_estado'], 'cli_nombre' => utf8_encode($fila['cli_nombre']), 'dm_id_dir' => $fila['dm_id_dir'], "fecha_inicial" => $fila['dm_date_start'], "fecha_final" => $fila['dm_date_end'], "impresoT" => $impreso
    ];
}
echo json_encode(["list" => $data]);

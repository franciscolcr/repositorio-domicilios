<?php
require_once('conexion.php');
$mo_id = $_POST['mo_id'];

$con = "SELECT dm_codigo,dm_observacion,mt_motivo,cli_nombre,dm_id,des_id, 
(select count(*) from prg.des_destinos where des_id_mo = '$mo_id' and des_en_ruta = 0) n
FROM prg.des_destinos
JOIN prg.dm_domicilios on des_id_dm=dm_id
JOIN prg.mt_motivos on dm_id_mt=mt_id
LEFT OUTER JOIN prg.cli_clientes on dm_id_cli=cli_id
WHERE des_id_mo = '$mo_id' and des_en_ruta = 0 ";
$ds = odbc_exec($conn, $con);  ?>
<thead>
    <tr>
        <th width=1%>#</th>
        <th width=2%>Codigo</th>
        <th width=25%>Tipo</th>
        <th width=25%>Cliente</th>
        <th width=10%>Orden</th>
    </tr>
</thead>
<?php
//$RowNumber = odbc_num_rows($ds); 
$i = 0;
while ($fila = odbc_fetch_array($ds)) {
    $i++;
?>
    <tr>
        <td><?= $i ?></td>
        <td align="center"><?= utf8_encode($fila['dm_codigo'])  ?><input type="hidden" name="des_id[]" id="des_id" value="<?= $fila['des_id'] ?>"></td>
        <td><?= $fila['mt_motivo'] ?></td>
        <td><?= (utf8_encode(isset($fila['cli_nombre'])) ? utf8_encode($fila['cli_nombre']) : "Sin Cliente") ?></td>
        <td>
            <select name="orden[]" id="orden" class="form-control">
                <option value="0">No</option>
                <?php for ($ii = 0; $ii < $fila['n']; $ii++) {
                    $ij = $ii + 1;
                ?>

                    <option value="<?php echo $ij; ?>" <?php echo (($i == $ij) ? "selected" : "") ?>><?php echo $ij; ?></option>
                <?php } ?>

            </select>
        </td>
    </tr>
<?php

}

?>
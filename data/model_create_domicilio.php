<?php
require_once('conexion.php');

$motivo = $_POST['tipo'];

function generateRandomString($length = 5)
{
    return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
}

$codigo = generateRandomString();

if (isset($motivo)) {
    if ($motivo == 2) { // venta 
        $usu_id = $_COOKIE['usu_id'];
        $sucursal_creacion = $_COOKIE['sucursal'];
        $ndoc = $_POST['ndoc'];
        $cli_id = $_POST['cli_id'];
        $direccion = $_POST['direccion'];
        $observacion = strtoupper($_POST['observacion']);

        $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_doc,dm_id_cli,dm_id_dir,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
                VALUES ('$motivo','$ndoc','$cli_id','$direccion','$observacion','$usu_id','$sucursal_creacion','$codigo')";
        odbc_exec($conn, $sql);
        odbc_close($conn);

        echo '2';
    } else if ($motivo == 4) { // venta 
        $usu_id = $_COOKIE['usu_id'];
        $sucursal_creacion = $_COOKIE['sucursal'];
        $ndoc = $_POST['ndoc3'];
        $cli_id = $_POST['cli_id3'];
        $direccion = $_POST['direccion3'];
        $observacion = strtoupper($_POST['observacion3']);

        $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_doc,dm_id_cli,dm_id_dir,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
                VALUES ('$motivo','$ndoc','$cli_id','$direccion','$observacion','$usu_id','$sucursal_creacion','$codigo')";
        odbc_exec($conn, $sql);
        odbc_close($conn);

        echo '2';
    } else if ($motivo == 11) { //traslado de mercaderia
        $usu_id = $_COOKIE['usu_id'];
        $sucursal_origen = $_COOKIE['sucursal'];
        $sucursal_destino = $_POST['sucursal'];
        $ntraslado = $_POST['ntraslado'];
        $observaciont = strtoupper($_POST['otraslado']);
        //$bodegero = $_POST['bodegero'];
        $direccionSuc = $_POST['direccionSuc'];
        //$telefonoSuc = $_POST['telefonoSuc'];
        $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_cli,dm_id_dir,dm_id_doc,dm_id_suc_origen,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
        VALUES ('$motivo','$sucursal_destino','$direccionSuc','$ntraslado','$sucursal_origen','$observaciont','$usu_id','$sucursal_origen','$codigo')";
        odbc_exec($conn, $sql);
        odbc_close($conn);

        echo '2';
    } else if ($motivo == 6) { //recoger muestra jeu
        $sv = file_get_contents("C:\PUBLIC\SERVER.ID", "r");
        $servidora = trim($sv);
        $consul = "SELECT CancelacionContratoId parametro from prg.parametros where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);

        $parametro = odbc_fetch_object($sql1);
        $fecha_now = date('Y-m-d h:i:s a');
        $usu_id = $_COOKIE['usu_id'];
        $sucursal_creacion = $_COOKIE['sucursal'];
        $clientes_muestras = $_POST['clientes_muestras'];
        $direccioncliente = $_POST['direccioncliente_muestra'];
        $telefonocliente_muestra = $_POST['telefonocliente_muestra'];
        $observacion_muestra = $_POST['observacion_muestra'];
        /** INSERTAR EN LA TABLA DM_OTROS */
        $insert1 = "INSERT INTO prg.dm_otros(id,tipo_domicilio,n_orden,created_at,updated_at)
        VALUES ($parametro->parametro,$motivo,0,'$fecha_now','$fecha_now'); commit";
        odbc_exec($conn, $insert1);
        /** CONSULTAR EL PRODUCTO PARA EL DETALLE DE RECOGER MUESTRA */
        $consulP = "SELECT PLUProducto from prg.productos where nobasico ='muestra' ";
        $sql2 = odbc_exec($conn, $consulP);

        $PLUProducto = odbc_fetch_object($sql2);

        /** INSERTAR EN LA TABLA DM_OTROSD */
        $insert2 = "INSERT INTO prg.dm_otrosd (dm_otros_id,PLUProducto)
        VALUES ($parametro->parametro,$PLUProducto->PLUProducto); commit";
        odbc_exec($conn, $insert2);
        /** INSERTAR EN LA TABLA DM_DOMICILIOS */
        $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_cli,dm_id_dir,dm_id_doc,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
        VALUES ('$motivo','$clientes_muestras','$direccioncliente','$parametro->parametro','$observacion_muestra','$usu_id','$sucursal_creacion','$codigo')";
        odbc_exec($conn, $sql);



        $SUMA = $parametro->parametro + 1; // SUMO UNO MAS AL PARAMETRO
        $consul = "UPDATE  prg.parametros SET CancelacionContratoId='$SUMA' where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);
        odbc_close($conn);
        echo '2';
    } else if ($motivo == 7) { //compras en plaza
        $sv = file_get_contents("C:\PUBLIC\SERVER.ID", "r");
        $servidora = trim($sv);
        $consul = "SELECT CancelacionContratoId parametro from prg.parametros where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);

        $parametro = odbc_fetch_object($sql1);
        //echo ($parametro->parametro);

        $fecha_now = date('Y-m-d h:i:s a');
        $n_order = $_POST['n_order'];
        $clientes_compras = $_POST['clientes_compras'];
        $direccion = $_POST['direccioncliente_compras'];
        $observacion =  $_POST['ocompras'];
        $telefono = $_POST['telefonocliente_compras'];
        $usu_id = $_COOKIE['usu_id'];
        $sucursal_creacion = $_COOKIE['sucursal'];

        /** INSERTAR EN LA TABLA DM_OTROS */
        $insert1 = "INSERT INTO prg.dm_otros(id,tipo_domicilio,n_orden,created_at,updated_at)
        VALUES ($parametro->parametro,$motivo,'$n_order','$fecha_now','$fecha_now'); commit";
        odbc_exec($conn, $insert1);

        /** CONSULTAR EL PRODUCTO PARA EL DETALLE DE COMPRAS EN PLAZA */
        $consulP = "SELECT PLUProducto from prg.productos where nobasico ='cplaza' ";
        $sql2 = odbc_exec($conn, $consulP);

        $PLUProducto = odbc_fetch_object($sql2);

        /** INSERTAR EN LA TABLA DM_OTROSD */
        $insert2 = "INSERT INTO prg.dm_otrosd (dm_otros_id,PLUProducto)
        VALUES ($parametro->parametro,$PLUProducto->PLUProducto); commit";
        odbc_exec($conn, $insert2);

        /** INSERTAR EN LA TABLA DM_DOMICILIOS */
        $insert3 = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_dir,dm_id_cli,dm_id_doc,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
        VALUES ('$motivo','$direccion','$clientes_compras','$parametro->parametro','$observacion','$usu_id','$sucursal_creacion','$codigo'); commit";
        odbc_exec($conn, $insert3);

        $SUMA = $parametro->parametro + 1; // SUMO UNO MAS AL PARAMETRO
        $consul = "UPDATE  prg.parametros SET CancelacionContratoId='$SUMA' where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);
        odbc_close($conn);

        echo '2';
    } else if ($motivo == 1) { //cobro
        $sv = file_get_contents("C:\PUBLIC\SERVER.ID", "r");
        $servidora = trim($sv);
        $consul = "SELECT CancelacionContratoId parametro from prg.parametros where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);

        $parametro = odbc_fetch_object($sql1);
        $fecha_now = date('Y-m-d h:i:s a');
        $usu_id = $_COOKIE['usu_id'];
        $sucursal_creacion = $_COOKIE['sucursal'];
        $clientes_cobro = $_POST['clientes_cobro'];
        $direccioncliente_cobro = $_POST['direccioncliente_cobro'];
        $telefonocliente_cobro = $_POST['telefonocliente_cobro'];
        $observacion_cobro = $_POST['observacion_cobro'];

        /** INSERTAR EN LA TABLA DM_OTROS */
        $insert1 = "INSERT INTO prg.dm_otros(id,tipo_domicilio,n_orden,created_at,updated_at)
        VALUES ($parametro->parametro,$motivo,0,'$fecha_now','$fecha_now'); commit";
        odbc_exec($conn, $insert1);

        /** CONSULTAR EL PRODUCTO PARA EL DETALLE DE RECOGER MUESTRA */
        $consulP = "SELECT PLUProducto from prg.productos where nobasico ='cobros' ";
        $sql2 = odbc_exec($conn, $consulP);

        $PLUProducto = odbc_fetch_object($sql2);

        /** INSERTAR EN LA TABLA DM_OTROSD */
        $insert2 = "INSERT INTO prg.dm_otrosd (dm_otros_id,PLUProducto)
        VALUES ($parametro->parametro,$PLUProducto->PLUProducto); commit";
        odbc_exec($conn, $insert2);

        /** INSERTAR EN LA TABLA DM_DOMICILIOS */
        $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_cli,dm_id_dir,dm_id_doc,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
        VALUES ('$motivo','$clientes_cobro','$direccioncliente_cobro','$parametro->parametro','$observacion_cobro','$usu_id','$sucursal_creacion','$codigo')";
        odbc_exec($conn, $sql);

        $SUMA = $parametro->parametro + 1; // SUMO UNO MAS AL PARAMETRO
        $consul = "UPDATE  prg.parametros SET CancelacionContratoId='$SUMA' where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);
        odbc_close($conn);

        echo '2';
    } else if ($motivo == 12) { //ruta
        $sv = file_get_contents("C:\PUBLIC\SERVER.ID", "r");
        $servidora = trim($sv);
        $consul = "SELECT CancelacionContratoId parametro from prg.parametros where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);

        $parametro = odbc_fetch_object($sql1);
        $fecha_now = date('Y-m-d h:i:s a');
        $usu_id = $_COOKIE['usu_id'];
        $sucursal_creacion = $_COOKIE['sucursal'];
        $clientes_ruta = $_POST['clientes_ruta'];
        $direccioncliente_ruta = $_POST['direccioncliente_ruta'];
        $telefonocliente_ruta = $_POST['telefonocliente_ruta'];
        $observacion_ruta = $_POST['observacion_ruta'];

        /** INSERTAR EN LA TABLA DM_OTROS */
        $insert1 = "INSERT INTO prg.dm_otros(id,tipo_domicilio,n_orden,created_at,updated_at)
        VALUES ($parametro->parametro,$motivo,0,'$fecha_now','$fecha_now'); commit";
        odbc_exec($conn, $insert1);

        /** CONSULTAR EL PRODUCTO PARA EL DETALLE DE RECOGER MUESTRA */
        $consulP = "SELECT PLUProducto from prg.productos where nobasico ='cobros' ";
        $sql2 = odbc_exec($conn, $consulP);

        $PLUProducto = odbc_fetch_object($sql2);

        /** INSERTAR EN LA TABLA DM_OTROSD */
        $insert2 = "INSERT INTO prg.dm_otrosd (dm_otros_id,PLUProducto)
        VALUES ($parametro->parametro,$PLUProducto->PLUProducto); commit";
        odbc_exec($conn, $insert2);

        /** INSERTAR EN LA TABLA DM_DOMICILIOS */
        $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_cli,dm_id_dir,dm_id_doc,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
        VALUES ('$motivo','$clientes_ruta','$direccioncliente_ruta','$parametro->parametro','$observacion_ruta','$usu_id','$sucursal_creacion','$codigo')";
        odbc_exec($conn, $sql);

        $SUMA = $parametro->parametro + 1; // SUMO UNO MAS AL PARAMETRO
        $consul = "UPDATE  prg.parametros SET CancelacionContratoId='$SUMA' where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);
        odbc_close($conn);

        echo '2';
    } else if ($motivo == 4) { //devolucion
        $usu_id = $_COOKIE['usu_id'];
        $sucursal_creacion = $_COOKIE['sucursal'];
        $ndoc = $_POST['ndoc3'];
        $cli_id = $_POST['cli_id3'];
        $direccion = $_POST['direccion3'];
        $observacion = strtoupper($_POST['observacion3']);

        $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_doc,dm_id_cli,dm_id_dir,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
                VALUES ('$motivo','$ndoc','$cli_id','$direccion','$observacion','$usu_id','$sucursal_creacion','$codigo')";

        /* $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
        VALUES ('$motivo','$odevolucion','$usu_id','$sucursal_creacion','$codigo')"; */
        odbc_exec($conn, $sql);
        odbc_close($conn);

        echo '2';
    } else if ($motivo == 5) { //pedido pendiente
        $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_doc,dm_id_cli,dm_id_dir,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
        VALUES ('$motivo','$ndoc2','$cli_id','$direccion','$oppendiente','$usu_id','$sucursal_creacion','$codigo')";
        odbc_exec($conn, $sql);
        odbc_close($conn);

        echo '2';
    } else if ($motivo == 9) { //Administrativo
        $sv = file_get_contents("C:\PUBLIC\SERVER.ID", "r");
        $servidora = trim($sv);
        $consul = "SELECT CancelacionContratoId parametro from prg.parametros where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);

        $parametro = odbc_fetch_object($sql1);
        $fecha_now = date('Y-m-d h:i:s a');
        $usu_id = $_COOKIE['usu_id'];
        $sucursal_creacion = $_COOKIE['sucursal'];
        $clientes_Admin = $_POST['clientes_Admin'];
        $direccioncliente_Admin = $_POST['direccioncliente_Admin'];
        $telefonocliente_Admin = $_POST['telefonocliente_Admin'];
        $observaciones_Admin = $_POST['observaciones_Admin'];

        /** INSERTAR EN LA TABLA DM_OTROS */
        $insert1 = "INSERT INTO prg.dm_otros(id,tipo_domicilio,n_orden,created_at,updated_at)
        VALUES ($parametro->parametro,$motivo,0,'$fecha_now','$fecha_now'); commit";
        odbc_exec($conn, $insert1);

        /** CONSULTAR EL PRODUCTO PARA EL DETALLE DE RECOGER MUESTRA */
        $consulP = "SELECT PLUProducto from prg.productos where nobasico ='administrativo' ";
        $sql2 = odbc_exec($conn, $consulP);

        $PLUProducto = odbc_fetch_object($sql2);

        /** INSERTAR EN LA TABLA DM_OTROSD */
        $insert2 = "INSERT INTO prg.dm_otrosd (dm_otros_id,PLUProducto)
        VALUES ($parametro->parametro,$PLUProducto->PLUProducto); commit";
        odbc_exec($conn, $insert2);

        /** INSERTAR EN LA TABLA DM_DOMICILIOS */
        $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_cli,dm_id_dir,dm_id_doc,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
        VALUES ('$motivo','$clientes_Admin','$direccioncliente_Admin','$parametro->parametro','$observaciones_Admin','$usu_id','$sucursal_creacion','$codigo')";
        odbc_exec($conn, $sql);

        $SUMA = $parametro->parametro + 1; // SUMO UNO MAS AL PARAMETRO
        $consul = "UPDATE  prg.parametros SET CancelacionContratoId='$SUMA' where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);
        odbc_close($conn);

        echo '2';
    } else if ($motivo == 15) { //Retorno
        $sv = file_get_contents("C:\PUBLIC\SERVER.ID", "r");
        $servidora = trim($sv);
        $consul = "SELECT CancelacionContratoId parametro from prg.parametros where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);

        $parametro = odbc_fetch_object($sql1);
        $fecha_now = date('Y-m-d h:i:s a');
        $usu_id = $_COOKIE['usu_id'];
        $sucursal_creacion = $_COOKIE['sucursal'];
        $clientes_Retorno = $_POST['clientes_Retorno'];
        $direccioncliente_Retorno = $_POST['direccioncliente_Retorno'];
        $telefonocliente_Retorno = $_POST['telefonocliente_Retorno'];
        $observaciones_Retorno = $_POST['observaciones_Retorno'];

        /** INSERTAR EN LA TABLA DM_OTROS */
        $insert1 = "INSERT INTO prg.dm_otros(id,tipo_domicilio,n_orden,created_at,updated_at)
        VALUES ($parametro->parametro,$motivo,0,'$fecha_now','$fecha_now'); commit";
        odbc_exec($conn, $insert1);

        /** CONSULTAR EL PRODUCTO PARA EL DETALLE DE RECOGER MUESTRA */
        $consulP = "SELECT PLUProducto from prg.productos where nobasico ='administrativo' ";
        $sql2 = odbc_exec($conn, $consulP);

        $PLUProducto = odbc_fetch_object($sql2);

        /** INSERTAR EN LA TABLA DM_OTROSD */
        $insert2 = "INSERT INTO prg.dm_otrosd (dm_otros_id,PLUProducto)
        VALUES ($parametro->parametro,$PLUProducto->PLUProducto); commit";
        odbc_exec($conn, $insert2);

        /** INSERTAR EN LA TABLA DM_DOMICILIOS */
        $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_cli,dm_id_dir,dm_id_doc,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
        VALUES ('$motivo','$clientes_Retorno','$direccioncliente_Retorno','$parametro->parametro','$observaciones_Retorno','$usu_id','$sucursal_creacion','$codigo')";
        odbc_exec($conn, $sql);

        $SUMA = $parametro->parametro + 1; // SUMO UNO MAS AL PARAMETRO
        $consul = "UPDATE  prg.parametros SET CancelacionContratoId='$SUMA' where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);
        odbc_close($conn);

        echo '2';
    } else if ($motivo == 14) { // TALLER
        $sv = file_get_contents("C:\PUBLIC\SERVER.ID", "r");
        $servidora = trim($sv);
        $consul = "SELECT CancelacionContratoId parametro from prg.parametros where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);

        $parametro = odbc_fetch_object($sql1);
        //echo ($parametro->parametro);

        $fecha_now = date('Y-m-d h:i:s a');
        $n_order = $_POST['n_order'];
        $clientes_taller = $_POST['clientes_taller'];
        $direccion = $_POST['direccioncliente_taller'];
        $observacion =  $_POST['otaller'];
        $telefono = $_POST['telefonocliente_taller'];
        $usu_id = $_COOKIE['usu_id'];
        $sucursal_creacion = $_COOKIE['sucursal'];

        /** INSERTAR EN LA TABLA DM_OTROS */
        $insert1 = "INSERT INTO prg.dm_otros(id,tipo_domicilio,n_orden,created_at,updated_at)
        VALUES ($parametro->parametro,$motivo,'$n_order','$fecha_now','$fecha_now'); commit";
        odbc_exec($conn, $insert1);

        /** CONSULTAR EL PRODUCTO PARA EL DETALLE DE COMPRAS EN PLAZA */
        $consulP = "SELECT PLUProducto from prg.productos where nobasico ='cplaza' ";
        $sql2 = odbc_exec($conn, $consulP);

        $PLUProducto = odbc_fetch_object($sql2);

        /** INSERTAR EN LA TABLA DM_OTROSD */
        $insert2 = "INSERT INTO prg.dm_otrosd (dm_otros_id,PLUProducto)
        VALUES ($parametro->parametro,$PLUProducto->PLUProducto); commit";
        odbc_exec($conn, $insert2);

        /** INSERTAR EN LA TABLA DM_DOMICILIOS */
        $insert3 = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_id_dir,dm_id_cli,dm_id_doc,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
        VALUES ('$motivo','$direccion','$clientes_taller','$parametro->parametro','$observacion','$usu_id','$sucursal_creacion','$codigo'); commit";
        odbc_exec($conn, $insert3);

        $SUMA = $parametro->parametro + 1; // SUMO UNO MAS AL PARAMETRO
        $consul = "UPDATE  prg.parametros SET CancelacionContratoId='$SUMA' where servidora='$servidora'";
        $sql1 = odbc_exec($conn, $consul);
        odbc_close($conn);

        echo '2';
    } else if ($motivo == 10) { //operaciones en banco
        $sql = "INSERT INTO prg.dm_domicilios (dm_id_mt,dm_observacion,dm_id_usu,dm_id_suc,dm_codigo) 
        VALUES ('$motivo','$obanco','$usu_id','$sucursal_creacion','$codigo')";
        odbc_exec($conn, $sql);
        odbc_close($conn);

        echo '2';
    }
} else {
    echo "
    <script>
        alert('DEBES MANDAR DATOS EN EL FORMULARIO');
        window.location='../views/create_domicilios.php';
    </script>";
}

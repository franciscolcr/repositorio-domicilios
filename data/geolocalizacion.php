<?php
header('Content-type: application/json');
require_once('conexion.php');


$idDireccion = $_GET['idDireccion'];
$sql = "SELECT * FROM prg.dir_direcciones WHERE dir_id=$idDireccion ";

$ds = odbc_exec($conn, $sql);
$data = array();
while ($fila = odbc_fetch_array($ds)) {
    $data[] = ["id" => $fila['dir_id'], "direccion" => $fila['dir_direccion'], "idCliente" => $fila['dir_id_cli'], "latitude" => $fila['dir_latitude'], "longitude" => $fila['dir_longitude']];
}

/*$posts = '{
   "id": 1,
   "name": "A wooden door",
   "price": 12.50,
   "tags": ["home", "wooden"]
}';   */

echo json_encode($data);

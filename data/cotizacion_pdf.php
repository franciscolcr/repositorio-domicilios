<?php
ob_start();
require_once('conexion.php');
session_start();
if (!isset($_SESSION['usuario'])) {
    echo "  <script>
               alert('USUARIO NO EXISTE COMUNICARSE CON UN ADMINISTRADOR');
               window.location='../index.php';
            </script>";
}

// Cargamos la librería dompdf que hemos instalado en la carpeta dompdf
$ndoc =  $_GET['ndoc'];
$cli_id = $_GET['cli_id'];
$descuento = $_GET['descuento'];
$division = $_COOKIE['sucursal'];
//echo $_SERVER['DOCUMENT_ROOT'] . '/img/nlogo.png';
/**
 * cOTIZACION

 */
$cot = "SELECT  PLUDocCliente, PLUGestor, Codigo, Fecha,Hora, PLUCliente, Nombre, SumaGravado, IVA,Total, Tipo FROM prg.docclientesm WHERE Cerrado = 'T' and tipo in ('CTFC','CTCF') and PLUDocCliente='$ndoc'";
$dcm = odbc_exec($conn, $cot);
$docm = odbc_fetch_object($dcm);

/**
 * info del cliente
 */
$cli_c = "SELECT * FROM prg.cli_clientes WHERE  cli_id='$cli_id'";
$cli = odbc_exec($conn, $cli_c);
$cliente = odbc_fetch_object($cli);

/**
 * info de la sucursal
 */
$suc = "SELECT Nombre,Fax,Telefono FROM prg.divisiones WHERE  PLUDivision='$division'";
$sucu = odbc_exec($conn, $suc);
$sucursal = odbc_fetch_object($sucu);
/**
 * info del gestor
 */
$gest = "SELECT * FROM prg.gestores WHERE PLUGestor='$docm->PLUGestor'";
$gesto = odbc_exec($conn, $gest);
$gestor = odbc_fetch_object($gesto);
/**
 * detalle de cotizacion
 */
$cotd = "SELECT dcd.Cantidad, dcd.PLUProducto, dcd.Descuento,dcd.Unitario, dcd.Total, dcd.UnitarioConIVA, dcd.TotalConIVA,dcd.PLUDocCliente, pr.Descripcion, g.Codigo from prg.docclientesd as dcd 
JOIN prg.productos pr ON dcd.pluproducto=pr.pluproducto
JOIN prg.pGrupos g ON pr.PLUGrupo = g.GrupoId 
     WHERE dcd.PLUDocCliente = '$ndoc'";
$dcd = odbc_exec($conn, $cotd);

/**
 * compra emplaza
 */
$cotcp = "SELECT dcd.Cantidad, dcd.NoBasico, dcd.Descuento,dcd.Unitario, dcd.Total, dcd.UnitarioConIVA, dcd.TotalConIVA,dcd.PLUDocCliente, dcd.Descripcion, g.Codigo from prg.docclientesotros as dcd 
LEFT OUTER JOIN prg.productos pr ON dcd.NoBasico=pr.NoBasico
LEFT OUTER JOIN prg.pGrupos g ON pr.PLUGrupo = g.GrupoId 
     WHERE dcd.PLUDocCliente ='$ndoc'";
$dcotcp = odbc_exec($conn, $cotcp);

/**
 * Servicios
 */


$deleteFile = $_SERVER['DOCUMENT_ROOT'] . '/cotizacion/' . $ndoc . '.pdf';
if (file_exists($deleteFile)) {
    unlink($deleteFile);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reportes de depositos </title>
    <style>
    .blueTable {
        border: 1px solid #1C6EA4;
        width: 100%;

        /*text-align: center;*/
        border-collapse: collapse;
    }

    .blueTable td,
    .blueTable th {

        padding: 3px 2px;
    }



    .blueTable thead {
        background: #1C6EA4;
        border-bottom: 2px solid #444444;
    }

    .blueTable thead th {
        font-size: 15px;
        font-weight: bold;
        color: #FFFFFF;
        border-left: 2px solid #D0E4F5;
    }

    .blueTable thead th:first-child {
        border-left: none;
    }

    .blueTable tfoot {
        top: 60px;

        font-size: 18px;
        font-weight: bold;
        color: #222222;
        background: #D0E4F5;
        background: -moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
        background: -webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
        background: linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
        border-top: 2px solid #444444;
    }

    .blueTable tfoot td {
        font-size: 14px;

    }

    .center {
        text-align: center;
    }

    .espacio {
        position: fixed;
        bottom: 10px;
        left: 0px;
        right: 0px;
        height: 50px;
    }


    footer {
        position: fixed;
        bottom: -10px;
        left: 0px;
        right: 0px;
        height: 50px;

        text-align: center;
        line-height: 35px;

    }

    /**div */
    .myDiv1 {
        text-align: left;
        width: 250px;
        height: 100px;
    }

    .myDiv2 {
        text-align: center;
        width: 260px;
    }

    .myDiv3 {
        text-align: center;
        width: 260px;
    }

    .divRdio {
        border-radius: 20px;
        border: 2px dotted #000;
        padding: 5px;
        width: 260px;
        height: 35px;
        font-size: 22px;
        text-align: center;
    }
    </style>
</head>

<body>


    <table width="100%;">
        <tbody>
            <tr>
                <td>
                    <div class="myDiv1">
                        <img src="<?php echo $_SERVER['DOCUMENT_ROOT'] . '/img/nlogo.png'; ?>" width="250px;"
                            height="100px;">
                    </div>
                    <!-- <span style="font-size: 10px;">¡SOMOS MÁS QUE UN SURTIDO COMPLETO DE REPUESTOS!</span> -->
                </td>
                <td width="280px;" align="center">
                    <!--<div class="divRdio">
                        <label class="labelRadio">Compara Nuestros Precios</label>
                    </div> -->
                    <span style="font-size: 15px; "><strong>VENTA DE REPUESTOS Y ACCESORIOS PARA CARROS Y CAMIONES SAN
                            SALVADOR, EL SALVADOR, C.A.</strong></span>
                </td>
                <td>
                    <div class="myDiv3">
                        <span>Fecha: <?= date("d-m-Y", strtotime($docm->Fecha)); ?></span><br>
                        <span>Hora: <?= date("H:i a", strtotime($docm->Hora)); ?></span>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <table width="100%;">
        <thead>
            <tr>
                <th width="18%;">
                    <div style="text-align: left;"> <strong>SUCURSAL </strong></div>
                </th>
                <th width="35%;">
                    <div style="text-align: left; font-weight: normal;">: <?= $sucursal->Nombre ?></div>
                </th>
                <th width="10%;">
                    <div style="text-align: left;"> <strong>Correo </strong></div>
                </th>
                <th width="32%;">
                    <div style="text-align: left; font-weight: normal;">: <?= $sucursal->Fax ?></div>
                </th>
            </tr>
            <tr>
                <th width="18%;">
                    <div style="text-align: left;"> <strong>COTIZACION N° </strong></div>
                </th>
                <th width="35%;">
                    <div style="text-align: left; font-weight: normal;">: <?= $docm->Codigo ?></div>
                </th>
                <th width="10%;">
                    <div style="text-align: left;"> <strong>Telefono </strong></div>
                </th>
                <th width="32%;">
                    <div style="text-align: left; font-weight: normal;">: <?= $sucursal->Telefono ?></div>
                </th>

            </tr>
            <tr>
                <th>
                    <div style="text-align: left;"> <strong>SR(A)S </strong></div>
                </th>
                <th width="30%;">
                    <div style="text-align: left; font-weight: normal;">: <?= $docm->PLUCliente ?> &nbsp;-&nbsp;
                        <?= $cliente->cli_nombre ?></div>
                </th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th>
                    <div style="text-align: left;"> <strong>VENDEDOR </strong></div>
                </th>
                <th>
                    <div style="text-align: left; font-weight: normal;">: <?= $gestor->Codigo ?> &nbsp;-&nbsp;
                        <?= $gestor->Nombre ?></div>
                </th>
                <th></th>
                <th></th>
            </tr>

        </thead>
    </table>
    <br>
    <br>
    <table class="blueTable" width="100%">
        <thead>
            <tr>
                <th scope="col">Cantidad</th>
                <th scope="col">Grupo</th>
                <th scope="col">Descripcion</th>
                <th scope="col">P. Unit</th>
                <?php
                if ($descuento == 'Si') { // condicion si va amostrar el descuento
                ?>
                <th scope="col">Descuento</th>
                <?php
                }
                ?>
                <th scope="col">P. Total</th>
            </tr>
        </thead>
        <tbody>
            <?php
            while ($fila = odbc_fetch_array($dcd)) {
            ?>
            <tr>
                <td class="center"><?= $fila['Cantidad'] ?></td>
                <td class="center"><?= $fila['Codigo'] ?></td>
                <td><?= utf8_encode($fila['Descripcion']) ?></td>
                <td class="center">$
                    <?php
                        if ($docm->Tipo == 'CTCF') {
                            echo number_format($fila['Unitario'], 4);
                        } else {
                            echo number_format($fila['UnitarioConIVA'], 4);
                        }
                        ?>
                </td>
                <?php
                    if ($descuento == 'Si') { // condicion si va amostrar el descuento
                    ?>
                <td class="center"><?= $fila['Descuento'] ?></td>
                <?php
                    }
                    ?>

                <td class="center">$
                    <?php
                        if ($docm->Tipo == 'CTCF') {
                            echo number_format($fila['Total'], 4);
                        } else {
                            echo number_format($fila['TotalConIVA'], 4);
                        }
                        ?>
                </td>

            </tr>
            <?php
            }
            while ($fila2 = odbc_fetch_array($dcotcp)) {
            ?>
            <tr>
                <td class="center"><?= $fila2['Cantidad'] ?></td>
                <td class="center"><?php echo $fila2['Codigo'] ? isset($fila2['Codigo']) : '-';  ?></td>
                <td><?php echo utf8_encode($fila2['Descripcion']) ; ?></td>
                <td class="center">$
                    <?php
                        if ($docm->Tipo == 'CTCF') {
                            echo number_format($fila2['Unitario'], 4);
                        } else {
                            echo number_format($fila2['UnitarioConIVA'], 4);
                        }
                        ?>
                </td>
                <?php
                    if ($descuento == 'Si') { // condicion si va amostrar el descuento
                    ?>
                <td class="center"><?php echo $fila2['Descuento'] ? isset($fila2['Descuento']) : '-'; ?></td>
                <?php
                    }
                    ?>

                <td class="center">$
                    <?php
                        if ($docm->Tipo == 'CTCF') {
                            echo number_format($fila2['Total'], 4);
                        } else {
                            echo number_format($fila2['TotalConIVA'], 4);
                        }
                        ?>
                </td>

            </tr>
            <?php
            }
            ?>
        </tbody>

        <tfoot>
            <?php
            if ($descuento == 'Si') { // condicion si va amostrar el descuento
            ?>
            <tr>
                <td colspan="4"><strong>NOTA:</strong></td>
                <td class="center">SUMAS</td>
                <td class="center">$
                    <?php
                        if ($docm->Tipo == 'CTCF') {
                            echo number_format($docm->SumaGravado, 4);
                        } else {
                            $subtotal = $docm->SumaGravado + $docm->IVA;
                            echo number_format($subtotal, 4);
                        }
                        ?>
                </td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td>IVA</td>
                <td>$
                    <?php
                        if ($docm->Tipo == 'CTCF') {
                            echo  number_format($docm->IVA, 4);
                        } else {

                            echo number_format(0, 4);
                        }
                        ?>
                </td>
            </tr>
            <tr>
                <td colspan="4"><strong>¡Precio sujeto a cambio!</strong></td>
                <td class="center">TOTAL</td>
                <td class="center">$ <?= $docm->Total ?> </td>
            </tr>
            <?php
            } else {
            ?>
            <tr>
                <td colspan="3"><strong>NOTA:</strong></td>
                <td class="center">SUMAS</td>
                <td class="center">$
                    <?php
                        if ($docm->Tipo == 'CTCF') {
                            echo number_format($docm->SumaGravado, 4);
                        } else {
                            $subtotal = $docm->SumaGravado + $docm->IVA;
                            echo number_format($subtotal, 4);
                        }
                        ?>
                </td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td class="center">IVA</td>
                <td class="center">$
                    <?php
                        if ($docm->Tipo == 'CTCF') {
                            echo  number_format($docm->IVA, 4);
                        } else {

                            echo number_format(0, 4);
                        }
                        ?>
                </td>
            </tr>
            <tr>
                <td colspan="3"><strong>¡Precio sujeto a cambio!</strong></td>
                <td class="center">TOTAL</td>
                <td class="center">$ <?= $docm->Total ?> </td>
            </tr>
            <?php
            }
            ?>

        </tfoot>
    </table>
</body>
<footer>
    <table style="margin-top: -150px;" align="center" width="100%;">
        <tbody>
            <tr>
                <td class="center"><span style="font-size: 15px; color:#3E408C;"><strong>¡SOMOS MAS QUE UN SURTIDO
                            COMPLETO DE REPUESTOS!</strong> </span></td>
            </tr>
            <tr>
                <td><img src="<?php echo $_SERVER['DOCUMENT_ROOT'] . '/img/cotizacion/2.PNG'; ?>" width="100%"
                        height="170PX"></td>

            </tr>
        </tbody>
    </table>
</footer>

</html>
<?php
require('../vendor/dompdf/autoload.inc.php');
$html = ob_get_clean();
//echo $html;
use Dompdf\Dompdf;
use Dompdf\Options;

// instantiate and use the dompdf class
$options = new Options();
$options->setIsRemoteEnabled(true);
$pdf = new Dompdf($options);
$pdf->loadHtml($html);
$pdf->setPaper('letter');
$pdf->render();

$canvas = $pdf->getCanvas();

$w = $canvas->get_width();
$h = $canvas->get_height();

$imageURL = $_SERVER['DOCUMENT_ROOT'] . '/img/departamentos_bg_3.jpg';
//$imageURL = $_SERVER['DOCUMENT_ROOT'] . '/img/departments__body.jpg';
$imgWidth = 650;
$imgHeight = 800;

$canvas->set_opacity(.1);

$x = (($w - $imgWidth) / 2);
$y = (($h - $imgHeight) / 2);

$canvas->image($imageURL, $x, $y, $imgWidth, $imgHeight);

$pdf = $pdf->output(); // esto te deja en la variable $pdf el contenido del documento PDF
$filename = $_SERVER['DOCUMENT_ROOT'] . '/cotizacion/' . $ndoc . '.pdf';
file_put_contents($filename, $pdf);



//$pdf->stream("cotizacion.pdf", array("Attachment" => false));
if (file_put_contents($filename, $pdf)) {
    echo 1;
}
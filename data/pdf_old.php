<?php
require_once('conexion.php');

require('../vendor/fpdf/fpdf.php');
// $ntraslado = $_POST['ndoc'];

$id_motorista = $_GET['motorista'];
$finicio = $_GET['finicio'];
$ffin = $_GET['ffin'];


$con = "SELECT dc.tipo, dm_observacion,mt_motivo,mo_nombre,dm_codigo,des_fecha_asignacion,dm_id_doc,dm_id_mt,dm_id_cli,cli_nombre,dm_date_start, dm_date_end FROM prg.dm_domicilios
LEFT OUTER JOIN prg.docclientesm dc on dm_id_doc=pludoccliente
JOIN prg.mt_motivos on dm_id_mt=mt_id
JOIN prg.des_destinos on dm_id=des_id_dm
JOIN prg.mo_motoristas on des_id_mo=mo_id
JOIN PRG.cli_clientes on dm_id_cli=cli_id
WHERE des_id_mo= '$id_motorista' and des_id_estado=4 and des_fecha_asignacion between '$finicio' and '$ffin' AND mt_motivo !='TRASLADO MERCADERIA' order by  des_fecha_asignacion DESC, dm_date_start DESC ";
$ds = odbc_exec($conn, $con);

$query = "SELECT  dm_observacion,mt_motivo,mo_nombre,dm_codigo,des_fecha_asignacion,dm_id_doc,dm_id_mt,dm_id_cli,cli_nombre,dm_date_start, dm_date_end FROM prg.dm_domicilios
LEFT OUTER JOIN prg.trasladosm dc on dm_id_doc = plutraslado
JOIN prg.mt_motivos on dm_id_mt=mt_id
JOIN prg.des_destinos on dm_id=des_id_dm
JOIN prg.mo_motoristas on des_id_mo=mo_id
JOIN PRG.cli_clientes on dm_id_cli=cli_id
WHERE des_id_mo= '$id_motorista' and des_id_estado=4 and des_fecha_asignacion between '$finicio' and '$ffin' AND mt_motivo = 'TRASLADO MERCADERIA' order by  des_fecha_asignacion DESC, dm_date_start DESC";
$return = odbc_exec($conn, $query);


// MOTO
$con2 = "SELECT  mo_nombre FROM  prg.mo_motoristas WHERE mo_id= '$id_motorista'  ";
$ds2 = odbc_exec($conn, $con2);
// create document
//Instaciamos la clase para genrear el documento pdf
$pdf = new FPDF('L', 'mm', 'A4');

//Agregamos la primera pagina al documento pdf
$pdf->AddPage();

//Seteamos el inicio del margen superior en 25 pixeles

//$y_axis_initial = 25;

//Seteamos el tiupo de letra y creamos el titulo de la pagina. No es un encabezado no se repetira
$pdf->SetFont('Arial', 'B', 10);

$pdf->Cell(290, 6, 'LA CASA DEL REPUESTO, S.A. DE C.V.', 0, 0, 'C');
$pdf->Ln(6);
$pdf->Cell(290, 6, 'REPORTE DE DOMICILIOS POR MOTORISTA', 0, 0, 'C');
$pdf->Ln(6);
$pdf->SetFont('Arial', '', 10);

while ($fila2 = odbc_fetch_array($ds2)) {
    $pdf->Cell(110, 6, 'NOMBRE: ' . $fila2['mo_nombre'], 0, 0, 'C');
}
$pdf->Cell(150, 6, 'DESDE: ' . date("d/m/Y", strtotime($_GET['finicio'])), 0, 0, 'C');
$pdf->Cell(-25, 6, 'HASTA: ' . date("d/m/Y", strtotime($_GET['ffin'])), 0, 0, 'C');
$pdf->Ln(6);

//Creamos las celdas para los titulo de cada columna y le asignamos un fondo gris y el tipo de letra
$pdf->SetFillColor(232, 232, 232);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(8, 6, '#', 1, 0, 'C', 5);
$pdf->Cell(45, 6, 'MOTIVO', 1, 0, 'C', 5);
$pdf->Cell(23, 6, 'FECHA', 1, 0, 'C', 5);
$pdf->Cell(35, 6, 'HORA INICIO/FIN', 1, 0, 'C', 5);
$pdf->Cell(85, 6, 'CLIENTE', 1, 0, 'C', 5);
$pdf->Cell(15, 6, 'CODIGO', 1, 0, 'C', 5);
$pdf->Cell(10, 6, 'TIPO', 1, 0, 'C', 5);
$pdf->Cell(20, 6, 'DOC', 1, 0, 'C', 5);
$pdf->Cell(25, 6, 'MONTO', 1, 0, 'C', 5);
$pdf->Ln(6);

//Comienzo a crear las filas del reporte según la consulta MySQL
$i = 1;
$suma_total = 0;
$suma = 0;
$resta = 0;
$menosTotal = 0;
$menos = 0;
$FinTotal = 0;

while ($fila = odbc_fetch_array($ds)) {


    $mt_motivo = $fila['mt_motivo'];
    $des_fecha_asignacion = $fila['des_fecha_asignacion'];
    $dm_codigo = $fila['dm_codigo'];
    $dm_id_mt = $fila['dm_id_mt'];
    $dm_id_doc = $fila['dm_id_doc'];
    $tipo = $fila['tipo'];
    $cliente = trim(strtoupper(utf8_decode($fila['cli_nombre'])));

    $dm_date_start = date("d-m-Y H:i a", strtotime($fila['dm_date_start']));
    $dm_date_end = date("d-m-Y H:i a", strtotime($fila['dm_date_end']));;
    // $Codigo = $fila['Codigo'];

    //*************************************************************** */
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(8, 6, $i, 1, 0, 'C', 0);
    $pdf->Cell(45, 6, $mt_motivo, 1, 0, 'C', 0);
    $pdf->Cell(23, 6, $des_fecha_asignacion, 1, 0, 'C', 0);
    $pdf->Cell(60, 6, $dm_date_start . ' || ' . $dm_date_end, 1, 0, 'C', 0);
    $pdf->Cell(85, 6, $cliente, 1, 0, 'C', 0);
    $pdf->Cell(15, 6, $dm_codigo, 1, 0, 'C', 0);
    $pdf->Cell(10, 6, $tipo, 1, 0, 'C', 0);
    if ($dm_id_mt == 2) {
        /**
         * para ventas 
         */
        $numfact1 = '';
        $numberFat = "SELECT Codigo, Total FROM prg.docclientesm where PLUDocCliente=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);

        $numfact2 = odbc_fetch_object($fact);
        $suma = floatval($numfact2->Total);
        $pdf->Cell(20, 6, $numfact2->Codigo, 1, 0, 'C', 0);
        $pdf->Cell(25, 6, '$ ' . number_format($suma, 2), 1, 0, 'C', 0);
    }

    if ($dm_id_mt == 6) {
        /**
         * para MUESTRA
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);

        $numfact2 = odbc_fetch_object($fact);
        $suma = floatval(0);
        $orden = $numfact2->n_orden ? $numfact2->n_orden : null;
        $pdf->Cell(20, 6, $orden, 1, 0, 'C', 0);
        $pdf->Cell(25, 6, '$ ' . number_format($suma, 2), 1, 0, 'C', 0);
    }
    if ($dm_id_mt == 7) {
        /**
         * para CP
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);


        $numfact2 = odbc_fetch_object($fact);
        $suma = floatval(0);
        $pdf->Cell(20, 6, $numfact2->n_orden, 1, 0, 'C', 0);
        $pdf->Cell(25, 6, '$ ' . number_format($suma, 2), 1, 0, 'C', 0);
    }
    if ($dm_id_mt == 1) {
        /**
         * para COBRO
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);


        $numfact2 = odbc_fetch_object($fact);
        $suma = floatval(0);
        $pdf->Cell(20, 6, $numfact2->n_orden, 1, 0, 'C', 0);
        $pdf->Cell(25, 6, '$ ' . number_format($suma, 2), 1, 0, 'C', 0);
    }
    if ($dm_id_mt == 9) {
        /**
         * para ADMIN
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);


        $numfact2 = odbc_fetch_object($fact);
        $suma = floatval(0);
        $pdf->Cell(20, 6, $numfact2->n_orden, 1, 0, 'C', 0);
        $pdf->Cell(25, 6, '$ ' . number_format($suma, 2), 1, 0, 'C', 0);
    }
    if ($dm_id_mt == 14) {
        /**
         * para TALLER
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);


        $numfact2 = odbc_fetch_object($fact);
        $suma = floatval(0);
        $pdf->Cell(20, 6, $numfact2->n_orden, 1, 0, 'C', 0);
        $pdf->Cell(25, 6, '$ ' . number_format($suma, 2), 1, 0, 'C', 0);
    }

    if ($dm_id_mt == 4) {
        /**
         * para  devoluciones
         */
        $numfact1 = '';

        $numberFat = "SELECT Codigo, Total FROM prg.docclientesm where PLUDocCliente=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);

        $numfact2 = odbc_fetch_object($fact);
        $resta = floatval($numfact2->Total);
        $suma = floatval($numfact2->Total);
        $pdf->Cell(20, 6, $numfact2->Codigo, 1, 0, 'C', 0);
        $pdf->Cell(25, 6, '- $ ' . number_format($resta, 2), 1, 0, 'C', 0);
        $menos += $resta;
    }


    $suma_total += $suma;

    $pdf->Ln(6);
    $i++;
}

//Traslado 
$suma = $suma_total;
//$menosTotal = $menos;
while ($fila2 = odbc_fetch_array($return)) {
    $mt_motivo = $fila2['mt_motivo'];
    $des_fecha_asignacion = $fila2['des_fecha_asignacion'];
    $dm_codigo = $fila2['dm_codigo'];
    $dm_id_mt = $fila2['dm_id_mt'];
    $dm_id_doc2 = $fila2['dm_id_doc'];
    $tipo = 'T';
    $cliente = trim(strtoupper(utf8_decode($fila2['cli_nombre'])));

    $dm_date_start = date("d-m-Y H:i a", strtotime($fila2['dm_date_start']));
    $dm_date_end = date("d-m-Y H:i a", strtotime($fila2['dm_date_end']));
    // $Codigo = $fila['Codigo'];

    //*************************************************************** */
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(8, 6, $i, 1, 0, 'C', 0);
    $pdf->Cell(45, 6, $mt_motivo, 1, 0, 'C', 0);
    $pdf->Cell(23, 6, $des_fecha_asignacion, 1, 0, 'C', 0);
    $pdf->Cell(35, 5, $dm_date_start . ' || ' . $dm_date_end, 1, 0, 'C', 0);
    $pdf->Cell(85, 6, $cliente, 1, 0, 'C', 0);
    $pdf->Cell(15, 6, $dm_codigo, 1, 0, 'C', 0);
    $pdf->Cell(10, 6, $tipo, 1, 0, 'C', 0);

    if ($dm_id_mt == 11) {
        /**
         * para traslados
         */
        $numfactT = '';
        $numberFat = "SELECT Codigo, Total FROM  prg.trasladosm where PLUTraslado=$dm_id_doc2";
        $fact = odbc_exec($conn, $numberFat);

        $numfactT = odbc_fetch_object($fact);

        $suma = isset($numfactT->Total) ? isset($numfactT->Total) : 0;
        $codigo = isset($numfactT->Codigo) ? isset($numfactT->Codigo) : null;

        $pdf->Cell(20, 6, $codigo, 1, 0, 'C', 0);
        $pdf->Cell(25, 6, '$ ' . number_format($suma, 2), 1, 0, 'C', 0);
    }

    $pdf->Ln(6);
    $suma_total += $suma;
}
$FinTotal = $suma_total - $menos;
//$rest = floatval($suma_total) - floatval($RestaTotal);
$pdf->Ln(6);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(253, 6, 'Total', 1, 0, 'C', 0);
$pdf->Cell(25, 6, '$ ' . number_format($suma_total, 2), 1, 0, 'C', 0);

$pdf->Ln(6);
$pdf->Cell(253, 6, 'Descuento', 1, 0, 'C', 0);
$pdf->Cell(25, 6, '- $ ' . number_format($menos, 2), 1, 0, 'C', 0);

$pdf->Ln(6);
$pdf->Cell(253, 6, 'Total Final', 1, 0, 'C', 0);
$pdf->Cell(25, 6, '$ ' . number_format($FinTotal, 2), 1, 0, 'C', 0);

//mysql_close($enlace);

//Mostramos el documento pdf
$pdf->Output();

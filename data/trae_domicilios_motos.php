<?php
header('Content-type: application/json');  
require_once('conexion.php');
$id_motorista = $_POST['motorista'];
$finicio = $_POST['finicio'] . ' 00:00:00';
$ffin = $_POST['ffin'] . ' 23:59:59';
//echo $finicio;
$con = "SELECT  dm_observacion,mt_motivo,mo_nombre,Codigo,dm_codigo,des_fecha_asignacion,dm_id,dm_id_mt,dm_id_doc,dm_id_cli,cli_nombre,dm_date_start, dm_date_end  FROM prg.dm_domicilios
LEFT OUTER JOIN prg.docclientesm dc on dm_id_doc=pludoccliente
JOIN prg.mt_motivos on dm_id_mt=mt_id
JOIN prg.des_destinos on dm_id=des_id_dm
JOIN prg.mo_motoristas on des_id_mo=mo_id
JOIN PRG.cli_clientes on dm_id_cli=cli_id
WHERE des_id_mo= '$id_motorista' and des_id_estado=4 and des_fecha_asignacion BETWEEN '$finicio' AND '$ffin' ";
$ds = odbc_exec($conn, $con);
?>
<thead>
    <tr>
        <th width=1%>#</th>
        <th width=2%>TIPO</th>
        <th width=2%>FECHA</th>
        <th width=2%>HORA INICIO/FIN</th>
        <th width=2%>CLIENTE</th>
        <th width=2%>CODIGO</th>
        <th width=2%>DOC</th>
        <th width=2%>MONTO</th>
        <th width=2%>ACCION</th>
    </tr>
</thead>
<?php
$jj = 0;
while ($fila = odbc_fetch_array($ds)) {
    $jj++;
    $dm_id_doc = $fila['dm_id_doc'];
    $total = '';
    $dm_date_start = date("H:i a", strtotime($fila['dm_date_start']));
    $dm_date_end = date("H:i a", strtotime($fila['dm_date_end']));;
?>
    <tr>
        <td><?= $jj ?></td>
        <td align="center"><?= $fila['mt_motivo'] ?></td>
        <td align="center"><?= $fila['des_fecha_asignacion'] ?></td>
        <td align="center"><?= $dm_date_start . ' || ' . $dm_date_end ?></td>
        <td align="center"><a href="#" onclick="ver_dm(<?= $fila['dm_id'] ?>);"><?= $fila['cli_nombre'] ?></a></td>
        <td align="center"><?= utf8_encode($fila['dm_codigo']) ?></td>
        <?php if ($fila['dm_id_mt'] == 2 || $fila['dm_id_mt'] == 4) {
            /**
             * para ventas y devoluciones
             */
            $numfact1 = '';
            $numberFat = "SELECT Codigo, Total FROM prg.docclientesm where PLUDocCliente=$dm_id_doc";
            $fact = odbc_exec($conn, $numberFat);
            $numfact1 = odbc_fetch_object($fact);
            if (isset($numfact1->Total)) {
                $total = $numfact1->Total;
        ?>
                <td align="center"><?= $numfact1->Codigo ?></td>
                <td align="center">$<?= number_format($total, 2) ?></td>
            <?php
            } else {
            ?>
                <td align="center"><?= 0 ?></td>
                <td align="center">$<?= number_format(0, 2) ?></td>
            <?php
            }
        }
        if ($fila['dm_id_mt'] == 11) {
            /**
             * para traslados
             */
            $numfact2 = '';
            $numberFat = "SELECT Codigo, Total FROM  prg.trasladosm where PLUTraslado=$dm_id_doc";
            $fact = odbc_exec($conn, $numberFat);

            $numfact2 = odbc_fetch_object($fact);
            $total = $numfact2->Total;
            ?>
            <td align="center"><?= $numfact2->Codigo ?></td>
            <td align="center">$<?= number_format($total, 2) ?></td>
        <?php
        }
        if ($fila['dm_id_mt'] == 6) {
            /**
             * para Muestras
             */
            $numfact2 = '';
            $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
            $fact = odbc_exec($conn, $numberFat);

            $numfact2 = odbc_fetch_object($fact);
            $total = 0;
        ?>
            <td align="center"><?= $numfact2->n_orden ?></td>
            <td align="center">$<?= number_format($total, 2) ?></td>
        <?php
        }
        if ($fila['dm_id_mt'] == 7) {
            /**
             * CP
             */
            $numfact2 = '';
            $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
            $fact = odbc_exec($conn, $numberFat);
            $numfact2 = odbc_fetch_object($fact);
            $total = 0;
        ?>
            <td align="center"><?= $numfact2->n_orden ?></td>
            <td align="center">$<?= number_format($total, 2) ?></td>
        <?php
        }
        if ($fila['dm_id_mt'] == 12) {
            $numfact2 = '';
            $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
            $fact = odbc_exec($conn, $numberFat);
            $numfact2 = odbc_fetch_object($fact);
            $total = 0;
        ?>
            <td align="center"><?= $numfact2->n_orden ?></td>
            <td align="center">$<?= number_format($total, 2) ?></td>
        <?php
        }
        if ($fila['dm_id_mt'] == 9) {
            /**
             * para ADmin
             */
            $numfact2 = '';
            $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
            $fact = odbc_exec($conn, $numberFat);
            $numfact2 = odbc_fetch_object($fact);
            $total = 0;
        ?>
            <td align="center"><?= $numfact2->n_orden ?></td>
            <td align="center">$<?= number_format($total, 2) ?></td>
        <?php
        }
        if ($fila['dm_id_mt'] == 15) {
            /**
             * para RETORNO
             */
            $numfact2 = '';
            $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
            $fact = odbc_exec($conn, $numberFat);

            $numfact2 = odbc_fetch_object($fact);
            $total = 0;
        ?>
            <td align="center"><?= $numfact2->n_orden ?></td>
            <td align="center">$<?= number_format($total, 2) ?></td>
        <?php
        }
        if ($fila['dm_id_mt'] == 14) {
            /**
             * para taller
             */
            $numfact2 = '';
            $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
            $fact = odbc_exec($conn, $numberFat);
            $numfact2 = odbc_fetch_object($fact);
            $total = 0;
        ?>
            <td align="center"><?= $numfact2->n_orden ?></td>
            <td align="center">$<?= number_format($total, 2) ?></td>
        <?php
        }
        if ($fila['dm_id_mt'] == 1) {
            /**
             * para cobros
             */
            $numfact2 = '';
            $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
            $fact = odbc_exec($conn, $numberFat);
            $numfact2 = odbc_fetch_object($fact);
            $total = 0;
        ?>
            <td align="center"><?= $numfact2->n_orden ?></td>
            <td align="center">$<?= number_format($total, 2) ?></td>
        <?php
        }
        ?>
        }
        <td align="center"><button class="btn btn-primary" role="button" data-bs-toggle="button" onclick="ver_dm(<?= $fila['dm_id'] ?>);">VER DETALLE</button></td>
    </tr>
<?php
}
?>
<script>
    function ver_dm(id_domicilio) {
        $('#ver_dm').modal('show');
        trae_dm_ver(id_domicilio);
        //alert (id_domicilio);
    }
    function trae_dm_ver(id) {
        var divVenta = document.getElementById("divVenta");
        var divTraslado = document.getElementById("divTraslado");
        var divCobros = document.getElementById("divCobros");
        var divAdmin = document.getElementById("divAdmin");
        var divMuestra = document.getElementById("divMuestra");
        var divCP = document.getElementById("divCP");
        var divTaller = document.getElementById("divTaller");
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '../data/trae_datos_dm.php',
            data: {
                id: id
            },
            success: function(html) {
                //var mo[] = html;
                $('#codigo_dm_ver').val(html.codigo);
                $('#tipo_dm_ver').val(html.motivo);
                $('#vendedor_dm_ver').val(html.vendedor);
                $('#fecha_dm_ver').val(html.fecha);
                $('#hora_dm_ver').val(html.hora);
                $('#observaciones_dm_ver').val(html.observacion);
                $('#motorista_dm_ver').val(html.motorista);
                $('#fasignacion_dm_ver').val(html.fasignacion);
                $('#hasignacion_dm_ver').val(html.hasignacion);
                //$('#dm_tipo').val(html.tipo);         
                $('#dm_doc').val(html.doc); // venta
                $('#dm_doct').val(html.doc); // traslados
                $('#dm_docc').val(html.doc); // cobros
                $('#dm_doc_ad').val(html.doc); // admin
                $('#dm_docm').val(html.doc); // muestra
                $('#dm_doc_cp').val(html.doc); // compra en plaza
                $('#dm_doc_taller').val(html.doc); // compra en plaza
                $('#codigodoc').val(html.codigoFact);
                $('#codigodoc1').val(html.codigoFact);
                $('#codigodoc2').val(html.n_orden);
                $('#dm_suc').val(html.sucursal);
                // venta y devoluciones
                if (html.tipo == 2 || html.tipo == 4) {
                    divVenta.style.display = "block";
                    divTraslado.style.display = "none";
                    divCobros.style.display = "none";
                    divAdmin.style.display = "none";
                    divMuestra.style.display = "none";
                    divCP.style.display = "none";
                    divTaller.style.display = "none";
                } else if (html.tipo == 11) { // traslado
                    divVenta.style.display = "none";
                    divTraslado.style.display = "block";
                    divCobros.style.display = "none";
                    divAdmin.style.display = "none";
                    divMuestra.style.display = "none";
                    divCP.style.display = "none";
                    divTaller.style.display = "none";
                } else if (html.tipo == 1) { // cobro
                    divVenta.style.display = "none";
                    divTraslado.style.display = "none";
                    divCobros.style.display = "block";
                    divAdmin.style.display = "none";
                    divMuestra.style.display = "none";
                    divCP.style.display = "none";
                    divTaller.style.display = "none";
                } else if (html.tipo == 6) { // muestra
                    divVenta.style.display = "none";
                    divTraslado.style.display = "none";
                    divCobros.style.display = "none";
                    divAdmin.style.display = "none";
                    divMuestra.style.display = "block";
                    divCP.style.display = "none";
                    divTaller.style.display = "none";
                } else if (html.tipo == 7) { // CP
                    divVenta.style.display = "none";
                    divTraslado.style.display = "none";
                    divCobros.style.display = "none";
                    divAdmin.style.display = "none";
                    divMuestra.style.display = "none";
                    divCP.style.display = "block";
                    divTaller.style.display = "none";
                } else if (html.tipo == 9) { // Admin
                    divVenta.style.display = "none";
                    divTraslado.style.display = "none";
                    divCobros.style.display = "none";
                    divAdmin.style.display = "block";
                    divMuestra.style.display = "none";
                    divCP.style.display = "none";
                    divTaller.style.display = "none";
                } else if (html.tipo == 14) { // taller
                    divVenta.style.display = "none";
                    divTraslado.style.display = "none";
                    divCobros.style.display = "none";
                    divAdmin.style.display = "none";
                    divMuestra.style.display = "none";
                    divCP.style.display = "none";
                    divTaller.style.display = "block";
                } else {
                    divVenta.style.display = "none";
                    divTraslado.style.display = "none";
                    divCobros.style.display = "none";
                    divAdmin.style.display = "none";
                    divMuestra.style.display = "none";
                    divCP.style.display = "none";
                    divTaller.style.display = "none";
                }
                trae_productos(html.doc, html.tipo);
            }
        });
    }
    //trae los productos del documento de una venta y de traslados
    function trae_productos(id_doc, tipo) {
        var tipo = tipo;
        var ndoc = id_doc;
        var tabla = '';
        if (tipo == 11) {
            tabla = '#table_productos_traslados';
        }
        if (tipo == 2 || tipo == 4) {
            tabla = '#table_productos';
        }
        if (tipo == 6) {
            tabla = '#table_productos_muestra';
        }
        if (tipo == 7) {
            tabla = '#table_productos_cp';
        }
        if (tipo == 1) {
            tabla = '#table_productos_cobros';
        }

        if (tipo == 9) {
            tabla = '#table_productos_admin';
        }
        if (tipo == 14) {
            tabla = '#table_productos_taller';
        }
        // (tipo == 11) ? (tabla = '#table_productos_traslados', ruta = '../data/trae_detalle_traslado.php') : (tabla = tabla, ruta = '../data/trae_detalle_doc.php');
        $.ajax({
            data: {
                ndoc: ndoc,
                tipo: tipo
            },
            url: "../data/trae_detalle_doc.php",
            dataType: 'html',
            type: 'POST',
            beforeSend: function() {
                $(tabla).html(
                    '<center><img alt="Cargando Reporte Motorista" src="../img/searching3.gif" /></center>'
                );
            },
            success: function(html) {
                setTimeout(function() {
                    $(tabla).html(html);
                }, 2000)
            }
        });
    }
</script>
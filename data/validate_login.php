<?php

$username = $_POST['usuario'];
/**variable $username captura el valor del usuario. */
$password = $_POST['password'];
/**variable $password captura la contraseña del usuario. */
require_once('conexion.php');
/**archivo de conexion */
/**variable $consulta guarda el SELECT A LA TABLA USUARIOS */
$consulta = "SELECT*FROM prg.usu_usuarios where usu_nombre='$username' and usu_password='$password'";
/**variable $resul guarda los datos obtenidos de la consulta */
$resul = odbc_exec($conn, $consulta);
/**variable $filas recorre los resultados de la consulta para poder evaluar los tipos de usuarios que se estan logeando */
$filas = odbc_fetch_array($resul);
if ($filas > 0) {
   /** la condicion debe ser mayor a cero por que el contador de los usuarios inicia en 1*/
   if ($filas['usu_id_tp'] == 1) {
      /**ADMINISTRADOR */
      setcookie("usuario", $filas['usu_nombre'],  time() + (14400 * 2), '/');
      setcookie("sucursal", $filas['id_suc'],  time() + (14400 * 2), '/');
      setcookie("tp_id", $filas['usu_id_tp'], time() + (14400 * 2), '/');
      setcookie("codigo_gestor", $filas['id_codigo_gestor'], time() + (14400 * 2), '/');
      setcookie("usu_id", $filas['usu_id'], time() + (14400 * 2), '/');
      header("Location:../views/admin_view.php");
   } else if ($filas['usu_id_tp'] == 2) {
      /**VENDEDOR */
      setcookie("usuario", $filas['usu_nombre'], time() + (14400 * 2), '/');
      setcookie("sucursal", $filas['id_suc'], time() + (14400 * 2), '/');
      setcookie("tp_id", $filas['usu_id_tp'], time() + (14400 * 2), '/');
      setcookie("codigo_gestor", $filas['id_codigo_gestor'], time() + (14400 * 2), '/');
      setcookie("usu_id", $filas['usu_id'], time() + (14400 * 2), '/');
      header("Location:../views/sales_view.php");
   } else if ($filas['usu_id_tp'] == 3) {
      /**DESPACHO */
      setcookie("usuario", $filas['usu_nombre'],  time() + (14400 * 2), '/');
      setcookie("sucursal", $filas['id_suc'],  time() + (14400 * 2), '/');
      setcookie("tp_id", $filas['usu_id_tp'], time() + (14400 * 2), '/');
      setcookie("codigo_gestor", $filas['id_codigo_gestor'], time() + (14400 * 2), '/');
      setcookie("usu_id", $filas['usu_id'], time() + (14400 * 2), '/');
      header("Location:../views/dispatch_view.php");
   } else if ($filas['usu_id_tp'] == 4) {
      /**MOTOCICLISTA*/
      setcookie("usuario", $filas['usu_nombre'],  time() + (14400 * 2), '/');
      setcookie("sucursal", $filas['id_suc'],  time() + (14400 * 2), '/');
      setcookie("tp_id", $filas['usu_id_tp'], time() + (14400 * 2), '/');
      setcookie("codigo_gestor", $filas['id_codigo_gestor'], time() + (14400 * 2), '/');
      setcookie("usu_id", $filas['usu_id'], time() + (14400 * 2), '/');
      setcookie("mo_id", $filas['usu_id_mo'], time() + (14400 * 2), '/');
      header("Location:../views/moto_view.php");
   }
} else {
   echo "
            <script>
               alert('USUARIO NO EXISTE COMUNICARSE CON UN ADMINISTRADOR');
               window.location='../index.php';
            </script>";
}
odbc_close($conn);

<?php
require_once('conexion.php');
$nombre = strtoupper($_POST['nombrecliente']);
$telefono = $_POST['telefono'];
$dui = $_POST['dui'];
$nit = $_POST['nit'];
$nrc = $_POST['nrc'];
$direccion = utf8_encode($_POST['direccion']);
$lat = $_POST['latitude'];
$log = $_POST['longitude'];
$suc = $_COOKIE['sucursal'];
$cliente_mt = $_POST['cliente_mt'];
$nit_mt = $_POST['nit_mt'];
$codigo_mt = $_POST['cod_mt'];
$tarjeta_mt = $_POST['tar_mt'];


$select_tipo_cliente = $_POST['select_tipo_cliente'];
if (isset($lat) || isset($log)) {
    if ($select_tipo_cliente == 1) {
        $correo_n = $_POST['correo_n'];
        $sql = "INSERT INTO prg.cli_clientes (cli_nombre,cli_celular,cli_id_suc,cli_nit,cli_dui,cli_nrc,cli_id_cliente_mt,cli_tipo,cli_email) 
        VALUES ('$nombre','$telefono','$suc','$nit','$dui','$nrc','$cliente','$select_tipo_cliente','$correo_n');COMMIT";
        odbc_exec($conn, $sql);

        $con = "SELECT TOP 1 cli_id FROM prg.cli_clientes WHERE cli_celular = '$telefono'";
        $ds = odbc_exec($conn, $con);

        $cliente_id = odbc_fetch_object($ds);

        $sql2 = "INSERT INTO prg.dir_direcciones (dir_id_cli,dir_direccion,dir_latitude,dir_longitude) 
        VALUES ('$cliente_id->cli_id','$direccion','$lat','$log');COMMIT";
        odbc_exec($conn, $sql2);
        odbc_close($conn);

        header("Location:../views/create_customers.php?msj=1");
    } else if ($select_tipo_cliente == 2) {
        $nombrecliente_emp = $_POST['nombrecliente_emp'];
        $telefono_emp = $_POST['telefono_emp'];
        $dui_emp = $_POST['dui_emp'];
        $nit_emp = $_POST['nit_emp'];
        $nrc_emp = $_POST['nrc_emp'];
        $correo_e = $_POST['correo_e'];
        $cliente = $_POST['cliente'];
        $nom_emp = $_POST['nom_emp'];
        $per_cont = $_POST['per_cont'];
        $num_cont = $_POST['num_cont'];


        $sql = "INSERT INTO prg.cli_clientes (cli_nombre,cli_celular,cli_id_suc,cli_nit,cli_dui,cli_nrc,cli_id_cliente_mt,cli_tipo,cli_nom_emp,cli_per_cont,cli_num_cont,cli_email) 
        VALUES ('$nombrecliente_emp','$telefono_emp','$suc','$nit_emp','$dui_emp','$nrc_emp','$cliente','$select_tipo_cliente','$nom_emp','$per_cont','$num_cont','$correo_e');COMMIT";
        odbc_exec($conn, $sql);

        $con = "SELECT TOP 1 cli_id FROM prg.cli_clientes WHERE cli_celular = '$telefono_emp'";
        $ds = odbc_exec($conn, $con);
        $cliente_id = odbc_fetch_object($ds);

        $sql2 = "INSERT INTO prg.dir_direcciones (dir_id_cli,dir_direccion,dir_latitude,dir_longitude) 
        VALUES ('$cliente_id->cli_id','$direccion','$lat','$log');COMMIT";
        odbc_exec($conn, $sql2);
        odbc_close($conn);

        header("Location:../views/create_customers.php?msj=1");
    } else if ($select_tipo_cliente == 3) {
        // modificado por jeu
        $suc_name = $_POST['sucursal_id'];
        $telefono_suc = $_POST['telefono_suc'];
        $correo_suc = $_POST['correo_suc'];

        $sql = "INSERT INTO prg.cli_clientes (cli_nombre,cli_celular,cli_id_suc,cli_nit,cli_dui,cli_nrc,cli_id_cliente_mt,cli_tipo,cli_nom_emp,cli_per_cont,cli_num_cont,cli_email) 
        VALUES ('$suc_name','$telefono_suc','$suc','$nit_emp','$dui_emp','$nrc_emp','$cliente','$select_tipo_cliente','$nom_emp','$per_cont','$num_cont','$correo_suc');COMMIT";
        odbc_exec($conn, $sql);

        $con = "SELECT TOP 1 * FROM prg.cli_clientes order by cli_id desc";
        $ds = odbc_exec($conn, $con);
        $cliente_id = odbc_fetch_object($ds);

        $sql2 = "INSERT INTO prg.dir_direcciones (dir_id_cli,dir_direccion,dir_latitude,dir_longitude) 
        VALUES ('$cliente_id->cli_id','$direccion','$lat','$log');COMMIT";
        odbc_exec($conn, $sql2);
        odbc_close($conn);

        header("Location:../views/create_customers.php?msj=1");
    } else if ($select_tipo_cliente == 4) {
        // modificado por jeu
        $proveedor = $_POST['proveedor'];
        $telefono_pro = $_POST['telefono_pro'];
        $nit_pro = $_POST['nit_pro'];
        $nrc_pro = $_POST['nrc_pro'];
        $correo_pro = $_POST['correo_pro'];

        $sql = "INSERT INTO prg.cli_clientes (cli_nombre,cli_celular,cli_id_suc,cli_nit,cli_nrc,cli_tipo,cli_email) 
        VALUES ('$proveedor','$telefono_pro','$suc','$nit_pro','$nrc_pro','$select_tipo_cliente','$correo_pro'); COMMIT";
        odbc_exec($conn, $sql);

        $con = "SELECT TOP 1 * FROM prg.cli_clientes order by cli_id desc";
        $ds = odbc_exec($conn, $con);
        $cliente_id = odbc_fetch_object($ds);

        $sql2 = "INSERT INTO prg.dir_direcciones (dir_id_cli,dir_direccion,dir_latitude,dir_longitude) 
        VALUES ('$cliente_id->cli_id','$direccion','$lat','$log');COMMIT";
        odbc_exec($conn, $sql2);
        odbc_close($conn);

        header("Location:../views/create_customers.php?msj=1");
    } else {
        echo "
        <script>
            alert('DEBES MANDAR DATOS EN EL FORMULARIO');
            window.location='../views/create_customers.php';
        </script>";
    }
} else {
    echo "
        <script>
            alert('DIRECCION OBLIGATORIO');
            window.location='../views/create_customers.php';
        </script>";
}
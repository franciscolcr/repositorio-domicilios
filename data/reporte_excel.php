<?php
require '../vendor/autoload.php';
require_once('conexion.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\IOFactory;

$id_motorista = $_GET['motorista'];
$finicio = $_GET['finicio'] . ' 00:00:00';
$ffin = $_GET['ffin'] . ' 23:59:59';

$con = "SELECT dc.tipo, dm_observacion,mt_motivo,mo_nombre,dm_codigo,des_fecha_asignacion,dm_id_doc,dm_id_mt,dm_id_cli,cli_nombre,dm_date_start, dm_date_end FROM prg.dm_domicilios
LEFT OUTER JOIN prg.docclientesm dc on dm_id_doc=pludoccliente
JOIN prg.mt_motivos on dm_id_mt=mt_id
JOIN prg.des_destinos on dm_id=des_id_dm
JOIN prg.mo_motoristas on des_id_mo=mo_id
JOIN PRG.cli_clientes on dm_id_cli=cli_id
WHERE des_id_mo= '$id_motorista' and des_id_estado=4 and des_fecha_asignacion between '$finicio' and '$ffin' AND mt_motivo !='TRASLADO MERCADERIA' order by  des_fecha_asignacion ASC, dm_date_start ASC ";
$ds = odbc_exec($conn, $con);

$query = "SELECT  dm_observacion,mt_motivo,mo_nombre,dm_codigo,des_fecha_asignacion,dm_id_doc,dm_id_mt,dm_id_cli,cli_nombre,dm_date_start, dm_date_end FROM prg.dm_domicilios
LEFT OUTER JOIN prg.trasladosm dc on dm_id_doc = plutraslado
JOIN prg.mt_motivos on dm_id_mt=mt_id
JOIN prg.des_destinos on dm_id=des_id_dm
JOIN prg.mo_motoristas on des_id_mo=mo_id
JOIN PRG.cli_clientes on dm_id_cli=cli_id
WHERE des_id_mo= '$id_motorista' and des_id_estado=4 and des_fecha_asignacion between '$finicio' and '$ffin' AND mt_motivo = 'TRASLADO MERCADERIA' order by  des_fecha_asignacion ASC, dm_date_start ASC";
$return = odbc_exec($conn, $query);


// MOTO
$con2 = "SELECT  mo_nombre FROM  prg.mo_motoristas WHERE mo_id= '$id_motorista'  ";
$ds2 = odbc_exec($conn, $con2);


$spreadsheet = new Spreadsheet();
$spreadsheet->getProperties()->setCreator("LCR JEU")->setTitle("REPORTE DE MOTOS");

$spreadsheet->setActiveSheetIndex(0);
$hojaActiva = $spreadsheet->getActiveSheet();

// Estilo de la letra y tamaño
$spreadsheet->getDefaultStyle()->getFont()->setName('Tahoma');
$spreadsheet->getDefaultStyle()->getFont()->setSize('15');

// ancho de la columna
$hojaActiva->getColumnDimension('A')->setWidth(5);
$hojaActiva->getColumnDimension('B')->setWidth(30);
$hojaActiva->getColumnDimension('C')->setWidth(15);
$hojaActiva->getColumnDimension('D')->setWidth(40);
$hojaActiva->getColumnDimension('E')->setWidth(50);
$hojaActiva->getColumnDimension('F')->setWidth(20);
$hojaActiva->getColumnDimension('G')->setWidth(15);
$hojaActiva->getColumnDimension('H')->setWidth(15);
$hojaActiva->getColumnDimension('I')->setWidth(15);

$spreadsheet->getActiveSheet()->mergeCells('A1:I1');
$spreadsheet->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setHorizontal('center');
/* $spreadsheet->getActiveSheet()->getStyle('A1:F1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFFF0000'); */
$hojaActiva->setCellValue('A1', 'LA CASA DEL REPUESTO, S.A. DE C.V.');

$spreadsheet->getDefaultStyle()->getFont()->setSize('11');

$spreadsheet->getActiveSheet()->mergeCells('A2:I2');
$spreadsheet->getActiveSheet()->getStyle('A2:I2')->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('A2', 'REPORTE DE DOMICILIOS POR MOTORISTA');

$spreadsheet->getDefaultStyle()->getFont()->setSize('9');
$spreadsheet->getActiveSheet()->mergeCells('B3:D3');
$spreadsheet->getActiveSheet()->getStyle('B3:D3')->getAlignment()->setHorizontal('');
while ($fila2 = odbc_fetch_array($ds2)) {
    $hojaActiva->setCellValue('B3', 'NOMBRE: ' . $fila2['mo_nombre']);
}
$hojaActiva->setCellValue('F3', 'DESDE: ' . date("d/m/Y", strtotime($_GET['finicio'])));
$hojaActiva->setCellValue('G3', 'HASTA: ' . date("d/m/Y", strtotime($_GET['ffin'])));

/// CUERPO
//Comienzo a crear las filas del reporte según la consulta MySQL
$i = 1;
$suma_total = 0;
$suma = 0;
$resta = 0;
$menosTotal = 0;
$menos = 0;
$FinTotal = 0;

$n = 5;
$hojaActiva->setCellValue('A4', '#');
$hojaActiva->setCellValue('B4', 'TIPO');
$hojaActiva->setCellValue('C4', 'FECHA_DM');
$hojaActiva->setCellValue('D4', 'HORA INICIO/FIN');
$hojaActiva->setCellValue('E4', 'CLIENTE');
$hojaActiva->setCellValue('F4', 'CODIGO_DM');
$hojaActiva->setCellValue('G4', 'TIPO');
$hojaActiva->setCellValue('H4', 'NUMERO_DOC');
$hojaActiva->setCellValue('I4', 'MONTO');
$spreadsheet->getActiveSheet()->getStyle('A4:I4')->getAlignment()->setHorizontal('center');
while ($fila = odbc_fetch_array($ds)) {

    $mt_motivo = $fila['mt_motivo'];
    $dm_id_mt = $fila['dm_id_mt'];
    $des_fecha_asignacion = date("d-m-Y", strtotime($fila['des_fecha_asignacion']));
    $dm_id_doc = $fila['dm_id_doc'];
    $dm_codigo = $fila['dm_codigo'];
    $cliente = $fila['cli_nombre'];
    $tipo = $fila['tipo'];

    $dm_date_start = date("d-m-Y H:i a", strtotime($fila['dm_date_start']));
    $dm_date_end = date("d-m-Y H:i a", strtotime($fila['dm_date_end']));;

    $hojaActiva->setCellValue('A' . $n, $i);
    $hojaActiva->setCellValue('B' . $n, $mt_motivo);
    $hojaActiva->setCellValue('C' . $n, $des_fecha_asignacion);
    $hojaActiva->setCellValue('D' . $n, $dm_date_start . ' || ' . $dm_date_end);
    $hojaActiva->setCellValue('E' . $n, $cliente);
    $hojaActiva->setCellValue('F' . $n, $dm_codigo);
    $hojaActiva->setCellValue('G' . $n, $tipo);


    //
    if ($dm_id_mt == 2) {
        /**
         * para ventas
         */
        $numfact1 = '';
        $numberFat = "SELECT Codigo,Total FROM prg.docclientesm where PLUDocCliente=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);

        $numfact2 = odbc_fetch_object($fact);
        if (isset($numfact2->Total)) {
            $suma = floatval($numfact2->Total);
            $hojaActiva->setCellValue('H' . $n, $numfact2->Codigo);
            $hojaActiva->setCellValue('I' . $n, number_format($suma, 2));
        } else {
            $suma = floatval(0);
            $hojaActiva->setCellValue('H' . $n, 0);
            $hojaActiva->setCellValue('I' . $n, number_format($suma, 2));
        }
    }


    if ($dm_id_mt == 6) {
        /**
         * recoger muestra
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);

        $numfact2 = odbc_fetch_object($fact);
        $suma = number_format(0, 2);
        $hojaActiva->setCellValue('H' . $n, $numfact2->n_orden);
        $hojaActiva->setCellValue('I' . $n, $suma);
    }

    if ($dm_id_mt == 12) {

        /**
         * VISITA CLIENTE PROGRAMADA
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);


        $numfact2 = odbc_fetch_object($fact);
        $suma = number_format(0, 2);
        $hojaActiva->setCellValue('H' . $n, $numfact2->n_orden);
        $hojaActiva->setCellValue('I' . $n, $suma);
    }

    if ($dm_id_mt == 7) {
        /**
         * compra emplaza
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);


        $numfact2 = odbc_fetch_object($fact);
        $suma = number_format(0, 2);
        $hojaActiva->setCellValue('H' . $n, $numfact2->n_orden);
        $hojaActiva->setCellValue('I' . $n, $suma);
    }
    if ($dm_id_mt == 1) {
        /**
         * cobro
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);


        $numfact2 = odbc_fetch_object($fact);
        $suma = number_format(0, 2);
        $hojaActiva->setCellValue('H' . $n, $numfact2->n_orden);
        $hojaActiva->setCellValue('I' . $n, $suma);
    }

    if ($dm_id_mt == 9) {
        /**
         * Administrativo
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);


        $numfact2 = odbc_fetch_object($fact);
        $suma = number_format(0, 2);
        $hojaActiva->setCellValue('H' . $n, $numfact2->n_orden);
        $hojaActiva->setCellValue('I' . $n, $suma);
    }
    if ($dm_id_mt == 15) {
        /**
         * RETORNO
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);


        $numfact2 = odbc_fetch_object($fact);
        $suma = number_format(0, 2);
        $hojaActiva->setCellValue('H' . $n, $numfact2->n_orden);
        $hojaActiva->setCellValue('I' . $n, $suma);
    }

    if ($dm_id_mt == 14) {
        /**
         * taller
         */
        $numfact2 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);


        $numfact2 = odbc_fetch_object($fact);
        $suma = number_format(0, 2);
        $hojaActiva->setCellValue('H' . $n, $numfact2->n_orden);
        $hojaActiva->setCellValue('I' . $n, $suma);
    }

    if ($dm_id_mt == 4) {
        /**
         * para  devoluciones
         */
        $numfact1 = '';
        $numberFat = "SELECT Codigo,Total FROM prg.docclientesm where PLUDocCliente=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);

        $numfact2 = odbc_fetch_object($fact);
        $resta = floatval($numfact2->Total);
        $suma = floatval($numfact2->Total);
        $hojaActiva->setCellValue('H' . $n, $numfact2->Codigo);
        $hojaActiva->setCellValue('I' . $n, number_format($suma, 2));
        $menos += $resta;
    }

    $suma_total += $suma;

    $spreadsheet->getActiveSheet()->getStyle('A' . $n . ':' . 'I' . $n)->getAlignment()->setHorizontal('center');
    $i++;
    $n++;
}
//$suma = $suma_total;
$suma2 = 0;
$traslados = 0;
$valTotal = 0;
//TRASLADOS
while ($fila2 = odbc_fetch_array($return)) {
    $mt_motivo = $fila2['mt_motivo'];
    $dm_id_mt = $fila2['dm_id_mt'];
    $des_fecha_asignacion = date("d-m-Y", strtotime($fila2['des_fecha_asignacion']));
    $dm_id_doc = $fila2['dm_id_doc'];
    $dm_codigo = $fila2['dm_codigo'];
    $cliente = $fila2['cli_nombre'];
    $tipo = 'T';
    $cliente = trim(strtoupper(utf8_decode($fila2['cli_nombre'])));

    $dm_date_start = date("d-m-Y H:i a", strtotime($fila2['dm_date_start']));
    $dm_date_end = date("d-m-Y H:i a", strtotime($fila2['dm_date_end']));

    $hojaActiva->setCellValue('A' . $n, $i);
    $hojaActiva->setCellValue('B' . $n, $mt_motivo);
    $hojaActiva->setCellValue('C' . $n, $des_fecha_asignacion);
    $hojaActiva->setCellValue('D' . $n, $dm_date_start . ' || ' . $dm_date_end);
    $hojaActiva->setCellValue('E' . $n, $cliente);
    $hojaActiva->setCellValue('F' . $n, $dm_codigo);
    $hojaActiva->setCellValue('G' . $n, $tipo);

    if ($dm_id_mt == 11) {
        // para traslados
        $numfact2 = '';
        $numberFat = "SELECT Codigo,Total FROM  prg.trasladosm where PLUTraslado=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);

        $numfactT = odbc_fetch_object($fact);
        if (isset($numfactT->Total)) {
            $valTotal = $numfactT->Total ? $numfactT->Total : 0;
            $codigo = $numfactT->Codigo;
        }

        $suma2 = floatval($valTotal);
        $hojaActiva->setCellValue('H' . $n, $codigo);
        $hojaActiva->setCellValue('I' . $n, number_format($suma2, 2));
    }
    $spreadsheet->getActiveSheet()->getStyle('A' . $n . ':' . 'I' . $n)->getAlignment()->setHorizontal('center');
    $traslados += $suma2;
    $i++;
    $n++;
}

$FinTotal = $suma_total - $menos;

$spreadsheet->getActiveSheet()->mergeCells('A' . ($n + 1) . ':' . 'H' . ($n + 1));
$spreadsheet->getActiveSheet()->getStyle('A' . ($n + 1) . ':' . 'I' . ($n + 1))->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('A' . ($n + 1), 'Devoluciones');
$hojaActiva->setCellValue('I' . ($n + 1), '- $ ' . number_format($menos, 2));


$spreadsheet->getActiveSheet()->mergeCells('A' . $n . ':' . 'H' . $n);
$spreadsheet->getActiveSheet()->getStyle('A' . $n . ':' . 'I' . $n)->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('A' . $n, 'Total Ventas');
$hojaActiva->setCellValue('I' . $n, '$ ' . number_format($FinTotal, 2));



$spreadsheet->getActiveSheet()->mergeCells('A' . ($n + 2) . ':' . 'H' . ($n + 2));
$spreadsheet->getActiveSheet()->getStyle('A' . ($n + 2) . ':' . 'I' . ($n + 2))->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('A' . ($n + 2), 'Total Traslados');
$hojaActiva->setCellValue('I' . ($n + 2), '$ ' . number_format($traslados, 2));
// redirect output to client browser
//header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="REPORTE_MOTO.xlsx"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
/* $writer = new Xlsx($spreadsheet);
$writer->save('Mi primer excel.xlsx'); */
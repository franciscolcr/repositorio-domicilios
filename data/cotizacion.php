<?php
require_once('conexion.php');

require('../vendor/dompdf/autoload.inc.php');
$ndoc =  $_POST['ndoc'];
$cli_id = $_POST['cli_id'];
$division = $_COOKIE['sucursal'];
$descuento = $_POST['descuento'];
/**
 * cOTIZACION

 */
$cot = "SELECT  PLUDocCliente, PLUGestor, Codigo, Fecha, PLUCliente, Nombre, SumaGravado, IVA,Total, Tipo FROM prg.docclientesm WHERE Cerrado = 'T' and tipo in ('CTFC','CTCF') and PLUDocCliente='$ndoc'";
$dcm = odbc_exec($conn, $cot);
$docm = odbc_fetch_object($dcm);

/**
 * info del cliente
 */
$cli_c = "SELECT * FROM prg.cli_clientes WHERE  cli_id='$cli_id'";
$cli = odbc_exec($conn, $cli_c);
$cliente = odbc_fetch_object($cli);

/**
 * info de la sucursal
 */
$suc = "SELECT Nombre FROM prg.divisiones WHERE  PLUDivision='$division'";
$sucu = odbc_exec($conn, $suc);
$sucursal = odbc_fetch_object($sucu);
/**
 * info del gestor
 */
$gest = "SELECT * FROM prg.gestores WHERE PLUGestor='$docm->PLUGestor'";
$gesto = odbc_exec($conn, $gest);
$gestor = odbc_fetch_object($gesto);
/**
 * detalle de cotizacion
 */
$cotd = "SELECT dcd.Cantidad, dcd.PLUProducto, dcd.Descuento, dcd.Unitario, dcd.Total, dcd.UnitarioConIVA, dcd.TotalConIVA,dcd.PLUDocCliente, pr.Descripcion, g.Codigo from prg.docclientesd as dcd 
LEFT OUTER JOIN prg.productos pr ON dcd.pluproducto=pr.pluproducto
LEFT OUTER JOIN prg.pGrupos g ON pr.PLUGrupo = g.GrupoId 
     WHERE dcd.PLUDocCliente = '$ndoc'";
$dcd = odbc_exec($conn, $cotd);

/**
 * compra emplaza
 */
$cotcp = "SELECT dcd.Cantidad, dcd.NoBasico, dcd.Descuento,dcd.Unitario, dcd.Total, dcd.UnitarioConIVA, dcd.TotalConIVA,dcd.PLUDocCliente, dcd.Descripcion, g.Codigo from prg.docclientesotros as dcd 
LEFT OUTER JOIN prg.productos pr ON dcd.NoBasico=pr.NoBasico
LEFT OUTER JOIN prg.pGrupos g ON pr.PLUGrupo = g.GrupoId 
     WHERE dcd.PLUDocCliente ='$ndoc'";
$dcotcp = odbc_exec($conn, $cotcp);
?>
<br>
<table width="100%" cellspacing="0">
    <thead>

        <tr>
            <th colspan="5">
                LA CASA DEL REPUESTO, S.A. DE C.V.
            </th>
        </tr>
        <tr>
            <th colspan="5">
                <?= $sucursal->Nombre ?>
            </th>
        </tr>
        <tr>
            <th colspan="5">
                Calle San Antonio Abad y Av. El Tanque N°. 3508 San Salvador
            </th>
        </tr>

    </thead>
</table>
<br>
<div class="card" style="width: 100%;">
    <table width="100%" cellspacing="0">
        <thead>

            <tr>
                <th>
                    <strong>COTIZACION N° </strong>
                </th>
                <th colspan="4">
                    : <?= $docm->Codigo ?>
                </th>
            </tr>
            <tr>
                <th>
                    <strong>SR(A)S </strong>
                </th>
                <th colspan="4">
                    : <?= $docm->PLUCliente ?> &nbsp;-&nbsp; <?= $cliente->cli_nombre ?>
                </th>
            </tr>
            <tr>
                <th ">
            <strong>VENDEDOR </strong>
        </th>
        <th colspan=" 4">
                    : <?= $gestor->Codigo ?> &nbsp;-&nbsp; <?= $gestor->Nombre ?>
                </th>
            </tr>

        </thead>
    </table>
</div>
<br>
<table class="table" width="100%">
    <thead>
        <tr>
            <th scope="col">Cantidad</th>
            <th scope="col">Grupo</th>
            <th scope="col">Descripcion</th>
            <th scope="col">P. Unit</th>
            <?php
            if ($descuento == 'Si') { // condicion si va amostrar el descuento
            ?>
            <th scope="col">Descuento</th>
            <?php
            }
            ?>
            <th scope="col">P. Total</th>
        </tr>
    </thead>
    <tbody>
        <?php
        while ($fila = odbc_fetch_array($dcd)) {
        ?>
        <tr>
            <td><?= $fila['Cantidad'] ?></td>
            <td><?= $fila['Codigo'] ?></td>
            <td><?= utf8_encode($fila['Descripcion']) ?></td>

            <td>$
                <?php
                    if ($docm->Tipo == 'CTCF') {
                        echo number_format($fila['Unitario'], 4);
                    } else {
                        echo number_format($fila['UnitarioConIVA'], 4);
                    }
                    ?>
            </td>
            <?php
                if ($descuento == 'Si') { // condicion si va amostrar el descuento
                ?>
            <td><?= $fila['Descuento'] ?></td>
            <?php
                }
                ?>

            <td>$
                <?php
                    if ($docm->Tipo == 'CTCF') {
                        echo number_format($fila['Total'], 4);
                    } else {
                        echo number_format($fila['TotalConIVA'], 4);
                    }
                    ?>
            </td>

        </tr>
        <?php
        }
        while ($fila2 = odbc_fetch_array($dcotcp)) {
        ?>
        <tr>
            <td class="center"><?= $fila2['Cantidad'] ?></td>
            <td class="center"><?php echo $fila2['Codigo'] ? isset($fila2['Codigo']) : '-';  ?></td>
            <td><?php echo utf8_encode($fila2['Descripcion']); ?></td>
            <td class="center">$
                <?php
                    if ($docm->Tipo == 'CTCF') {
                        echo number_format($fila2['Unitario'], 4);
                    } else {
                        echo number_format($fila2['UnitarioConIVA'], 4);
                    }
                    ?>
            </td>
            <?php
                if ($descuento == 'Si') { // condicion si va amostrar el descuento
                ?>
            <td class="center"><?php echo $fila2['Descuento'] ? isset($fila2['Descuento']) : '-'; ?></td>
            <?php
                }
                ?>
            <td class="center">$
                <?php
                    if ($docm->Tipo == 'CTCF') {
                        echo number_format($fila2['Total'], 4);
                    } else {
                        echo number_format($fila2['TotalConIVA'], 4);
                    }
                    ?>
            </td>

        </tr>
        <?php
        }
        ?>
    </tbody>
    <tfoot class="font-weight-bold">
        <?php
        if ($descuento == 'Si') { // condicion si va amostrar el descuento
        ?>
        <tr>
            <td colspan="4"><strong>NOTA:</strong></td>
            <td>SUMAS</td>
            <td>$ <?php
                        if ($docm->Tipo == 'CTCF') {
                            echo number_format($docm->SumaGravado, 4);
                        } else {
                            $subtotal = $docm->SumaGravado + $docm->IVA;
                            echo number_format($subtotal, 4);
                        }
                        ?>
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>IVA</td>
            <td>$
                <?php
                    if ($docm->Tipo == 'CTCF') {
                        echo  number_format($docm->IVA, 4);
                    } else {

                        echo number_format(0, 4);
                    }
                    ?>
            </td>
        </tr>
        <tr>
            <td colspan="4"><strong>Estos precios se mantienen durante 15 días!</strong></td>
            <td>TOTAL</td>
            <td>$ <?= number_format($docm->Total, 2) ?> </td>
        </tr>
        <?php
        } else {
        ?>
        <tr>
            <td colspan="3"><strong>NOTA:</strong></td>
            <td>SUMAS</td>
            <td>$ <?php
                        if ($docm->Tipo == 'CTCF') {
                            echo number_format($docm->SumaGravado, 4);
                        } else {
                            $subtotal = $docm->SumaGravado + $docm->IVA;
                            echo number_format($subtotal, 4);
                        }
                        ?>
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>IVA</td>
            <td>$
                <?php
                    if ($docm->Tipo == 'CTCF') {
                        echo  number_format($docm->IVA, 4);
                    } else {

                        echo number_format(0, 4);
                    }
                    ?>
            </td>
        </tr>
        <tr>
            <td colspan="3"><strong>Estos precios se mantienen durante 15 días!</strong></td>
            <td>TOTAL</td>
            <td>$ <?= number_format($docm->Total, 2) ?> </td>
        </tr>
        <?php
        }
        ?>

    </tfoot>
</table>
            
<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

<!-- Sidebar Toggle (Topbar) -->
<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
    <i class="fa fa-bars"></i>
</button>

<div>
    <h2>Bienvenido a 
        <?php 
        if($_COOKIE['sucursal']== 1){
            echo 'CASA MATRIZ';
        }else if ($_COOKIE['sucursal'] == 2) {
            echo 'SUCURSAL CENTRO';
        } else if ($_COOKIE['sucursal'] == 3){
            echo 'SUCURSAL SONSONATE';
        }else if ($_COOKIE['sucursal'] == 6){
            echo 'SUCURSAL 5 DE NOVIEMBRE';
        }else if ($_COOKIE['sucursal'] == 7){
            echo 'SUCURSAL SAN MARCOS';
        }else if ($_COOKIE['sucursal'] == 8){
            echo 'SUCURSAL SANTA TECLA';
        }else if ($_COOKIE['sucursal'] == 9){
            echo 'SUCURSAL DIRESA';
        }else if ($_COOKIE['sucursal'] == 10){
            echo 'SUCURSAL SAN MIGUEL';
        }else if ($_COOKIE['sucursal'] == 4000011){
            echo 'SUCURSAL 29';
        }else if ($_COOKIE['sucursal'] == 4000012){
            echo 'SUCURSAL CONSTITUCION';
        }else if ($_COOKIE['sucursal'] == 4000013){
            echo 'SUCURSAL VENEZUELA';
        }else if ($_COOKIE['sucursal'] == 25000001){
            echo 'SUCURSAL SANTA ANA';
        }else if ($_COOKIE['sucursal'] == 85000019){
            echo 'SUCURSAL ZACATECOLUCA';
        }else if ($_COOKIE['sucursal'] == 85000020){
            echo 'SUCURSAL NEJAPA';
        }

         ?></h2>
</div>

<!-- Topbar Navbar -->
<ul class="navbar-nav ml-auto">

<div class="topbar-divider d-none d-sm-block"></div>

<!-- Nav Item - User Information -->
<li class="nav-item dropdown no-arrow">
    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_COOKIE['usuario']?></span>
        <img class="img-profile rounded-circle"
            src="../img/undraw_profile.svg">
    </a>
    <!-- Dropdown - User Information -->
    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
        aria-labelledby="userDropdown">
        <a class="dropdown-item" href="#">
            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
            <?php echo $_COOKIE['usuario']?>
        </a>        
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
            Salir
        </a>
    </div>
</li>

</ul>

</nav>
<!-- End of Topbar -->

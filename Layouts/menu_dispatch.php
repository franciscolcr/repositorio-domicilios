<style>
img {
  max-width: 100%;
  height: auto;
}
</style>
    
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <br>    
    <!-- Nav Item - Dashboard -->
        <li class="nav-item ">
         <img src="../img/nlogo.png" alt="" width="180" height="70"> 
        </li>

       <br>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="../views/dispatch_view.php">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Inicio</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

         <!-- Nav Item - Pages Collapse Menu -->
         <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                aria-expanded="true" aria-controls="collapsePages">
                <i class="fas fa-fw fa-folder"></i>
                <span>Domicilios</span>
            </a>
            <div id="collapsePages" class="collapse show" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="../views/create_domicilios.php" onclick="iden();"><div  id="div1"></div>Nuevo Domicilio</a>
                <a class="collapse-item" href="../views/dispatch_view.php" >Todos Los Domicilios</a>
                <!--<a class="collapse-item" href="../views/view_dm_asignados.php" >Domicilios Asignados </a>   -->                 
                <a class="collapse-item" href="../views/view_multiruta.php">Orden De Ruta</a>
                <!--<a  class="collapse-item" href="../views/view_multiruta.php">Rutas</a>-->
                </div>
            </div>
        </li>              
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages2"
                aria-expanded="true" aria-controls="collapsePages2">
                <i class="fas fa-fw fa-folder"></i>
                <span>Reportes</span>
            </a>
            <div id="collapsePages2" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">                
                <a class="collapse-item" href="../views/reports_moto.php" >Reportes</a>
                <!--<a class="collapse-item" href="../views/view_dm_asignados.php" >Domicilios Asignados </a>   -->                 
                
                <!--<a  class="collapse-item" href="../views/view_multiruta.php">Rutas</a>-->
                </div>
            </div>
        </li>  
        
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle" ></button>
        </div>


    </ul>
    <!-- End of Sidebar -->
    <script>
    
    

    </script>
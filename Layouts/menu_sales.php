<style>
    img {
        max-width: 100%;
        height: auto;
    }
</style>

<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <br>
    <!-- Nav Item - Dashboard -->
    <li class="nav-item ">
        <img src="../img/nlogo.png" alt="" width="180" height="70">
    </li>

    <br>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="../views/sales_view.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fa fa-window-restore"></i>
            <span>Domicilios</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">

                <a class="collapse-item" href="../views/create_domicilios.php">Crear</a>
                <a class="collapse-item" href="../views/sales_view.php">Buscar</a>

            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="../views/cotizacion.php">
            <i class="fa fa-file-invoice-dollar"></i>
            <span>Cotización</span></a>
    </li>

    <!-- Nav Item - Utilities Collapse Menu 
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fa fa-motorcycle"></i>
                <span>Transportes</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">                   
                    <a class="collapse-item" href="../views/create_motoristas.php">Crear</a>
                    <a class="collapse-item" href="utilities-color.html">Buscar</a>
                    
                </div>
            </div>
        </li>-->


    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#clientes" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fa fa-list-ul" aria-hidden="true"></i>
            <span>Clientes</span>
        </a>
        <div id="clientes" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="create_customers.php">Crear Clientes</a>
                <a class="collapse-item" href="../views/client_list.php">Lista Clientes</a>

            </div>
        </div>
    </li>


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>


</ul>
<!-- End of Sidebar -->
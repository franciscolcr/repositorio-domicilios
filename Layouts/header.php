<?php

if (isset($_COOKIE['usuario'])) {
} else {
    echo "
    <script>
        alert('DEBES INICIAR SESSION PRIMERO');
        window.location='../index.php';
    </script>";
    exit;
}

?>
<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Domicilios Lcr</title>

    <!-- Custom fonts for this template-->

    <!-- Custom styles for this template-->
    <link href="../css/sb-admin-2.min.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <!-- 
    <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet"> -->
    <link href="../js/dist/css/select2.min.css" rel="stylesheet" />
    <!-- 
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css">

    <link href="../css/sweet_alert.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">

</head>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Domicilios Lcr</title>
    <!-- hacemos referencia a los template en la carpeta VENDOR de bostrap-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- hacemos referencia a los template en la carpeta CSS de bostrap-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
</head>

<body class="bg-gradient-primary">

    <div class="container">
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">

                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Bienvenido! <span id="mostrarSucur"></span>
                                        </h1>
                                    </div>
                                    <form class="user" action="data/validate_login.php" method="POST" autocomplete="OFF"
                                        id="loginform">
                                        <!-- Con el ACTION hacemos referencia al archivo de validacion para los usuarios-->

                                        <div id="error" class="alert alert-danger" style="display:none;">
                                        </div>
                                        <div class="form-group">
                                            <input name="usuario" class="form-control form-control-user" id="usuario"
                                                aria-describedby="emailHelp" placeholder="Usuario">
                                            <!-- INPUT para casilla usuario-->
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="password"
                                                placeholder="Contraseña" name="password">
                                            <!-- INPUT para casilla Contraseña-->
                                        </div>
                                        <button class="btn btn-primary btn-user btn-block col">Iniciar </button>
                                        <!--BUTTON iniciar session-->
                                    </form>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Complemento principal de JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Scripts personalizados para todas las páginas-->
    <script src="js/sb-admin-2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.1/js.cookie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.1/js.cookie.min.js.map"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        /** 
         * para mostar los nombres de las sucursales
         **/
        var cokkies = Cookies.get('id_sucu');
        if (cokkies) {
            if (cokkies == "1") {
                $("#mostrarSucur").html("CASA MATRIZ");
            }
            if (cokkies == "2") {
                $("#mostrarSucur").html("SUCURSAL CENTRO");
            }
            if (cokkies == "3") {
                $("#mostrarSucur").html("SUCURSAL SONSONATE");
            }
            if (cokkies == "6") {
                $("#mostrarSucur").html("SUCURSAL 5 DE NOVIEMBRE");
            }
            if (cokkies == "7") {
                $("#mostrarSucur").html("SUCURSAL SAN MARCOS");
            }
            if (cokkies == "8") {
                $("#mostrarSucur").html("SUCURSAL SANTA TECLA");
            }
            if (cokkies == "9") {
                $("#mostrarSucur").html("SUCURSAL DIRESA");
            }
            if (cokkies == "10") {
                $("#mostrarSucur").html("SUCURSAL SAN MIGUEL");
            }
            if (cokkies == "4000011") {

                $("#mostrarSucur").html("SUCURSAL 29");
            }
            if (cokkies == "4000012") {
                $("#mostrarSucur").html("SUCURSAL CONSTITUCION");
            }
            if (cokkies == "4000013") {
                $("#mostrarSucur").html("SUCURSAL VENEZUELA");
            }
            if (cokkies == "25000001") {
                $("#mostrarSucur").html("SUCURSAL SANTA ANA");
            }
            if (cokkies == "85000019") {
                $("#mostrarSucur").html("SUCURSAL ZACATECOLUCA");
            }
            if (cokkies == "85000020") {
                $("#mostrarSucur").html("SUCURSAL NEJAPA");
            }

        } else {


        }
        //VALIDAMOS QUE AMBOS INPUT (CONTRASEÑA Y USUARIO NO VAYAN VACIOS);
        $('#usuario').focus();
        $("#loginform").submit(function() {
            if (!$("#usuario").val()) {
                $("#error").text("Ingrese su Usuario").addClass("alert alert-danger").fadeIn("Slow");
                $('#usuario').focus();
                return false;
            } else if (!$("#password").val()) {
                $("#error").text("Ingrese su Contraseña").addClass("alert alert-danger").fadeIn("Slow");
                return false;
            } else {
                return true;
            }
        });
        $(function() {
            $("#id_suc").change(function() {
                var valor = $(this).val();
                Cookies.set('id_sucu', valor);
                //alert(sessionStorage.getItem("id_sucu"));
                $("#staticBackdrop").modal('hide');
                location.reload();
            });
        });
        $("#limpiar").click(function() {
            Cookies.remove('id_sucu');
            location.reload();
        });
    });
    </script>
    <script>
    $(document).ready(function() {
        $('.select2').select2({
            theme: 'classic',
            width: '100%',
            placeholder: "Selecccione un Vendedor",
            allowClear: true
        });
    });
    </script>
</body>

</html>
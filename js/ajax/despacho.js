function zero(n) {
  return (n > 9 ? "" : "0") + n;
}
$("#example").DataTable({ pageLength: 50 });
function tblRate() {
  $.ajax({
    type: "GET",
    dataType: "JSON",
    url: "../data/list_despacho.php",
    contentType: false,
    processData: false,
  }).done(function (response) {
    var table = $("#example").DataTable();
    table.clear();
    $.each(response["list"], (i, item) => {
      i += 1;
      console.log(item);
      if (item.cli_nombre) {
        var cliente = item.cli_nombre;
      } else {
        var cliente = "Sin Cliente";
      }
      //botones
      var condicion = "";
      if (item.impresoT == "T" || item.impresoT == null) {
        if (item.dm_estado != 4) {
          if (item.dm_estado == 1) {
            condicion =
              '<li><hr class="dropdown-divider"></li>	<a class="dropdown-item" href="#" onclick="trae_motoristas(' +
              item.dm_id +
              ');" >Asignar Domicilio</a> <li><hr class="dropdown-divider"></li><a class="dropdown-item" href="#" onclick="cancelarDomi(' +
              item.dm_id +
              ');">Cancelar Domicilio</a>';
          } else {
            condicion =
              '<li><hr class="dropdown-divider"></li> <a class="dropdown-item" href="#" onclick="trae_motoristas(' +
              item.dm_id +
              ');">Re-Asignar</a><li><hr class="dropdown-divider"><li> <a class="dropdown-item" href="https://domicilioslcr.com/domicilios_ver2/ruta/#/search/' +
              item.dm_codigo +
              '" target="_blank" >Ubicar</a></li>		  <a class="dropdown-item" href="#" onclick="cancelarDomi(' +
              item.dm_id +
              ');">Cancelar Domicilio</a>';
          }
        } else {
          condicion =
            '<li><hr class="dropdown-divider"></li>	 <a class="dropdown-item" href="https://domicilioslcr.com/domicilios_ver2/ruta/#/search/' +
            item.dm_codigo +
            '" target="_blank" >Ubicar</a>';
        }
      } else {
        condicion =
          '<li><hr class="dropdown-divider"></li><a class="dropdown-item  bg-warning text-dark">Doc No Impreso</a>';
      }

      // estados
      if (item.dm_estado == 1) {
        var style = "red";
      } else if (item.dm_estado == 2) {
        var style = "sky_blue";
      } else if (item.dm_estado == 3) {
        var style = "amarillo";
      } else if (item.dm_estado == 4) {
        var style = "verde";
      }

      // fecha
      var date = new Date(item.dm_fcreacion);
      var inicio = new Date(item.fecha_inicial);
      var fin = new Date(item.fecha_final);
      //console.log(item.fecha_final);
      var fecha =
        date.getFullYear() +
        "-" +
        zero(date.getMonth() + 1) +
        "-" +
        zero(date.getDate());
      var hora =
        zero(date.getHours()) +
        ":" +
        zero(date.getMinutes()) +
        ":" +
        zero(date.getSeconds());

      if (item.fecha_inicial != null) {
        // incio
        var hora_inicio =
          zero(inicio.getHours()) +
          ":" +
          zero(inicio.getMinutes()) +
          ":" +
          zero(inicio.getSeconds());
      } else {
        var hora_inicio = "00:00";
      }
      if (item.fecha_final != null) {
        // fin
        var hora_fin =
          zero(fin.getHours()) +
          ":" +
          zero(fin.getMinutes()) +
          ":" +
          zero(fin.getSeconds());
      } else {
        var hora_fin = "00:00";
      }
      $(
        table.row
          .add([
            i++,
            item.dm_codigo,
            item.mt_motivo,
            item.dm_observacion,
            '<a  href="#" class="' +
              style +
              '" onclick="ver_dm(' +
              item.dm_id +
              ');">' +
              cliente +
              "</a>",
            fecha + "<br>" + hora,
            hora_inicio + "<br>" + hora_fin,
            '<img src="../img/icons/' +
              item.est_icon +
              '" class="img-fluid"   width="25" height="10" data-toggle="tooltip" title="' +
              item.est_nombre +
              '">',
            '<div class="dropdown mb-4"><button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opciones</button><div class="dropdown-menu dropdown-menu" aria-labelledby="dropdownMenuButton" >          <a  href="#" class="dropdown-item" onclick="ver_dm(' +
              item.dm_id +
              ');"> Ver domicilio </a> <li><hr class="dropdown-divider"></li>	<a class="dropdown-item" href="./rout.php?idDireccion=' +
              item.dm_id_dir +
              '" target="_blank">Ver Geolocalización</a>' +
              condicion +
              " </div></div>",
          ])
          .nodes()
      ).addClass(style);
      table.draw();
      i++;
    });
  });
}
tblRate();
setInterval(tblRate, 10000);

$(document).ready(function () {
  $(document).on("change", "#cli_id", function () {
    var cli_id = $(this).val();
    $.ajax({
      dataType: "json",
      type: "POST",
      url: "../data/trae_info_cliente.php",
      data: {
        cli_id: cli_id,
      },
      success: function (html) {
        //console.log(html);
        var html5 =
          '<div class="form-check"><input class="form-check-input" type="checkbox" value="' +
          html.telefono +
          '" name="telefono" id="telefono" disabled><label class="form-check-label" for="telefono">Whatsapp: ' +
          html.telefono +
          '</label></div><div class="form-check"><input class="form-check-input" type="checkbox" value="' +
          html.correo +
          '" name="correo" id="correo" checked><label class="form-check-label" for="correo">Correo: ' +
          html.correo +
          " </label></div>";

        $("#radio").html(html5);
      },
    });
  });

  $(document).on("click", "#gpdf", function () {
    var from = $("#cotizacion").serialize();
    $("#loading").modal("show");
    //console.log(from);
    //var dataPrint = '<a href="../data/reporte_excel.php?'+from+'" class="btn btn-success" > <i class="fa fa-file-excel"></i> Imprimir Excel</a><a href="../data/reporte_pdf.php?'+from+'" class="btn btn-danger" target="_blank" id="reportePDF"> <i class="fa fa-file-pdf"></i> Imprimir PDF</a>';
    $.ajax({
      type: "GET",
      url: "../data/cotizacion_pdf.php?" + from,
      dataType: "html",
      success: function (html) {
        //console.log(html == true);
        $("#loading").modal("hide");
        if (html == true) {
          AlertConfirmacin("Documento Generado");
        } else {
          AlertError("Error al Generar el Documento");
        }
      },
    });
  });

  // envio
  $(document).on("click", "#envio", function () {
    var correo = $("#correo").val();
    var doc = $(this).val();
    var cli_id = $("#cli_id").val();
    $("#loading").modal("show");
    $.ajax({
      type: "POST",
      data: { correo: correo, doc: doc, cli_id: cli_id },
      url: "../data/cotizacion_send_email.php",
      dataType: "html",
      success: function (html) {
        //alert(html);
        $("#loading").modal("hide");
        if (html == true) {
          AlertConfirmacin("Documento Generado");
        } else {
          AlertError("Error al Generar el Documento");
        }
      },
    });
  });
});

function buscar() {
  var ndoc = $("#ndoc").val();
  var cli_id = $("#cli_id").val();
  var telefono = $("#telefono").val();
  var from = $("#cotizacion").serialize();
  //console.log(telefono);

  var tabla = "#datoscotizacion";
  if (ndoc == 0) {
    alert("Debes Seleccionar un Documento");
  } else if (cli_id == 0) {
    alert("Debes Selecionar un Cliente");
  } else if (telefono == "") {
    alert("Debes Seleccionar whatsapp");
  } else {
    /* var dataPrint =
      '<a href="../data/cotizacion_pdf.php?' +
      from +
      '" class="btn btn-success" > <i class="fa fa-file-pdf"></i>Generar PDF</a>'; */
    var dataPrint =
      '<button type="button" class="btn btn-success" id="gpdf" ><i class="fa fa-file-pdf"></i> Generar PDF</button>';
    $("#mostrar").html(dataPrint);
    $.get("../../cotizacion/" + ndoc + ".pdf?val=" + new Date().getTime())
      .done(function () {
        //alert("Existe" + ndoc);
        $("#datoscotizacionPDF").show();
        $(tabla).hide();
        var exist =
          '<iframe src="../../cotizacion/' +
          ndoc +
          '.pdf?val="' +
          new Date().getTime() +
          '" style="width:100%; height:700px;" frameborder="0"></iframe>';
        //document.getElementById("datoscotizacionPDF").display = block;
        $("#datoscotizacionPDF").html(exist);
        var EnviarEmail =
          '<button type="button" class="btn btn-info" id="envio" value="' +
          ndoc +
          '"><i class="fa fa-file-pdf"></i>Enviar PDF</button>';

        $("#envioEmail").html(EnviarEmail);
      })
      .fail(function () {
        //alert("No existe");
        $(tabla).show();
        $("#datoscotizacionPDF").hide();
        $.ajax({
          data: from,
          url: "../data/cotizacion.php",
          dataType: "html",
          type: "POST",

          beforeSend: function () {
            $(tabla).html(
              '<center><img alt="Cargando..." src="../img/searching3.gif" /></center>'
            );
          },
          success: function (html) {
            setTimeout(function () {
              $(tabla).html(html);
            }, 2000);
          },
        });
      });
    /* $.ajax({
      data: from,
      url: "../data/cotizacion.php",
      dataType: "html",
      type: "POST",
      beforeSend: function () {
        $(tabla).html(
          '<center><img alt="Cargando..." src="../img/searching3.gif" /></center>'
        );
      },
      success: function (html) {
        setTimeout(function () {
          $(tabla).html(html);
        }, 2000);
      },
    }); */
  }
}

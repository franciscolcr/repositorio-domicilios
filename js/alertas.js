function AlerSuccess(){
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 3000
        };
        toastr.success("Registro creado correctamente");

    }, 10);
}

function AlerUpdate(){
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 3000
        };
        toastr.success("Registro Actualizado correctamente");
    }, 10);
}

function AlerBorrar(){
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 3000
        };
        toastr.success("Registro borrado correctamente");

    }, 10);
}

function AlertConfirmacin(datos){
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 3000
        };
        toastr.success(datos);

    }, 10);
}


function AlertInformacion(datos){
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 3000
        };
        toastr.info(datos);

    }, 10);
}

function AlertAbvertencia(datos){
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 3000
        };
        toastr.warning(datos);

    }, 10);
}

function AlertError(datos){
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 3000
        };
        toastr.error(datos);

    }, 10);
}

// toast para eliminar
function preloadEliminar(){
   Swal.fire({
        title: "Borrado!",
        text: "Registro borrado correctamente.",
        type: "success",
        showCancelButton: false,
        confirmButtonColor: "#00c0ef",
        confirmButtonText: "Aceptar",
        closeOnConfirm: false
    });
}
function alertaprint(title, data){
    Swal.fire({
        title: title,
        text: data,
        type: "success",
        showCancelButton: false,
        confirmButtonColor: "#00c0ef",
        confirmButtonText: "Aceptar",
        closeOnConfirm: false
    });
}

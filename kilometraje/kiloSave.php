<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
$id_dm = '';
$id_mo = '';
$id_dir = '';

function kilometraje($id_dm, $id_mo, $id_dir)
{

    echo 'MOTORISTA=' . $id_mo . ' || Domicilio= ' . $id_dm . '|| Direccion=' . $id_dir;
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>GEOLOCATION</title>
    <style type="text/css">
        #global {
            height: 750px;
            /*width: 200px;*/
            border: 1px solid #ddd;
            background: #f1f1f1;
            overflow-y: scroll;
        }

        #mensajes {
            height: auto;
        }

        .texto {
            padding: 4px;
            background: #fff;
        }
    </style>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3cejbHkJOZfpHllVqF0bApcL2j0yKU8Y&libraries=places">
    </script>
    <link href="../css/sb-admin-2.min.css" rel="stylesheet">
</head>

<body>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                </div>
                <div class="card shadow mb-4">
                    <div class="card-header">
                        Geolocalización
                    </div>
                    <div class="card-body" id="global">
                        <div class="row">
                            <div id="mapa" class="col-lg-6 card-body">

                            </div>

                            <div id="indiciaciones" class="col-lg-6 card-body">
                                <h1>INDICACIONES</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var divMapa = document.getElementById('mapa');
        var indicatorElse = document.getElementById('indiciaciones');
        navigator.geolocation.getCurrentPosition(fn_ok, fn_mal);

        function fn_mal() {}


        let params = new URLSearchParams(location.search);
        let idDireccion = <?php echo $id_dir; ?>
        alert(idDireccion);
        let idMoto = '';
        let idDm = '';

        function fn_ok() {
            $.get("../data/geolocalizacion.php?idDireccion=" + idDireccion, function(resp) {
                console.log(resp);

                var lat = 13.718803968310953;
                var lon = -89.2238367870019;
                var gLatLon = new google.maps.LatLng(lat, lon);
                var objConfig = {
                    zoom: 17,
                    center: gLatLon
                }
                //ubicacion del motorista
                var gMapa = new google.maps.Map(divMapa, objConfig);
                var objConfigMarker = {
                    position: gLatLon,
                    map: gMapa,
                    draggable: true,
                    title: "TU ESTAS AQUI"
                }
                var gMarker = new google.maps.Marker(objConfigMarker);
                //en esta linea de codigo debera ir la variable address del cliente
                var gCoder = new google.maps.Geocoder();
                var objInformacion = {
                    address: resp[0].direccion,
                }
                gCoder.geocode(objInformacion, fn_coder);

                function fn_coder(datos) {
                    var coordenadas = datos[0].geometry.location;
                    var config = {
                        map: gMapa,
                        position: coordenadas,
                        title: 'TU DESTINO'
                    }
                    //este ejecuta el segundo marker
                    var gMarkerDV = new google.maps.Marker(config);
                }

                //funciones para  dibujar las rutas 
                var objConfigDR = {
                    map: gMapa,
                    suppressMarkers: true
                }
                var objConfigDS = {
                    origin: gLatLon,
                    destination: objInformacion.address,
                    travelMode: google.maps.TravelMode.DRIVING,
                    avoidTolls: true
                }

                var ds = new google.maps.DirectionsService();
                var dr = new google.maps.DirectionsRenderer(objConfigDR);
                //Funcion para generar el kilometraje y el tiempo aproximado de llegada
                dr.setPanel(indicatorElse);
                ds.route(objConfigDS, fnRutear);

                function fnRutear(resultados, status) {
                    if (status == 'OK') {
                        dr.setDirections(resultados);
                        var distance = resultados['routes'][0]['legs'][0]['distance'].text;
                        var duration = resultados['routes'][0]['legs'][0]['duration'].text;
                        console.log(distance + '||' + duration);

                    } else {
                        alert('Erro al trasar la ruta' + status);
                    }
                }
            })
        }
    </script>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="../js/sb-admin-2.min.js"></script>

</html>